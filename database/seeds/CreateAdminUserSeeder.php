<?php

use App\Subtipo;
use App\Tipo;
use Illuminate\Database\Seeder;
use App\User;
use App\TypeMaterial;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if(DB::connection('contrataciones')->table('areas')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('areas')->insert([
          [
            'alias'=>'dge',
						'nombre' => 'Área - Dirección General Ejecutiva',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'transparencia',
						'nombre' => 'Unidad - Transparencia y Lucha Contra La Corrupción',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'auditoria',
						'nombre' => 'Unidad - Auditoria Interna',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'legal',
						'nombre' => 'Unidad - Asesoría Legal',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'sistemas',
						'nombre' => 'Unidad - Sistemas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'gido',
						'nombre' => 'Unidad - Gestión Institucional y Desarrollo Organizacional	',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'cpype',
						'nombre' => 'Área - Coordinación de Planificación y Evaluación de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'upep',
						'nombre' => 'Unidad - Planificación y Evaluación de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'ucmsp',
						'nombre' => 'Unidad - Control, Seguimiento y Monitorio de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'caf',
						'nombre' => 'Área - Coordinación Administrativa Financiera',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'adm',
						'nombre' => 'Unidad - Administración',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'alias'=>'finanzas',
						'nombre' => 'Unidad - Finanzas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]      
        ]);
      } 
      else 
      {
        	echo "\e[31mTable areas is not empty, therefore NOT "; 
      }






    $role = Role::create(['name' => 'Administrador']);
    $role1 = Role::create(['name' => 'Usuario']);
    $role2 = Role::create(['name' => 'Supervisor']);
    $role3 = Role::create(['name' => 'rpa']);
    $role4 = Role::create(['name' => 'Legal']);

    $permissions = Permission::pluck('id','id')->all();

    $role->syncPermissions($permissions);
    $role1->syncPermissions($permissions);
    $role2->syncPermissions($permissions);
    $role3->syncPermissions($permissions);
    $role4->syncPermissions($permissions);

    $users=User::where('status',1)->whereIn('userstypes_id',[0,1,2])->get();
    foreach ($users as $user) {
      switch ($user->id) {
        case 1:
          $user->assignRole([$role->id]);
          break;
        
        case 75:
          $user->assignRole([$role2->id]);
          break;

        case 146:
          $user->assignRole([$role3->id]);
          break;

        case 143:
          $user->assignRole([$role4->id]);
          break;

        default:
          $user->assignRole([$role1->id]);
          break;
      }
    }

    //$user->assignRole([$role->id]);
    //$user1->assignRole([$role1->id]);
    //$user2->assignRole([$role2->id]);
    //$user3->assignRole([$role3->id]);

    $subTipoBienes=new Subtipo();
    $subTipoBienes->nombre='Bienes';
    $subTipoBienes->save();

    $subTipoServicios=new Subtipo();
    $subTipoServicios->nombre='Servicios';
    $subTipoServicios->save();

    $subTipoConsutoria=new Subtipo();
    $subTipoConsutoria->nombre='Consultoria';
    $subTipoConsutoria->save();

    $tipoMenor=new Tipo();
    $tipoMenor->nombre='Menor';
    $tipoMenor->save();

    $tipoMenor->subtipos()->attach($subTipoBienes);
    $tipoMenor->subtipos()->attach($subTipoServicios);
    $tipoMenor->subtipos()->attach($subTipoConsutoria);

    $tipoDirecta=new Tipo();
    $tipoDirecta->nombre='Directa';
    $tipoDirecta->save();

    $tipoDirecta->subtipos()->attach($subTipoBienes);
    $tipoDirecta->subtipos()->attach($subTipoServicios);

    $tipoAnpeMenor=new Tipo();
    $tipoAnpeMenor->nombre='Anpe Menor';
    $tipoAnpeMenor->save();

    $tipoAnpeMenor->subtipos()->attach($subTipoBienes);
    $tipoAnpeMenor->subtipos()->attach($subTipoServicios);
    $tipoAnpeMenor->subtipos()->attach($subTipoConsutoria);

    $tipoAnpeMayor=new Tipo();
    $tipoAnpeMayor->nombre='Anpe Mayor';
    $tipoAnpeMayor->save();

    $tipoAnpeMayor->subtipos()->attach($subTipoBienes);
    $tipoAnpeMayor->subtipos()->attach($subTipoServicios);
    $tipoAnpeMayor->subtipos()->attach($subTipoConsutoria);


        if(DB::connection('contrataciones')->table('tipo_materiales')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_materiales')->insert([
          [
						'descripcion' => 'Equipos de Computadoras',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'descripcion' => 'Muebles',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]       
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_materiales is not empty, therefore NOT "; 
      }







        if(DB::connection('contrataciones')->table('tipo_referencias')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_referencias')->insert([
          [
       
						'descripcion' => 'Celular',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
        
						'descripcion' => 'Tefefono',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_referencias is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('partidas')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('partidas')->insert([
          [
            'codigo' => '43110',
						'nombre' => 'Equipo de Oficina y Muebles',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'codigo' => '43120',
						'nombre' => 'Equipo de Computación',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'codigo' => '43500',
						'nombre' => 'Equipo de Comunicacion',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable partidas is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('servicios_conexos')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('servicios_conexos')->insert([
          [
    
						'descripcion' => 'Ensamblaje y/o instalación',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Soporte técnico',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Talleres autorizados',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Mantenimiento preventivo',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Capacitación',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Desaduanización',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Instalación',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
    
						'descripcion' => 'Otros que sean requeridos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]            

        ]);
      } 
      else 
      {
        	echo "\e[31mTable servicios_conexos is not empty, therefore NOT "; 
      }

    if(DB::connection('contrataciones')->table('tipo_garantias_tecnicas')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_garantias_tecnicas')->insert([
          [
    
						'descripcion' => 'Garantía del fabricante',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Garantía del Proveedor',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_garantias_tecnicas is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('lugares')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('lugares')->insert([
          [
    
						'descripcion' => 'Oficinas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Otro',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable lugares is not empty, therefore NOT "; 
      }


      if(DB::connection('contrataciones')->table('tipo_garantia')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_garantia')->insert([
          [
    
						'descripcion' => 'Boleta Gantantía',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Garantía de Primer Requerimiento',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Póliza de Seguro de Caución a Primer Requerimiento',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable lugares is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('porcentajes')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('porcentajes')->insert([
          [
    
						'descripcion' => '7%',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => '3%',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable porcentajes is not empty, therefore NOT "; 
      }


      if(DB::connection('contrataciones')->table('tipo_contrato')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_contrato')->insert([
          [
    
						'descripcion' => 'Contrato Administrativo',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Contrato Menor',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Orden de Servicio',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable porcentajes is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('tipo_adjudicacion')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('tipo_adjudicacion')->insert([
          [
    
						'descripcion' => 'Total',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Item',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Lote',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_adjudicacion is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('estados')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('estados')->insert([
          [
    
						'descripcion' => 'Elaborando',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Pendiente',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Corregir',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Validada',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Autorizado',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'Rechazado',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'V.B. Legal',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'descripcion' => 'N.N. Legal',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable estados is not empty, therefore NOT "; 
      }

      if(DB::connection('contrataciones')->table('area_user')->get()->count() == 0)
			{
				DB::connection('contrataciones')->table('area_user')->insert([
          [
            'usuario_id' => '62',
            'area_id' => '5',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'usuario_id' => '146',
            'area_id' => '10',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'usuario_id' => '75',
            'area_id' => '11',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'usuario_id' => '143',
            'area_id' => '4',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]
        ]);
      } 
      else 
      {
        	echo "\e[31mTable area_user is not empty, therefore NOT "; 
      }


    }

    
}
