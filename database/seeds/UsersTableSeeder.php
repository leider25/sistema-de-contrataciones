<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=new User();
        $user->username="leider.ticlla";
        $user->description="Pasante - Unidad de Sistemas";
        $user->name="Leider Jeil Ticlla Copa";
        $user->email="leider.ticlla@fonabosque.gob.bo";
        $user->password=bcrypt("password");
        $user->save();
    }
}
