<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('contrataciones')->create('areas', function (Blueprint $table) {   
            $table->bigIncrements('id');
            $table->string('alias',128)->nullable(false);
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('area_user', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('area_id');
            $table->foreign('area_id')->references('id')
                    ->on('areas')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('tipo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('subtipo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('subtipo_tipo', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_id');
            $table->foreign('tipo_id')->references('id')
                    ->on('tipo')->onDelete('cascade');
            $table->unsignedBigInteger('subtipo_id');
            $table->foreign('subtipo_id')->references('id')
                    ->on('subtipo')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('partidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('tipo_garantias_tecnicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('servicios_conexos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('condiciones_tecnicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('garantia_funcionamiento')->nullable(true);
            $table->string('certificacion')->nullable(true);
            $table->string('condicion_adicional')->nullable(true);
            $table->string('pruebas_funcionamiento')->nullable(true);
            $table->timestamps();
        });
        Schema::connection('contrataciones')->create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->nullable(false);
            $table->integer('cantidad')->nullable(false)->default(1);
            $table->string('unidad')->nullable(false);

            $table->unsignedBigInteger('condiciontecnica_id');
            $table->foreign('condiciontecnica_id')->references('id')
                    ->on('condiciones_tecnicas')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('caracteristicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);

            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')
                    ->on('items')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);

            $table->unsignedBigInteger('servicioconexo_id');
            $table->unsignedBigInteger('condiciontecnica_id');

            $table->foreign('servicioconexo_id')->references('id')
                    ->on('servicios_conexos')->onDelete('cascade');

            $table->foreign('condiciontecnica_id')->references('id')
                    ->on('condiciones_tecnicas')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('garantiastecnicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);

            $table->unsignedBigInteger('tipogarantiatecnica_id');
            $table->foreign('tipogarantiatecnica_id')->references('id')
                    ->on('tipo_garantias_tecnicas')->onDelete('cascade');

            $table->unsignedBigInteger('condiciontecnica_id');
            $table->foreign('condiciontecnica_id')->references('id')
                    ->on('condiciones_tecnicas')->onDelete('cascade');

            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('tipo_adjudicacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('tipo_contrato', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('lugares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('entregas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->integer('plazo')->nullable(false);

            $table->unsignedBigInteger('lugar_id');

            $table->foreign('lugar_id')->references('id')
                    ->on('lugares')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('tipo_garantia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('porcentajes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('garantias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);

            $table->unsignedBigInteger('tipogarantia_id');
            $table->foreign('tipogarantia_id')->references('id')
                    ->on('tipo_garantia')->onDelete('cascade');

            $table->unsignedBigInteger('porcentaje_id');
            $table->foreign('porcentaje_id')->references('id')
                    ->on('porcentajes')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('multas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->double('porcentaje_multa')->nullable(false);
            $table->timestamps();
        });



        Schema::connection('contrataciones')->create('condiciones_administrativas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('validez_propuesta')->nullable(false);
            

            $table->unsignedBigInteger('tipoadjudicacion_id');
            $table->foreign('tipoadjudicacion_id')->references('id')
                    ->on('tipo_adjudicacion')->onDelete('cascade');
            
            $table->unsignedBigInteger('tipocontrato_id');
            $table->foreign('tipocontrato_id')->references('id')
                    ->on('tipo_contrato')->onDelete('cascade');

            $table->unsignedBigInteger('entrega_id');
            $table->foreign('entrega_id')->references('id')
                    ->on('entregas')->onDelete('cascade');
            
            $table->unsignedBigInteger('garantia_id')->nullable(true);
            $table->foreign('garantia_id')->references('id')
                    ->on('garantias')->onDelete('cascade');

            $table->unsignedBigInteger('multa_id');
            $table->foreign('multa_id')->references('id')
                    ->on('multas')->onDelete('cascade');
            
            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('especificaciones_tecnicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('condiciontecnica_id');
            $table->foreign('condiciontecnica_id')->references('id')
                    ->on('condiciones_tecnicas')->onDelete('cascade');

            $table->unsignedBigInteger('condicionadministrativa_id');
            $table->foreign('condicionadministrativa_id')->references('id')
                    ->on('condiciones_administrativas')->onDelete('cascade');
            $table->timestamps();

        });

        Schema::connection('contrataciones')->create('estados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion')->nullable(false);
            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('objeto')->nullable(false);
            $table->string('objetivo')->nullable(false);
            $table->string('cite')->nullable(false);
            $table->date('fecha')->nullable(false);
            $table->string('antecedente')->nullable(false);
            $table->string('justificacion_tecnica')->nullable(false);
            $table->string('justificacion_directa')->nullable(true);
            $table->string('nombreArchivo')->nullable(false);
            $table->integer('usuario_id')->nullable();
            $table->unsignedBigInteger('tipo_id');
            $table->unsignedBigInteger('subtipo_id');
            $table->unsignedBigInteger('especificaciontecnica_id')->nullable(true);

            $table->foreign('tipo_id')->references('id')
                    ->on('tipo')->onDelete('cascade');
            $table->foreign('subtipo_id')->references('id')
                    ->on('subtipo')->onDelete('cascade');
            $table->foreign('especificaciontecnica_id')->references('id')
                    ->on('especificaciones_tecnicas')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('estado_solicitud', function (Blueprint $table) {
            $table->integer('usuario_id')->nullable(false);
            $table->string('motivo')->nullable(false);
            $table->boolean('activo')->default(1);
            $table->unsignedBigInteger('estado_id');
            $table->foreign('estado_id')->references('id')
                    ->on('estados')->onDelete('cascade');

            $table->unsignedBigInteger('solicitud_id');
            $table->foreign('solicitud_id')->references('id')
                    ->on('solicitud')->onDelete('cascade');
            $table->timestamps();
        });











        Schema::connection('contrataciones')->create('partida_solicitud', function (Blueprint $table) {
            $table->unsignedBigInteger('solicitud_id');
            $table->foreign('solicitud_id')->references('id')
                    ->on('solicitud')->onDelete('cascade');

            $table->unsignedBigInteger('partida_id');
            $table->foreign('partida_id')->references('id')
                    ->on('partidas')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::connection('contrataciones')->create('encargados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->string('cargo',128)->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->string('direccion',128)->nullable(false);
            $table->unsignedBigInteger('encargado_id');
            $table->foreign('encargado_id')->references('id')
                    ->on('encargados')->onDelete('cascade');
            $table->timestamps();
        });

        



        Schema::connection('contrataciones')->create('tipo_referencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('referencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->unsignedBigInteger('tiporeferencia_id');
            
            $table->foreign('tiporeferencia_id')->references('id')
                ->on('tipo_referencias')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('proveedor_referencia', function (Blueprint $table) {
            $table->unsignedBigInteger('proveedor_id');
            $table->foreign('proveedor_id')->references('id')
                    ->on('proveedores')->onDelete('cascade');

            $table->unsignedBigInteger('referencia_id');
            $table->foreign('referencia_id')->references('id')
                    ->on('referencias')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('adjudicaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha',128)->nullable(false);
            $table->integer('monto')->nullable(false);
            $table->string('documento')->nullable(true);
            $table->unsignedBigInteger('proveedor_id');
            $table->foreign('proveedor_id')->references('id')
                ->on('proveedores')->onDelete('cascade');
            $table->unsignedBigInteger('solicitud_id');
            $table->foreign('solicitud_id')->references('id')
                ->on('solicitud')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::connection('contrataciones')->create('tipo_materiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::connection('contrataciones')->dropIfExists('tipo');
        Schema::connection('contrataciones')->dropIfExists('subtipo');
        Schema::connection('contrataciones')->dropIfExists('partidas');
        Schema::connection('contrataciones')->dropIfExists('solicitud');
        Schema::connection('contrataciones')->dropIfExists('solicitud_partida');
        Schema::connection('contrataciones')->dropIfExists('tipo_materiales');
        Schema::connection('contrataciones')->dropIfExists('referencia_proveedor');
        Schema::connection('contrataciones')->dropIfExists('proveedores');
        Schema::connection('contrataciones')->dropIfExists('referencias');
        Schema::connection('contrataciones')->dropIfExists('tipo_referencias');
        Schema::connection('contrataciones')->dropIfExists('users');
        Schema::connection('contrataciones')->dropIfExists('areas');
        
       
    }
}
