<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['middleware' => ['auth']], function() {


    Route::group(['middleware' => ['role:Administrador|rpa|Legal|Supervisor|Usuario']], function () {

        Route::resource('roles','RoleController');
        Route::resource('users','UserController');
        Route::resource('products','ProductController');


        //Route::resource('administrator/supplier', 'AdministratorController\SupplierController',['names' => [
		//	'store' => 'administrator.supplier.store',
		//	'index' => 'administrator.supplier.index',
		//	'create' => 'administrator.supplier.create',
		//	'show' => 'administrator.supplier.show',
		//	'edit' => 'administrator.supplier.edit',
		//]]);
		//Route::post('administrator/supplier/update', 'AdministratorController\SupplierController@update')->name('administrator.supplier.update');
		//Route::get('administrator/supplier/destroy/{id}', 'AdministratorController\SupplierController@destroy');
		Route::resource('administrador/usuarios','UsuarioController',['names' => [
			'store' => 'administrator.supplier.store',
			'index' => 'administrator.usuarios.index',
			'show' => 'administrator.supplier.show',
		]]);
		Route::get('/administrator/usuario/rol/{id}', 'UsuarioController@roles');

		Route::get('supervisor/solicitud', 'SolicitudSupervisorController@index')->name('supervisor.solicitud');
		Route::get('supervisor/autorizado', 'AutorizadosSupervisorController@index')->name('supervisor.autorizado');
		Route::get('supervisor/pdf/autorizacion/{id}', 'AutorizadosSupervisorController@autorizacion');
		Route::get('supervisor/pdf/adjudicacion/{id}', 'AutorizadosSupervisorController@adjudicacion');
		Route::post('supervisor/autorizacion/documentos', 'AutorizadosSupervisorController@documentosProveedor');
		Route::get('supervisor/autorizacion/documento/{id}', 'AutorizadosSupervisorController@documentos');

		Route::post('supervisor/autorizacion/enviar', 'AutorizadosSupervisorController@enviarLegal');
		Route::post('legal/proceso/aprobar', 'AutorizadosLegalController@aprobar');
		Route::post('legal/proceso/rechazar', 'AutorizadosLegalController@rechazar');

		Route::get('legal/procesos/', 'AutorizadosLegalController@index')->name('legal.autorizados');
		Route::get('responsable/solicitud', 'SolicitudResponsableController@index')->name('responsable.solicitud');
		
		Route::post('responsable/solicitud/aprobar', 'SolicitudResponsableController@aprobar');
		Route::post('responsable/solicitud/rechazar', 'SolicitudResponsableController@rechazar');


		Route::get('solicitud/especificacionbienes/{id}', 'EspecificacionTecnicaBienesController@index');
		Route::get('solicitud/especificacionbienes/edit/{id}', 'EspecificacionTecnicaBienesController@edit');
		
		
		Route::post('supervisor/solicitud/validar', 'SolicitudSupervisorController@validar');
		Route::post('supervisor/solicitud/corregir', 'SolicitudSupervisorController@corregir');
		Route::post('supervisor/autorizado/nota/{id}', 'AutorizadosSupervisorController@generarAdjudicacion');
		Route::get('supervisor/autorizado/empresa/{id}', 'AutorizadosSupervisorController@empresa');

		Route::get('solicitud/especificacionservicios/{id}', 'EspecificacionTecnicaServicioController@index');
		Route::get('solicitud/especificacionconsultoria/{id}', 'EspecificacionTecnicaConsultoriaController@index');





		Route::post('solicitud/especificacion/crear', 'EspecificacionTecnicaBienesController@crear')->name('especificacion.crear');
		Route::post('solicitud/especificacion/actualizar', 'EspecificacionTecnicaBienesController@actualizar')->name('especificacion.actualizar');
		Route::get('solicitud/especificacion/word/{id}', 'EspecificacionTecnicaBienesController@generadorWord');
		Route::get('solicitud/especificacion/pdf/{id}', 'EspecificacionTecnicaBienesController@generadorPdf');
		

		Route::post('solicitud/corregido', 'SolicitudController@corregido');
		Route::post('solicitud/pendiente', 'SolicitudController@pendiente');
		Route::post('solicitud/validado', 'SolicitudController@validado');

		Route::get('solicitud', 'SolicitudController@index')->name('solicitud');
		Route::get('solicitud/cotizacion/{id}', 'SolicitudController@cotizacion')->name('solicitud.cotizacion');
		
		Route::get('solicitud/word/{id}', 'SolicitudController@generadorWord')->name('solicitud.word');
		Route::get('solicitud/pdf/{id}', 'SolicitudController@generadorPDF')->name('solicitud.pdf');
		Route::get('solicitud/partida', 'SolicitudController@partidas');
		Route::get('solicitud/{id}', 'SolicitudController@porTipo');
		Route::get('solicitud/partidas/{id}', 'SolicitudController@porPartida');
		
		Route::post('solicitud/agregarSolicitud','SolicitudController@añadirSolicitud');
		//Route::resource('notasinternas','NotaInternaController',['names' => [
		//	'store' => 'notainterna.store',
		//	'index' => 'notainterna.index',
		//	'show' => 'notainterna.show',
		//]]);


		Route::resource('administrator/supplier','AdministratorController\SupplierController',['names' => [
			'store' => 'administrator.supplier.store',
			'index' => 'administrator.supplier.index',
			'show' => 'administrator.supplier.show',
		]]);

    	Route::post('administrator/supplier/addSupplier','AdministratorController\SupplierController@addSupplier');
    	Route::post('administrator/supplier/editSupplier','AdministratorController\SupplierController@editSupplier');
		Route::post('administrator/supplier/deleteSupplier','AdministratorController\SupplierController@deleteSupplier');

		

		
		Route::resource('administrator/typematerial','AdministratorController\TypeMaterialsController',['names' => [
			'store' => 'administrator.typematerial.store',
			'index' => 'administrator.typematerial.index',
			'show' => 'administrator.typematerial.show',
		]]);

    	Route::post('administrator/typematerial/addTypeMaterial','AdministratorController\TypeMaterialsController@addTypeMaterial');
    	Route::post('administrator/typematerial/editTypeMaterial','AdministratorController\TypeMaterialsController@editTypeMaterial');
		Route::post('administrator/typematerial/deleteTypeMaterial','AdministratorController\TypeMaterialsController@deleteTypeMaterial');
		
		//Route::resource('post','PostController');
    	//Route::POST('addPost','PostController@addPost');
    	//Route::POST('editPost','PostController@editPost');
    	//Route::POST('deletePost','PostController@deletePost');
    


	});

    
});
