@extends('layouts.partials.layout')
@section('content')
<div class="box-header">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">Nuevo Usuario</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Error Block-->
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
            <!-- ./error-->  
          
                {!! Form::open(array('route' => 'users.store','method'=>'POST','class' => 'form-horizontal')) !!}
            
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nombre de Usuario:</label>
                            <div class="col-md-6">
                                {!! Form::text('username', null, array('class' => 'form-control')) !!}
                            </div>                           
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Cargo:</label>
                            <div class="col-md-6">
                                {!! Form::text('description', null, array('class' => 'form-control')) !!}
                            </div>                           
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nombre:</label>
                            <div class="col-md-6">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>                           
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email:</label>
                            <div class="col-md-6">
                                {!! Form::text('email', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>z
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Password:</label>
                            <div class="col-md-6">
                                {!! Form::password('password', array('class' => 'form-control')) !!}
                            </div>
                        </div>         
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm Password:</label>
                            <div class="col-md-6">
                                {!! Form::password('confirm-password', array('class' => 'form-control')) !!}
                            </div>
                        </div>                  
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Role:</label>
                            <div class="col-md-6">
                                {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-4">
                            <button type="submit" class="btn btn-primary">Crear</button>  
                        </div>                     
                    </div>               
                {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
      </div>
</div>
@endsection