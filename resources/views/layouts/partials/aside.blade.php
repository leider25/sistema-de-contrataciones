  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img class="round" width="160" height="160" avatar="{{ auth()->user()->name }}">
        </div>
        <div class="pull-left info">
          <p>{{ auth()->user()->username }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li ><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li ><a href="{{ route('solicitud') }}"><i class="fa fa-tasks"></i> <span>Documentos</span></a></li>
        @hasrole('Administrador')
        <li class="treeview">
          
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Administrador</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a class="nav-link" href="{{ route('administrator.usuarios.index') }}">Usuarios</a></li>
          
            <li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>
            <li><a class="nav-link" href="{{ route('roles.index') }}">Manage Role</a></li>
            <li><a class="nav-link" href="{{ route('products.index') }}">Manage Product</a></li>
            <li><a class="nav-link" href="{{ route('administrator.typematerial.index') }}">Tipo Materiales</a></li>
      
          
          </ul>
        </li>
        @endhasrole
        @hasanyrole('Administrador|rpa|Supervisor|Legal')
        <li class="treeview">
          
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Procesos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @hasanyrole('Administrador|Supervisor')
            <li><a class="nav-link" href="{{ route('supervisor.solicitud') }}"> <span>Pendientes</span> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-yellow">3</small>
                </span> </a></li>
            <li><a class="nav-link" href="{{ route('supervisor.autorizado') }}">Autorizados</a></li>
            
            @endhasanyrole
            @hasanyrole('Administrador|rpa')
            <li><a class="nav-link" href="{{ route('responsable.solicitud') }}">Validados</a></li>
            <li><a class="nav-link" href="#">Corrección</a></li> 
            <li><a class="nav-link" href="#">Concluidos</a></li>  
            @endhasanyrole
            @hasanyrole('Administrador|Legal')
            <li><a class="nav-link" href="{{ route('legal.autorizados') }}">Revisión</a></li>
            @endhasanyrole
                   
          </ul>
        </li>
        @endhasanyrole
        @hasanyrole('Administrador|Supervisor')
        <li class="treeview">
          
          <a href="#">
            <i class="fa fa-road"></i> <span>Supervisor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a class="nav-link" href="{{ route('administrator.supplier.index') }}">Proveedores</a></li>       
          </ul>
        </li>
        @endhasanyrole
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->