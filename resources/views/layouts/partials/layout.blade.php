<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('titulo', 'Contrataciones')| Sistema de Contrataciones</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{asset("bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset("bower_components/font-awesome/css/font-awesome.min.css")}}">
        <!-- iCheck for -->
        <link rel="stylesheet" href="{{asset("plugins/iCheck/all.css")}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset("bower_components/select2/dist/css/select2.min.css")}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{asset("bower_components/Ionicons/css/ionicons.min.css")}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset("dist/css/AdminLTE.min.css")}}">
        <link rel="stylesheet" href="{{asset("dist/css/letteravatar.css")}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset("dist/css/skins/skin-red.css")}}">
        <!-- jQuery 3 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script> 
        @yield('style')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
    <body class="hold-transition skin-red fixed sidebar-mini">
        @include("layouts/partials/header")
        @include("layouts/partials/aside")
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Content Wrapper. Contains page content -->
            

                    @yield('content')

               
            @include("layouts/partials/footer")
        </div>
       
        
            <!-- Bootstrap 3.3.7 -->
            <script src="{{asset("bower_components/bootstrap/dist/js/bootstrap.min.js")}}"></script>
            <script src="{{asset("bower_components/select2/dist/js/select2.full.min.js")}}"></script>
            <!-- SlimScroll -->
            <script src="{{asset("bower_components/jquery-slimscroll/jquery.slimscroll.min.js")}}"></script>
            <!-- FastClick -->
            <script src="{{asset("bower_components/fastclick/lib/fastclick.js")}}"></script>
            <!-- iCheck 1.0.1 -->
            <script src="{{asset("plugins/iCheck/icheck.min.js")}}"></script>
            <!-- AdminLTE App -->
            <script src="{{asset("dist/js/adminlte.min.js")}}"></script>
            <script src="{{asset("dist/js/letteravatar.js")}}"></script>
            
         
            @yield('script')
    </body>
</html>