@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bienvenido
      <small>Usuarios</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="#">Administrador</li>
      <li class="active">Usuarios</li>
    </ol>
  </section>

<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Proveedores</h3>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <thead>
                  <tr>
                    <th width="150px">No</th>
                    <th>Descripción</th>
                    <th>Dirección</th>
                    <th>Creado En</th>
                    <th class="text-center" width="150px">
                      <a href="#" class="create-modal btn btn-success btn-sm">
                        <i class="glyphicon glyphicon-plus"></i>
                      </a>
                  </th>
                </thead>
              </tr>
              {{ csrf_field() }}
              <?php  $no=1; ?>
              @foreach ($proveedor as $value)
                <tr class="post{{$value->id}}">
                  <td>{{ $no++ }}</td>
                  <td>{{ $value->descripcion }}</td>
                  <td>{{ $value->direccion }}</td>
                  <td>{{ $value->created_at }}</td>
                  <td>
                    <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                      <i class="fa fa-eye"></i>
                    </a>
                    <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                      <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                      <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          {{$proveedor->links()}}
        </div>
    </div>  
  </div>
</section>
</div>




{{-- Modal Form Create Supplier --}}
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal supplier-form" role="form" >
          <div class="form-group">
              <div class="col-sm-12">
                  <label for="title">Empresa</label>
                  <input type="text" class="form-control" id="title1" name="title1"
                  placeholder="Ingrese el Nombre" required>
                  <p class="error text-center alert alert-danger hidden"></p>
                </div>
          </div>

            <div class="form-group">
              <div class="col-sm-12">
                <label for="title">Dirección</label>
                <input type="text" class="form-control" id="direction" name="direction"
                placeholder="Ingrese la Diección" required>
                <p class="error text-center alert alert-danger hidden"></p>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label>Encargado</label>
                <input type="text" class="form-control" id="encargado" name="encargado"
                placeholder="Ingrese el nombre completo..." required>
                <p class="errorCite text-center alert alert-danger hidden"></p>
              </div>
              <div class="col-sm-6">
                <label>Cargo</label>
                <input type="text" class="form-control" id="cargo" name="cargo"
                placeholder="Ejemplo: Encargado..." required>
                <p class="errorCite text-center alert alert-danger hidden"></p>
              </div>
            </div>
          <div class="form-group">
              <label class="control-label col-sm-2">Referencias</label>
          </div>
          <div class="form-group">
              <div class="col-sm-4">
                  <select class="form-control" name="sel-1">
                    <option disabled selected>Seleccione</option>
                    @foreach( $tipofererencia as $key => $value )
                       <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="col-sm-6">
                  <input type="text" class="form-control" id="body" name="inp-1"
                  placeholder="Ingrese su referencia" required>
                  <p class="error text-center alert alert-danger hidden"></p>
              </div>
              <div class="col-sm-2">
                  <button class="btn btn-primary" type="submit" id="more" title="Añadir">
                      <span class="glyphicon glyphicon-plus"></span>
                    </button>
              </div>
            </div>

        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="submit" id="add">
              </span>Registrar
            </button>
            <button class="btn btn-danger" type="button" data-dismiss="modal">
              </span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div></div>
{{-- Modal Form Show Type Material --}}
<div id="show" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
                  </div>
                    <div class="modal-body">
                    <div class="form-group">
                      <label for="">ID :</label>
                      <b id="i"/>
                    </div>
                    <div class="form-group">
                      <label for="">Descripción :</label>
                      <b id="ti"/>
                    </div>
                    </div>
                    </div>
                  </div>
</div>
{{-- Modal Form Edit and Delete Type Material --}}
<div id="myModal"class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2"for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="fid" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="title">Descripción</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="t">
            </div>
          </div>
        </form>
                {{-- Form Delete Type Material --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="title"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
{{-- ajax Form Add Type Supplier--}}
$(document).ready( function () {
var no = "<?php echo $no;?>";
var views_name =  new Array();
var i=1; 
views_name=[{id: 1,select: "sel-1",input: "inp-1"}];
var typereference = @json($tipofererencia); 
        $('#table').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 20,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
        });
  


        $('#more').click(function(){
          i++;
          var html='<div class="form-group" id="'+i+'">'+
            '<div class="col-sm-4">'+
                '<select class="form-control" name="sel-'+i+'">'+
                '<option disabled selected>Seleccione</option>';
          $.each(typereference, function(index, value) {
            html+="<option value='"+index+"'>"+value+"</option>";
          });
          html+='</select>'+
              '</div>'+
              '<div class="col-sm-6">'+
                  '<input type="text" class="form-control" name="inp-'+i+'" id="body"'+
                  'placeholder="Ingrese su referencia" required>'+
                  '<p class="error text-center alert alert-danger hidden"></p>'+
              '</div>'+
              '<div class="col-sm-2">'+
                  '<button class="btn btn-danger remove_button" type="submit" title="Eliminar">'+
                      '<span class="glyphicon glyphicon-minus"></span>'+
                    '</button>'+
              '</div>'+
            '</div>';
                  $('.supplier-form').append(html);
                  views_name.push({
                  id: i,
                  select: "sel-"+i,
                  input: "inp-"+i
                });
                console.log(views_name);
           });
      

        $('.supplier-form').on("click",".remove_button", function(e){
          e.preventDefault(); 
          $(this).parent().parent('div').remove(); 
          console.log(i);
          console.log($(this).parent().parent('div').prop("id"));
          var id_view=$(this).parent().parent('div').prop("id");
          for (index=0; index < views_name.length; index++) {
            if(views_name[index].id == id_view ){
             console.log(views_name[index].id+" es igual a "+id_view);
             views_name.splice(index, 1);
             console.log(views_name);
            }
          }
        });
        
      // function Add Type Suppliers
  $(document).on('click','.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Añadir Proveedor');
  });
  $("#add").click(function() {
    //console.log($('input[name=title]').val());
    //console.log($('input[name=direction]').val());
    var views_result =  new Array();
    for (index=0; index < views_name.length; index++){
      views_result.push({id_type: document.getElementsByName(views_name[index].select)[0].value,
                                reference: $('input[name='+views_name[index].input+']').val()});
    }
 //   console.log(views_result);
    var formdata=new FormData();
    formdata.append('_token',$('input[name=_token]').val());
    formdata.append('decripcion',$('input[name=title1]').val());
    formdata.append('direccion',$('input[name=direction]').val());
    formdata.append('encargado',$('input[name=encargado]').val());
    formdata.append('cargo',$('input[name=cargo]').val());
    formdata.append('array',JSON.stringify(views_result));

    $.ajax({
      type: 'POST',
      url: '/administrator/supplier/addSupplier',
      data: formdata,
      dataType: "json",
      contentType: false,
      processData: false,
      success: function(data){
        if ((data.errors)) {
          console.log(data);
          $('.error').removeClass('hidden');
          $('.error').text(data.errors.title);
        }else{
         
          console.log(data);
         $('.error').remove();
         $('#table').append("<tr class='post" + data.id + "'>"+
         "<td>" + no + "</td>"+
         "<td>" + data.descripcion + "</td>"+
         "<td>" + data.direccion + "</td>"+
         "<td>" + data.created_at + "</td>"+
         "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
         "</tr>");
        }
      },
    });
    no++;
    //$('#title').val('');
    //$('#direction').val('');
    });
  });




// function Edit Type Materials
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update Post");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Post Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#fid').val($(this).data('id'));
    $('#t').val($(this).data('description'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.edit', function() {
  $.ajax({
    type: 'POST',
    url: '/administrator/supplier/editSupplier',
    data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'title': $('#t').val()
    },
success: function(data) {
      $('.post' + data.id).replaceWith(" "+
      "<tr class='post" + data.id + "'>"+
      "<td>" + data.id + "</td>"+
      "<td>" + data.descripcion + "</td>"+
      "<td>" + data.created_at + "</td>"+
      "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.descripcion + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.descripcion + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.descripcion + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
      "</tr>");
    }
  });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-success');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function(){
  $.ajax({
    type: 'POST',
    url: '/administrator/supplier/deleteSupplier',
    data: {
      '_token': $('input[name=_token]').val(),
      'id': $('.id').text()
    },
    success: function(data){
       $('.post' + $('.id').text()).remove();
    }
  });
});

  // Show function
  $(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#ti').text($(this).data('description'));
    $('.modal-title').text('Show Post');
  });
</script>
@endsection
