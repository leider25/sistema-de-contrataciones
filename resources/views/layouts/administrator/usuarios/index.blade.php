@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bienvenido
      <small>Usuarios</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="#">Administrador</li>
      <li class="active">Usuarios</li>
    </ol>
  </section>

<section class="content">
<div class="col-xs-12">
  <div class="box box-info">
      <div class="box-header">
          <h3 class="box-title">Usuarios</h3>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nombre Usuario</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
            


            @foreach ($data as $key => $user)
            @php 
            $departamento=App\Departamento::find($user->department_id);
            $superior=App\User::find($user->depend);
            @endphp
              <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                  @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                       <label class="badge badge-success">{{ $v }}</label>
                    @endforeach
                  @endif
                </td>
                <td>
                   <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$user->id}}" 
                                                                      data-cargo="{{$user->description}}"
                                                                      data-nombre="{{$user->name}}"
                                                                      data-departamento="{{$departamento->name}}"
                                                                      data-superior="{{$superior->name}}">
                    <i class="fa fa-eye"></i></a>

                   <a href="#" class="edit-modal btn btn-primary btn-sm" data-id="{{$user->id}}" 
                                                                         data-cargo="{{$user->description}}"
                                                                         data-nombre="{{$user->name}}"
                                                                         data-departamento="{{$departamento->name}}">
                    <i class="glyphicon glyphicon-pencil"></i></a>
    
                </td>
                  
              </tr>
              @endforeach
          </table>
          {!! $data->render() !!}
        </div>
      
      </div>
  </div>  
</div>
</section>
</div>


{{-- Modal Ver Usuario--}}
<div id="show" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Large Modal</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal ">
        <div class="form-group">
          <div class="col-sm-6">
            <label>Nombre :</label>
            <span id="nombre"/>
          </div>
          <div class="col-sm-6">
            <label>Superior :</label>
            <span id="superior"/>
            </div>
        </div>

        <div class="form-group">
          <div class="col-sm-6">
            <label>Área :</label>
            <span id="area"/>  
          </div>
          <div class="col-sm-6">
            <label>Cargo :</label>
            <span id="cargo"/>
          </div>
        </div>

      </div>
    </form>
      <div class="modal-footer ">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

{{-- Modal Form Edit and Delete Type Material --}}
<div id="myModal"class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">

        <form class="form-horizontal" role="modal">
          <h4><strong>Datos: Sistema de Asisténcia</strong></h4>
          <div class="form-group">
            <div class="col-sm-12">
              <label>Nombre :</label>
              <span id="unombre"/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <label>Area :</label>
              <span id="uarea"/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <label>Cargo :</label>
              <span id="ucargo"/>
            </div>
          </div>

          <h4><strong>Asignación de Datos</strong></h4>
          
          
          <div class="form-group">
            <div class="col-sm-6">
              <label>Roles</label><br>
              <select class="form-control" name="uroles" id="uroles" >
            
              </select>
            </div>
            @php
             $areas=App\Area::all();   
            @endphp  
            <div class="col-sm-6">
              <label>Área</label><br>
              <select class="form-control select2" style="width: 100%;">
                @foreach( $areas as $value )
                  <option value="{{ $value->id }}">{{ $value->alias." - ".$value->nombre }}</option>
                @endforeach
              </select>
            </div> 
          </div>           
   

        </form>


      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div></div>


<script type="text/javascript">

  $(document).ready( function () { 
    var roles = @json($roles); 
    $('.select2').select2();

  $('#table').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'info'        : true,
    'autoWidth'   : false,
    "pageLength": 20,
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
  }
  });   

  // Show function
  $(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#nombre').text($(this).data('nombre'));
    $('#area').text($(this).data('departamento'));
    $('#cargo').text($(this).data('cargo'));
    $('#superior').text($(this).data('superior'));
    //$('#funcionario').text($(this).data('funcionario'));
    $('.modal-title').text('Mostrar Usuario');
  });

  $(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text("Asignar Rol");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Post Edit');
   
    $('.form-horizontal').show();
    $('#myModal').modal('show');
    $('#unombre').text($(this).data('nombre'));
    $('#uarea').text($(this).data('departamento'));
    $('#ucargo').text($(this).data('cargo'));

    var url='/administrator/usuario/rol/'+$(this).data('id');

    $.get(url, function(data){
       
          console.log(data);
          var rol='';
          for (let i = 0; i < roles.length; i++) {
          if (data.length==0) {
            rol+='<option value="'+roles[i].id+'">'+roles[i].id+' - '+roles[i].name+'</option>';
          }else{
            for (let j = 0; j < data.length; j++) {
              if(roles[i].id==data[j]){
                rol+='<option value="'+roles[i].id+'" selected>'+roles[i].id+' - '+roles[i].name+'</option>';
              }else{
                rol+='<option value="'+roles[i].id+'">'+roles[i].id+' - '+roles[i].name+'</option>';
              } 
            }
          }
          
        }
         
          $("#uroles").html(rol);

        });
   
  });

  $('.modal-footer').on('click', '.edit', function() {
  $.ajax({
    type: 'POST',
    url: '/administrator/typematerial/editTypeMaterial',
    data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'title': $('#t').val()
    },
  success: function(data) {
        $('.post' + data.id).replaceWith(" "+
        "<tr class='post" + data.id + "'>"+
        "<td>" + data.id + "</td>"+
        "<td>" + data.descripcion + "</td>"+
        "<td>" + data.created_at + "</td>"+
        "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
        "</tr>");
      }
    });
  });



  //$('#uroles').select2();
    
  });
  
  </script>
@endsection 