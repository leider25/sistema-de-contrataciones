@extends('layouts.partials.layout')
@section('content')

<div class="col-xs-12">
  <div class="box box-info">
      <div class="box-header">
          <h3 class="box-title">Tipo de Materiales</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="table">
              <thead>
                <tr>
                  <th width="150px">No</th>
                  <th>Descripción</th>
                  <th>Creado En</th>
                  <th class="text-center" width="150px">
                    <a href="#" class="create-modal btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </th>
              </thead>
            </tr>
            {{ csrf_field() }}
            <?php  $no=1; ?>
            @foreach ($typematerials as $value)
              <tr class="post{{$value->id}}">
                <td>{{ $no++ }}</td>
                <td>{{ $value->descripcion }}</td>
                <td>{{ $value->created_at }}</td>
                <td>
                  <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                    <i class="glyphicon glyphicon-pencil"></i>
                  </a>
                  <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}" data-description="{{$value->descripcion}}">
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
        {{$typematerials->links()}}
      </div>
  </div>  
</div>

{{-- Modal Form Create Type Material --}}
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group row add">
            <label class="control-label col-sm-4">Descripción:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" 
              placeholder="Ingrese el Nombre de Material" required>
              <p class="error text-center alert alert-danger hidden"></p>
            </div>
          </div>
        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="add">
              <span class="glyphicon glyphicon-plus"></span>Save Post
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remobe"></span>Close
            </button>
          </div>
    </div>
  </div>
</div></div>
{{-- Modal Form Show Type Material --}}
<div id="show" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
                  </div>
                    <div class="modal-body">
                    <div class="form-group">
                      <label for="">ID :</label>
                      <b id="i"/>
                    </div>
                    <div class="form-group">
                      <label for="">Descripción :</label>
                      <b id="ti"/>
                    </div>
                    </div>
                    </div>
                  </div>
</div>
{{-- Modal Form Edit and Delete Type Material --}}
<div id="myModal"class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2"for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="fid" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2"for="title">Descripción</label>
            <div class="col-sm-10">
            <input type="name" class="form-control" id="t">
            </div>
          </div>
        </form>
                {{-- Form Delete Type Material --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="title"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
{{-- ajax Form Add Type Material--}}
$(document).ready( function () {
        $('#table').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 20,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
        });

    } );
  $(document).on('click','.create-modal', function() {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add Post');
  });
  $("#add").click(function() {
    $.ajax({
      type: 'POST',
      url: '/administrator/typematerial/addTypeMaterial',
      data: {
        '_token': $('input[name=_token]').val(),
        'title': $('input[name=title]').val()
      },
      success: function(data){
        console.log("aquiiii");
        if ((data.errors)) {
          console.log("failed");
          $('.error').removeClass('hidden');
          $('.error').text(data.errors.title);
        } else {
          console.log("success");
          $('.error').remove();
          $('#table').append("<tr class='post" + data.id + "'>"+
          "<td>" + data.id + "</td>"+
          "<td>" + data.descripcion + "</td>"+
          "<td>" + data.created_at + "</td>"+
          "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.descripcion + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.descripcion + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
          "</tr>");
        }
      },
    });
    $('#title').val('');
    $('#body').val('');
  });

// function Edit Type Materials
$(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update Post");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Post Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#fid').val($(this).data('id'));
    $('#t').val($(this).data('description'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.edit', function() {
  $.ajax({
    type: 'POST',
    url: '/administrator/typematerial/editTypeMaterial',
    data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#fid").val(),
            'title': $('#t').val()
    },
success: function(data) {
      $('.post' + data.id).replaceWith(" "+
      "<tr class='post" + data.id + "'>"+
      "<td>" + data.id + "</td>"+
      "<td>" + data.descripcion + "</td>"+
      "<td>" + data.created_at + "</td>"+
      "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.descripcion + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
      "</tr>");
    }
  });
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-success');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function(){
  $.ajax({
    type: 'POST',
    url: '/administrator/typematerial/deleteTypeMaterial',
    data: {
      '_token': $('input[name=_token]').val(),
      'id': $('.id').text()
    },
    success: function(data){
       $('.post' + $('.id').text()).remove();
    }
  });
});

  // Show function
  $(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#i').text($(this).data('id'));
    $('#ti').text($(this).data('description'));
    $('.modal-title').text('Show Post');
  });
</script>
@endsection
