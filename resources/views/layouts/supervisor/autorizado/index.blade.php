@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bienvenido
      <small>Especificación Técnica</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="#">Procesos</li>
      <li class="active">Autorizados</li>
    </ol>
  </section>
<section class="content">
<div class="col-xs-12">
  <div class="box box-solid box-warning">
      <div class="box-header">
          <h3 class="box-title">Solicitud de Contrataciones</h3>
      </div>
      <!--FILTRAMOS SOLICITUDES APROBADAS-->
      @php 
      $solicituds=App\Solicitud::with('tipo','estados')->get();
      $solicitudes=array();
      foreach ($solicituds as $value) {
        $coleccion=$value->estados;
        foreach ($coleccion as $item){
          if ($item->pivot->activo==1){
            if ( in_array($item->id,[4,7,10,11])){
              array_push($solicitudes,$value);
            }
          }
        }
      } 
      @endphp
      <!--TABLA-->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="table">
              <thead>
                <tr class="text-center warning">
                  <th>No</th>
                  <th>Área</th>
                  <th>Funcionario</th>
                  <th>Estado</th>
                  <th>Objeto de Contratación</th>
                  <th>Objetivo</th>
                  <th WIDTH="50">Fecha</th>
                  <th class="bg-yellow text-center">Motivo</th>
                  <th>Archivos</th>
                  <th class="text-center" width="150px">
                    Accion
                </th>
              </thead>
            </tr>
            {{ csrf_field() }}
            @php  $no=1;@endphp

            @foreach ($solicitudes as $value)
            
            @php 
            $usuario=App\User::where('id',$value->usuario_id)->first();
            $area_id=DB::connection("contrataciones")->table('area_user')->select('area_id')->where('usuario_id','=',$usuario->id)->first();
            $area=App\Area::where('id',$area_id->area_id)->first();

             @endphp

              <tr>
                <td>{{ $no++ }}</td>
                <!--AREA-->
                <td>{{ $area->nombre}}</td>
                <!--NOMBRE USUARIO-->
                <td>{{ $usuario->name}}</td>
                <!--ESTADO-->
                @php $coleccion=$value->estados @endphp
                @foreach ($coleccion as $item)
                  @if ($item->pivot->activo==1)
                    @if ( $item->id==1 )
                      <td><span class="label label-info">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[2,11] ))
                      <td><span class="label label-warning">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[4,7] ) )
                      <td><span class="label label-success">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[3,9] ) )
                    <td><span class="label label-primary">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[5,10] ) )
                    <td><span class="label label-danger">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[6,8] ) )
                    <td><span class="label bg-maroon">{{$item->descripcion}}</span></td>
                    @endif
                  @endif  
                @endforeach
                <!--OBJETO-->
                <td>{{ $value->objeto}} <br> <b>{{ $value->tipo->nombre }} </b></td>
                <!--OBJETIVO-->
                <td>{{ $value->objetivo }}</td>
                <!--FECHA-->
                <td>{{ $value->fecha }}</td>
                <!--MOTIVO-->
                @php $adjudicacion=App\Adjudicacion::with('solicitud','proveedor')->where('solicitud_id',$value->id)->first() @endphp
                @foreach ($coleccion as $item)
                  @if ($item->pivot->activo==1)

                  <td class="warning text-center">{{$item->pivot->motivo}}</td>
                  @endif
                      
                @endforeach
                <!--DOCUMENTOS-->
                <td>
                  @if (isset($adjudicacion))
                    @if (isset($adjudicacion->documento))
                    <!--ADJUDICACION Y DOCUMENTOS DEL PROVEEDOR-->
                    <a href="#" class="documentos-cuatro-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                      data-objeto="{{$value->objeto}}"
                      data-esp="{{$value->especificaciontecnica_id}}"
                      data-idadju="{{$adjudicacion->id}}"
                      @foreach ($coleccion as $item)
                        @if ($item->pivot->activo==1)
                        data-estado="{{$item->id}}"> 
                        @endif
                      @endforeach
                      <i class="fa fa-eye"></i>
                    </a>&nbsp
                    @else
                    <!--SOLO ADJUDICACION-->
                    <a href="#" class="documentos-tres-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                      data-objeto="{{$value->objeto}}"
                      data-esp="{{$value->especificaciontecnica_id}}"
                      @foreach ($coleccion as $item)
                        @if ($item->pivot->activo==1)
                        data-estado="{{$item->id}}"> 
                        @endif
                      @endforeach
                      <i class="fa fa-eye"></i>
                    </a>&nbsp
                    @endif
                  @else
                  <!--NINGUNO DE LOS DOS-->
                  <a href="#" class="documentos-dos-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                    data-objeto="{{$value->objeto}}"
                    data-esp="{{$value->especificaciontecnica_id}}"
                    @foreach ($coleccion as $item)
                      @if ($item->pivot->activo==1)
                      data-estado="{{$item->id}}"> 
                      @endif
                    @endforeach
                    <i class="fa fa-eye"></i>
                  </a>&nbsp
                  @endif

                  <!--BOTONES PARA ANIADIR NOTA DE ADJUDICACION-NOTA DE ADJUDICACION -->
                  @if (isset($adjudicacion))
                    @if (!isset($adjudicacion->documento))
                   <a href="#" class="doc-modal btn btn-success btn-sm" data-id="{{$value->id}}"><i class="fa fa-plus"></i></a>
                    @endif
                  @else
                  <a href="#" class="adjudicacion-modal btn btn-success btn-sm" data-id="{{$value->id}}"><i class="fa fa-plus"></i></a>
                  @endif
                </td>
                <td align="center">

                  @foreach ($coleccion as $item)
                  @if ($item->pivot->activo==1)
                    @if (in_array($item->id,[4,11]) && isset($adjudicacion->documento) )
                    <a href="#" class="show-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                      data-objeto="{{$value->objeto}}" 
                      data-objetivo="{{$value->objetivo}}"
                      data-area="{{$area->nombre}}"
                      data-funcionario="{{$usuario->name}}"> 
                      <i class="fa fa-send"></i>
                    </a>
                    @else
                    <a href="#" class="show-modal btn btn-warning btn-sm disabled" data-id="{{$value->id}}" 
                      data-objeto="{{$value->objeto}}" 
                      data-objetivo="{{$value->objetivo}}"
                      data-area="{{$area->nombre}}"
                      data-funcionario="{{$usuario->name}}"> 
                      <i class="fa fa-send"></i>
                    </a>
                    @endif
                  @endif
                  @endforeach
                  
                  
              </tr>
              @endforeach
          </table>
        </div>
      
      </div>
  </div>  
</div>
</section>
</div>
{{-- Modal Form enviar Solicitud --}}
<div id="show" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row"><label for="">ID :</label></th>
                        <td id="id"></td>
                      </tr>
                      
                      <tr>
                        <th scope="row"><label for="">OBJETO :</label></th>
                        <td id="objeto"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">OBJETIVO :</label></th>
                        <td id="objetivo"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">ÁREA :</label></th>
                        <td id="area"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">FUNCIONARIO :</label></th>
                        <td id="funcionario"></td>
                      </tr>
                    </tbody>
                  </table>
                  <form class="form-horizontal">
                    <div class="form-group" id="div-motivo">  
                      <div class="col-sm-12">
                            <label for="title">Motivo</label>
                            <input type="text" name="motivo" id="motivo" class="form-control" placeholder="Ingrese el motivo" />
                      </div>
                    </div>
                  </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="submit" id="btnLegal">
                  </span>Enviar a Legal
                </button>
                <button class="btn btn-danger" type="submit" data-dismiss="modal">
                    </span>cerrar
                  </button>
            </div>
      </div>
    </div>
  </div>
  {{-- Modal Form generar nota de adjudicación--}}
<div id="show-adju" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      <form  class="form-horizontal request-form" role="form" enctype="multipart/form-data" id="formSol">
        <h5><b>AJUDICACIÓN</b></h5>
        <div class="form-group">
          <div class="col-sm-6">
            <label>Importe</label>
            <input type="number" class="form-control" id="importe" name="importe"
            placeholder="Monto (BS)" required>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
          <div class="col-sm-6">
            <label>Fecha</label>
            <input type="date" class="form-control" id="fechanota" name="fechanota" required>
            <p class="errorFecha text-center alert alert-danger hidden"></p>
          </div>
        </div>
        <h5><b>PROVEEDOR</b></h5>
        <div class="form-group">
          <div class="col-sm-12">
            @php $proveedores=App\Proveedor::all() @endphp
            <label>Empresa</label><br>
            <select class="form-control select2" style="width: 100%;" name="select-proveedor" id="select-proveedor">
              <option disabled selected>Selecciona una opción</option>
              @foreach( $proveedores as $value )
                <option value="{{ $value->id }}">{{ $value->id." - ".$value->descripcion }}</option>
              @endforeach
            </select>
          </div>

          
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label>Dirección</label>
            <input type="text" class="form-control" id="direccion" name="direccion"
            placeholder="Ingrese la dirección..." disabled>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6">
            <label>Teléfono fijo</label>
            <input type="number" class="form-control" id="fijo" name="fijo"
            placeholder="Ingrese el teléfono..." disabled>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
          <div class="col-sm-6">
            <label>Celular</label>
            <input type="number" class="form-control" id="celular" name="celular"
            placeholder="Ingrese el número..." disabled>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6">
            <label>Encargado</label>
            <input type="text" class="form-control" id="encargado" name="encargado"
            placeholder="Ingrese el nombre completo..." disabled>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
          <div class="col-sm-6">
            <label>Cargo</label>
            <input type="text" class="form-control" id="cargo" name="cargo"
            placeholder="Ejemplo: Encargado..." disabled>
            <p class="errorCite text-center alert alert-danger hidden"></p>
          </div>
        </div>
        
          

      </form>
      </div>
      <div class="modal-footer">
          <button class="btn btn-primary" type="submit" id="btnNota">
            </span>Generar Nota
          </button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">
            <span class="glyphicon glyphicon-remobe"></span>Cerrar
          </button>
      </div>
    </div>
  </div>
</div>


{{-- Modal Form Subir documentacion de Proveedores--}}
<div id="show-doc" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      <form  class="form-horizontal request-form" role="form" enctype="multipart/form-data" id="formDoc">
       
  
        <div class="form-group">
          <div class="col-sm-12">   
            <label for="cotizacion">Documentación</label>
            <input type="file" class="form-control-file" id="docs" name="docs">
            <p class="errorCotizacion text-center alert alert-danger hidden"></p>
          </div>
        </div> 
          
      </form>
      </div>
      <div class="modal-footer">
          <button class="btn btn-primary" type="submit" id="btnSubir">
            </span>Subir
          </button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">
            <span class="glyphicon glyphicon-remobe"></span>Cerrar
          </button>
      </div>
    </div>
  </div>
</div>

{{-- Modal Form Show dcumentos dos --}}
<div id="show-documentos-dos" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="dos-modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="dos-titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="dos-sol-id"></td>
                  <td ><b id="dos-sol-detalle"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="dos-coti-id"></td>
                  <td><b id="dos-coti-detalle"></b></td>
                </tr>
                <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>3</b></th>
                  <td id="dos-esp-id"></td>
                  <td><b id="dos-esp-detalle"></b></td>
                </tr>
                 <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>4</b></th>
                  <td id="dos-aut-id"></td>
                  <td><b id="dos-aut-detalle"></b></td>
                </tr>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Show dcumentos TRES --}}
<div id="show-documentos-tres" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="tres-modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="tres-titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="tres-sol-id"></td>
                  <td ><b id="tres-sol-detalle"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="tres-coti-id"></td>
                  <td><b id="tres-coti-detalle"></b></td>
                </tr>
                <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>3</b></th>
                  <td id="tres-esp-id"></td>
                  <td><b id="tres-esp-detalle"></b></td>
                </tr>
                 <!-- NOTA DE AUTORIZACION -->
                <tr>
                  <th scope="row"><b>4</b></th>
                  <td id="tres-aut-id"></td>
                  <td><b id="tres-aut-detalle"></b></td>
                </tr>
                <!-- NOTA DE ADJUDICACION-->
                <tr>
                  <th scope="row"><b>5</b></th>
                  <td id="tres-adju-id"></td>
                  <td><b id="tres-adju-detalle"></b></td>
                </tr>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>

{{-- Modal Form Show dcumentos CUATRO --}}
<div id="show-documentos-cuatro" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="cuatro-modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="cuatro-titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="cuatro-sol-id"></td>
                  <td ><b id="cuatro-sol-detalle"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="cuatro-coti-id"></td>
                  <td><b id="cuatro-coti-detalle"></b></td>
                </tr>
                <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>3</b></th>
                  <td id="cuatro-esp-id"></td>
                  <td><b id="cuatro-esp-detalle"></b></td>
                </tr>
                 <!-- NOTA DE AUTORIZACION -->
                <tr>
                  <th scope="row"><b>4</b></th>
                  <td id="cuatro-aut-id"></td>
                  <td><b id="cuatro-aut-detalle"></b></td>
                </tr>
                <!-- NOTA DE ADJUDICACION-->
                <tr>
                  <th scope="row"><b>5</b></th>
                  <td id="cuatro-adju-id"></td>
                  <td><b id="cuatro-adju-detalle"></b></td>
                </tr>
                <!-- DOCUMENTOS DEL PROVEEDOR -->
                <tr>
                  <th scope="row"><b>6</b></th>
                  <td id="cuatro-provee-id"></td>
                  <td><b id="cuatro-provee-detalle"></b></td>
                </tr>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>

  
<script type="text/javascript">
    {{-- ajax Form Add Type Supplier--}}
  $(document).ready( function () {
    $('.select2').select2();  
    var no = "<?php echo $no;?>";
    var idsol;

    console.log(no);
  // var checks =  new Array();
  var array_check=[];
  //var i=1; 
  //views_name=[{id: 1,select: "sel-1",input: "inp-1"}];
  //Flat red color scheme for iCheck  
  $('#table').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'info'        : true,
    'autoWidth'   : false,
    "pageLength": 20,
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
  }
  });   

  // Show function
  $(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#id').text($(this).data('id'));
    $('#objeto').text($(this).data('objeto'));
    $('#objetivo').text($(this).data('objetivo'));
    $('#area').text($(this).data('area'));
    $('#funcionario').text($(this).data('funcionario'));
    $('.modal-title').text('Solicitud');

    idsol=$(this).data('id');
  });

   // Show function
   $(document).on('click', '.adjudicacion-modal', function() {
    $('#show-adju').modal('show');
    ///$('#id').text($(this).data('id'));
    ///$('#objeto').text($(this).data('objeto'));
    ///$('#objetivo').text($(this).data('objetivo'));
    ///$('#area').text($(this).data('area'));
    ///$('#funcionario').text($(this).data('funcionario'));
    $('.modal-title').text('Generar nota de adjudicación');

     idsol=$(this).data('id');
  });

   // Show function
   $(document).on('click', '.doc-modal', function() {
    $('#show-doc').modal('show');
    $('.modal-title').text('Documentación del Proveedor');
    idsol=$(this).data('id');
  });


  $( "#btnLegal" ).click(function() {
    var formdata=new FormData();
    formdata.append('_token',$('input[name=_token]').val());
    formdata.append('motivo',$('input[name=motivo]').val());
    formdata.append('id',idsol);

    console.log(idsol);
    $.ajax({
     url:'/supervisor/autorizacion/enviar',
     type:'post',
     data:formdata,
     contentType: false,
     processData: false,
     dataType:'json',
     success:function(data)
     {
      console.log(data);
      location.reload();
     }
    });
  });

  $( "#btnCorregir" ).click(function() {
    var formdata=new FormData();
    formdata.append('_token',$('input[name=_token]').val());
    formdata.append('id',idsol);

    console.log(idsol);
    $.ajax({
     url:'/supervisor/solicitud/corregir',
     type:'post',
     data:formdata,
     contentType: false,
     processData: false,
     dataType:'json',
     success:function(data)
     {
      console.log(data);
      location.reload();
     }
    });
  });

  $("#select-proveedor").change(function(){
        var categoria = $(this).val();
        console.log(categoria);

      //  $('input[name=cite]').value=;

      //  disabledtipo2(categoria);
      //  
        $.get('/supervisor/autorizado/empresa/'+categoria, function(data){
        //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
          console.log(data);
          document.getElementById('direccion').value = data.direccion;
          document.getElementById('encargado').value = data.encargado.nombre;
          document.getElementById('cargo').value = data.encargado.cargo;

          var referencias=data.referencia;
          $.each(referencias, function(index, value) {
            if (value.tiporeferencia_id==1) {
              document.getElementById('celular').value = value.descripcion;
            }

            if (value.tiporeferencia_id==2) {
              document.getElementById('fijo').value = value.descripcion
            }
                        
          });
            //var producto_select = '<option disabled selected>Seleccione subcategoria</option>'
            //  for (var i=0; i<data.length;i++)
            //    producto_select+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
//
            //  $("#type2").html(producto_select);

        });
      });

      $( "#btnNota" ).click(function() {
        console.log(idsol);
        var url='/supervisor/autorizado/nota/'+idsol;
        var formdata=new FormData();
        formdata.append('_token',$('input[name=_token]').val());
        formdata.append('importe',$('input[name=importe]').val());
        formdata.append('fecha',$('input[name=fechanota]').val());
        formdata.append('select',document.getElementsByName("select-proveedor")[0].value);

     

        
        $.ajax({
         url:url,
         type:'post',
         data:formdata,
         contentType: false,
         processData: false,
         dataType:'json',
         success:function(data)
         {
          console.log(data);
          location.reload();
         }
        });


      });
  
      $("#btnSubir").click(function() {
      console.log($("input[type='file']")[0].files[0]);

      var formdata=new FormData();
      formdata.append('_token',$('input[name=_token]').val());
      formdata.append('archivo',$("input[type='file']")[0].files[0]);
      formdata.append('idsol',idsol);


      $.ajax({
        type: 'POST',
        url: '/supervisor/autorizacion/documentos',
        data:formdata,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(data){
           if ((data.errors)) {

            console.log(data);
             
           }else{
          
             console.log(data);
             location.reload();

        }
       },
      });       
      
      });

  $(document).on('click', '.documentos-dos-modal', function() {
  idsol=$(this).data('id');
  idesp=$(this).data('esp');
  console.log(idsol);
  console.log(idesp);
  $('#show-documentos-dos').modal('show');
  $('#dos-titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  $('#dos-sol-id').html("<a href='/solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-sol-detalle').text("Solicitud");
  /* COTIZACION */
  $('#dos-coti-id').html("<a href='/solicitud/cotizacion/"+idsol+"'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-coti-detalle').text("Cotización");
  /* ESPECIFICACION  */
  $('#dos-esp-id').html("<a href='/solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-esp-detalle').text("Especificación Técnica");
  /*NOTA DE AUTORIZACION*/
  $('#dos-aut-id').html("<a href='/supervisor/pdf/autorizacion/"+idsol+"'><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-aut-detalle').text("Nota de Autorización");
  $('.dos-modal-title').text('Documentos');
  });

  $(document).on('click', '.documentos-tres-modal', function() {
  idsol=$(this).data('id');
  idesp=$(this).data('esp');
  console.log(idsol);
  console.log(idesp);
  $('#show-documentos-tres').modal('show');
  $('#tres-titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  $('#tres-sol-id').html("<a href='/solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#tres-sol-detalle').text("Solicitud");
  /* COTIZACION */
  $('#tres-coti-id').html("<a href='/solicitud/cotizacion/"+idsol+"'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#tres-coti-detalle').text("Cotización");
  /* ESPECIFICACION  */
  $('#tres-esp-id').html("<a href='/solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#tres-esp-detalle').text("Especificación Técnica");
  /*NOTA DE AUTORIZACION*/
  $('#tres-aut-id').html("<a href='/supervisor/pdf/autorizacion/"+idsol+"'><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#tres-aut-detalle').text("Nota de Autorización");
  /*NOTA DE ADJUDIACION*/
  $('#tres-adju-id').html("<a href='/supervisor/pdf/adjudicacion/"+idsol+"' ><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#tres-adju-detalle').text("Nota de Adjudicación");

  $('.tres-modal-title').text('Documentos');
  });

  $(document).on('click', '.documentos-cuatro-modal', function() {
  idsol=$(this).data('id');
  idesp=$(this).data('esp');
  var idadju=$(this).data('idadju');
  console.log(idsol);
  console.log(idesp);
  console.log(idadju);
  $('#show-documentos-cuatro').modal('show');
  $('#cuatro-titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  $('#cuatro-sol-id').html("<a href='/solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-sol-detalle').text("Solicitud");
  /* COTIZACION */
  $('#cuatro-coti-id').html("<a href='/solicitud/cotizacion/"+idsol+"'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-coti-detalle').text("Cotización");
  /* ESPECIFICACION  */
  $('#cuatro-esp-id').html("<a href='/solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-esp-detalle').text("Especificación Técnica");
  /*NOTA DE AUTORIZACION*/
  $('#cuatro-aut-id').html("<a href='/supervisor/pdf/autorizacion/"+idsol+"'><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-aut-detalle').text("Nota de Autorización");
  /*NOTA DE ADJUDIACION*/
  $('#cuatro-adju-id').html("<a href='/supervisor/pdf/adjudicacion/"+idsol+"' ><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-adju-detalle').text("Nota de Adjudicación");
  /*DOCUMENTOS DEL PROVEEDOR*/
  $('#cuatro-provee-id').html("<a href='/supervisor/autorizacion/documento/"+idadju+"'><img src='{{asset('images/pdf-logo-blue.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#cuatro-provee-detalle').text("Documentos del Proveedor");

  $('.cuatro-modal-title').text('Documentos');
  });
    
  });
  
  </script>
@endsection