<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2.3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: .5cm;
                left: 1cm;
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
               
           
                text-align: center;
                
                
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: .5cm; 
                left: 1cm; 
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
                
                text-align: center;
  
             
            }
        </style>
    </head>
    <body style="font-size: 12px; font-family: sans-serif;">
        <!-- Define header and footer blocks before your content -->
        <header>
            <table  cellspacing="0" width="100%"> 
              <tr>
                <td align="left"><img src="images/escudoBolivia.png"  width="120" height="84"></td>
                <td align="center"><img src="images/fonabosque.png"  width="135" height="63"></td>
                <td align="right"><img src="images/logoMMAYA.png"  width="150" height="47"></td>
              </tr>
            </table>
        </header>

        <footer>
            <table border="0" cellspacing="0" width="100%"> 
                <tr>
                    <td align="left"> <h5>www.fonabosque.gob.bo</h5></td>
                    <td align="right">
                        <h6>
                         Calle Almirante Grau Nro. 557, Piso1, entre calle<br>
                         Zoilo Flores y Boquerón, Zona San pedro <br>
                         Telf:(591-2)-(2129838-2128772) <br>
                         Fax:(591-2)-2128772 <br>
                         info@fonabosque.gob.bo
                        </h6>
                    </td> 
                </tr>
  
              </table>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main><br><br>
            <div align="right">La Paz, 06 de diciembre de 2019</div>
            <div align="right"><strong><ins>FONABOSQUE/RPA/370/2019</ins></strong></div>
            <br>
            <div align="left">Señora</div>
            <div align="left">Lic. Isabel Mansilla Duran</div>
            <div align="left"><strong>Encargada Administrativa</strong></div>
            <div align="left"><ins>Presente.-</ins></div>
            <br><br><br>
  
            <div align="right"><strong>REF: AUTORIZACIÓN PROCESO DE CONTRATACIÓN <span style="text-transform: uppercase">{{$solicitud->tipo->nombre}}</span> FB/CM/BI Nº 140-2019 
            “{{$solicitud->objeto}}”</strong></div>
            <br><br><br>
            <div align="left">De mi consideración:</div>
            <br><br><br>
            <div align="justify">En cumplimiento al inciso b) del artículo 34 de las Normas Básicas del Sistema 
                de Contratación de Bienes y Servicios aprobadas mediante D.S. 0181, en mi 
                calidad de Responsable de Procesos de Contratación de Apoyo Nacional a la 
                Producción y Empleo (RPA-ANPE) del FONABOSQUE designado por Resolución Administrativa 
                Nro. 048/2017, <strong>AUTORIZO</strong> el inicio de proceso de <strong>CONTRATACIÓN <span style="text-transform: uppercase">{{$solicitud->tipo->nombre}}</span> 
                FB/CM/BI Nº 140-2019 “{{$solicitud->objeto}}”</strong>, 
                bajo la modalidad de Contratación Menor es así que instruyo dar continuidad al presente 
                proceso en cumplimiento del artículo 36 inciso c) del D.S. 0181. </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div align="left" style="font-size: 6"><small>CESJ/gvm</small></div>
            <div align="left" style="font-size: 6"><small>cc. archivo</small></div>
            <div align="left" style="font-size: 6"><small>Proceso Contrataciones</small></div>
        </main>
    </body>
</html>