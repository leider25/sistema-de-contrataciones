<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2.3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: .5cm;
                left: 1cm;
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
               
           
                text-align: center;
                
                
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: .5cm; 
                left: 1cm; 
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
                
                text-align: center;
  
             
            }
        </style>
    </head>
    <body style="font-size: 12px; font-family: sans-serif;">
        <!-- Define header and footer blocks before your content -->
        <header>
            <table  cellspacing="0" width="100%"> 
              <tr>
                <td align="left"><img src="images/escudoBolivia.png"  width="120" height="84"></td>
                <td align="center"><img src="images/fonabosque.png"  width="135" height="63"></td>
                <td align="right"><img src="images/logoMMAYA.png"  width="150" height="47"></td>
              </tr>
            </table>
        </header>

        <footer>
            <table border="0" cellspacing="0" width="100%"> 
                <tr>
                    <td align="left"> <h5>www.fonabosque.gob.bo</h5></td>
                    <td align="right">
                        <h6>
                         Calle Almirante Grau Nro. 557, Piso1, entre calle<br>
                         Zoilo Flores y Boquerón, Zona San pedro <br>
                         Telf:(591-2)-(2129838-2128772) <br>
                         Fax:(591-2)-2128772 <br>
                         info@fonabosque.gob.bo
                        </h6>
                    </td> 
                </tr>
  
              </table>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main><br><br>
        @php $proveedor=App\Proveedor::with('encargado','referencia')->where('id',$adjudicacion->proveedor_id)->first() @endphp
        <div align="right">La Paz, {{$fecha}}</div>
            <div align="right"><strong><ins>FONABOSQUE/RPA/370/2019</ins></strong></div>
            <br>
            <div align="left">Señor(a)</div>
            <div align="left">{{$proveedor->encargado->nombre}}</div>
            <div align="left">{{$proveedor->encargado->cargo}}</div>
            <div align="left"><strong>{{$proveedor->descripcion}}</strong></div>
            <div align="left">{{$proveedor->direccion}}</div>
            @php $referencias=$proveedor->referencia @endphp
            <div align="left">Teléfono: fijo. 
            @foreach ($referencias as $referencia)
                @if ($referencia->tiporeferencia_id==2)
                    {{$referencia->descripcion }}
                @endif
            @endforeach
            cel.
            @foreach ($referencias as $referencia)
                @if ($referencia->tiporeferencia_id==1)
                    {{$referencia->descripcion }}
                @endif
            @endforeach
            </div>
            <div align="left"><ins>Presente.-</ins></div>
            <br><br><br>
            <div align="center"><strong>REF: NOTIFICACIÓN DE ADJUDICACIÓN - PROCESO DE CONTRATACIÓN FB/CM/BI Nº 140-2019 
                “{{$adjudicacion->solicitud->objeto}}”</strong></div>
            <br><br><br>
            <div align="left">De mi consideración:</div>
            <br><br>
            @php 
            $solicitud=App\Solicitud::with('tipo','subtipo')->where('id',$adjudicacion->solicitud_id)->first();
   
            $especificacionTecnica=App\Especificaiontecnica::with('condiciontecnica','condicionadministrativa')
                                                    ->where('id', '=', $solicitud->especificaciontecnica_id)
                                                      ->first();
            $entrega=App\Entrega::with('lugar')->where('id',$especificacionTecnica->condicionadministrativa->entrega_id)->first(); 
            @endphp
            <div align="justify">Por intermedio de la presente, en el marco de los establecido  del D.S. N° 0181 artículo 34 inciso f), en mi calidad de 
                Responsable de Procesos de Contratación de Apoyo Nacional a la Producción y Empleo (RPA-ANPE) del FONABOSQUE designado por Resolución 
                Administrativa Nro. 048/2017, comunico a usted la Adjudicación del proceso de contratación {{$solicitud->tipo->nombre}} <strong>FB/CM/BI Nº 140/2019 
                “{{$solicitud->objeto}}”</strong>  por el importe de Bs. {{$adjudicacion->monto}}.- ({{$importe}} 00/100 Bolivianos), con un plazo de 
                entrega de diez ({{$entrega->plazo}}) días calendario a partir del día siguiente hábil  de la firma de la Orden de Compra. </div>
                <br><br>
            <div align="justify">En este sentido, para la elaboración de la Orden de Compra, deberá presentar en un plazo que no exceda los cuatro (4) días 
                hábiles a partir de la recepción de la presente Nota de Adjudicación, documentación que deberá ser presentada en oficinas del FONABOSQUE, 
                ubicada en la Zona de San Pedro calle Almirante Grau Nº 557, con una nota dirigida al Responsable del Proceso de Contratación del FONABOSQUE 
                Lic. Carlos Eduardo Salvador Justiniano, adjuntando lo siguiente:</div><br><br>


            <ul type="a">
            <li>Fotocopia del Padrón Nacional de Contribuyentes (NIT)
            <li>Fotocopia del Registro Beneficiario SIGEP 
            <li>Fotocopia de Cedula de Identidad del propietario o representante legal
            <li>Fotocopia del Poder otorgado al Representante Legal (Si corresponde)
            <li>Certificado de No Adeudo por Contribuciones al Seguro Social Obligatorio de Largo Plazo y al Sistema Integral de Pensiones (de ambas gestoras de pensiones). 
            <li>Fotocopia del Registro Comercial FUNDEMPRESA)
            </ul><br><br>

            <div align="left">Sin otro particular me despido atentamente.</div>

            <br><br><br><br>
            <div align="left" style="font-size: 6"><small>CESJ/IMD/GVM</small></div>
            <div align="left" style="font-size: 6"><small>Original: 	Interesado</small></div>
            <div align="left" style="font-size: 6"><small>Copias: 	Archivo Proceso RPA</small></div>







        </main>
    </body>
</html>