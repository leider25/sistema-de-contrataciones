@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bienvenido
      <small>Especificación Técnica</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="#">Procesos</li>
      <li class="active">Validados</li>
    </ol>
  </section>
<section class="content">
<div class="col-xs-12">
  <div class="box box-solid box-info">
      <div class="box-header">
          <h3 class="box-title">Solicitud de Contrataciones</h3>
      </div>
      <!--FILTRAMOS SOLICITUDES APROBADAS, RECHAZADAS Y VALIDADAS-->
      @php 
      $solicituds=App\Solicitud::with('tipo','estados')->get();
      $solicitudes=array();
      foreach ($solicituds as $value) {
        $coleccion=$value->estados;
        foreach ($coleccion as $item){
          if ($item->pivot->activo==1){
            if ( in_array($item->id,[4,5,9])){
              array_push($solicitudes,$value);
            }
          }
        }
      }
      @endphp

      <!--TABLA-->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="table">
              <thead>
                <tr class="text-center info">
                  <th>No</th>
                  <th>Área</th>
                  <th>Funcionario</th>
                  <th>Estado</th>
                  <th>Objeto de Contratación</th>
                  <th>Objetivo</th>
                  <th>Fecha</th>
                  <th class="bg-yellow text-center">Motivo</th>
                  <th>Docs</th>
                  <th class="text-center" width="150px">
                    Accion
                </th>
              </thead>
            </tr>
            {{ csrf_field() }}
            @php  $no=1;@endphp

            @foreach ($solicitudes as $value)
            @php 
            $usuario=App\User::where('id',$value->usuario_id)->first();
            $area_id=DB::connection("contrataciones")->table('area_user')->select('area_id')->where('usuario_id','=',$usuario->id)->first();
            $area=App\Area::where('id',$area_id->area_id)->first();
            @endphp

              <tr>
                <td>{{ $no++ }}</td>
                <!--AREA-->
                <td>{{ $area->nombre}}</td>
                <!--FUNCIONARIO-->
                <td>{{ $usuario->name}}</td>
                <!--ESTADOS-->
                @php $coleccion=$value->estados @endphp
                @foreach ($coleccion as $item)
                  @if ($item->pivot->activo==1)
                    @if ( in_array($item->id,[4] ) )
                      <td><span class="label label-success">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[9] ) )
                    <td><span class="label label-primary">{{$item->descripcion}}</span></td>
                    @elseif ( in_array($item->id,[5] ) )
                    <td><span class="label label-danger">{{$item->descripcion}}</span></td>
                    @endif 
                  @endif
                @endforeach
                <!--OBJETO-->
                <td>{{ $value->objeto}} <br> <b>{{ $value->tipo->nombre }} </b></td>
                <!--OBJETIVO-->
                <td>{{ $value->objetivo }}</td>
                <!--FECHA-->
                <td>{{ $value->fecha }}</td>
                <!--MOTIVO-->
                @foreach ($coleccion as $item)
                  @if ($item->pivot->activo==1)
                  <td class="warning text-center">{{$item->pivot->motivo}}</td>
                  @endif
                    
                @endforeach

                <!--DOCUMENTOS-->
                <td>
                  <a href="#" class="documentos-dos-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                    data-objeto="{{$value->objeto}}"
                    data-esp="{{$value->especificaciontecnica_id}}"
                    @foreach ($coleccion as $item)
                      @if ($item->pivot->activo==1)

                      data-estado="{{$item->id}}"> 
                      @endif
                    @endforeach
                    <i class="fa fa-eye"></i>
                  </a>
                </td>
                <!--DEMAS BOTONES-->
                <td align="center">
                  <a href="#" class="show-modal btn btn-success btn-sm" data-id="{{$value->id}}" 
                                                                        data-objeto="{{$value->objeto}}" 
                                                                        data-objetivo="{{$value->objetivo}}"
                                                                        data-area="{{$area->nombre}}"
                                                                        data-funcionario="{{$usuario->name}}"> 
                    <i class="fa fa-send"></i>
                  </a>
                  
              </tr>
              @endforeach
          </table>
        </div>
      
      </div>
  </div>  
</div>
</section>
</div>
{{-- Modal Form Show Solicitud --}}
<div id="show" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
            <div class="modal-body">


                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row"><label for="">ID :</label></th>
                        <td id="id"></td>
                      </tr>
                      
                      <tr>
                        <th scope="row"><label for="">OBJETO :</label></th>
                        <td id="objeto"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">OBJETIVO :</label></th>
                        <td id="objetivo"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">ÁREA :</label></th>
                        <td id="area"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">FUNCIONARIO :</label></th>
                        <td id="funcionario"></td>
                      </tr>

                    </tbody>
                  </table>
                  <form class="form-horizontal">
                    <div class="form-group" id="div-motivo">  
                      <div class="col-sm-12">
                            <label for="title">Motivo</label>
                            <input type="text" name="motivo" id="motivo" class="form-control" placeholder="Ingrese el motivo" />
                      </div>
                    </div>
                  </form>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" id="btnAprobar">
                  </span> Aprobar Solicitud
                </button>
      
                <button class="btn btn-danger" type="submit" id="btnRechazar">
                    </span> Rechazar Solicitud
                  </button>
      
  
            </div>
      </div>
      

    </div>
  </div>

{{-- Modal Form Show dcumentos dos --}}
<div id="show-documentos-dos" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="dos-modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="dos-titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="dos-sol-id"></td>
                  <td ><b id="dos-sol-detalle"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="dos-coti-id"></td>
                  <td><b id="dos-coti-detalle"></b></td>
                </tr>
                <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>3</b></th>
                  <td id="dos-esp-id"></td>
                  <td><b id="dos-esp-detalle"></b></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    {{-- ajax Form Add Type Supplier--}}
  $(document).ready( function () {  
    var no = "<?php echo $no;?>";
    let idSol;

    console.log(no);
  // var checks =  new Array();
  var array_check=[];
  //var i=1; 
  //views_name=[{id: 1,select: "sel-1",input: "inp-1"}];
  //Flat red color scheme for iCheck  
  $('#table').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'info'        : true,
    'autoWidth'   : false,
    "pageLength": 20,
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
  }
  });   

  // Show function
  $(document).on('click', '.show-modal', function() {
    $('#show').modal('show');
    $('#id').text($(this).data('id'));
    $('#objeto').text($(this).data('objeto'));
    $('#objetivo').text($(this).data('objetivo'));
    $('#area').text($(this).data('area'));
    $('#funcionario').text($(this).data('funcionario'));
    $('.modal-title').text('Solicitud');

    idsol=$(this).data('id');
  });


  $( "#btnAprobar" ).click(function() {


    var formdata=new FormData();
    formdata.append('_token',$('input[name=_token]').val());
    formdata.append('motivo',$('input[name=motivo]').val());
    formdata.append('id',idsol);

    console.log(idsol);
    $.ajax({
     url:'/responsable/solicitud/aprobar',
     type:'post',
     data:formdata,
     contentType: false,
     processData: false,
     dataType:'json',
     success:function(data)
     {
      console.log(data);
      location.reload();
     }
    });
    
  
  });

  $( "#btnRechazar" ).click(function() {
    var formdata=new FormData();
    formdata.append('_token',$('input[name=_token]').val());
    formdata.append('motivo',$('input[name=motivo]').val());
    formdata.append('id',idsol);

    console.log(idsol);
    $.ajax({
     url:'/responsable/solicitud/rechazar',
     type:'post',
     data:formdata,
     contentType: false,
     processData: false,
     dataType:'json',
     success:function(data)
     {
      console.log(data);
      location.reload();
     }
    });
  });

  $(document).on('click', '.documentos-dos-modal', function() {
  idsol=$(this).data('id');
  idesp=$(this).data('esp');
  console.log(idsol);
  console.log(idesp);
  $('#show-documentos-dos').modal('show');
  $('#dos-titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  $('#dos-sol-id').html("<a href='/solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-sol-detalle').text("Solicitud");
  /* COTIZACION */
  $('#dos-coti-id').html("<a href='/solicitud/cotizacion/"+idsol+"'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  $('#dos-coti-detalle').text("Cotización");
  /* espeficicacion  */
  $('#dos-esp-id').html("<a href='/solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='{{asset('images/pdf-logo.png')}}' width='40' height='40' alt='Submit'></a>");
  
  $('#dos-esp-detalle').text("Especificación Técnica");


  
  $('.dos-modal-title').text('Documentos');
  });
    
  });
  
  </script>
@endsection