@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <div class="box-header">
      <div class="col-lg-12 margin-tb">
          <div class="pull-right">
              <a class="btn btn-primary" href="{{ route('solicitud') }}"> volver</a>
          </div>
      </div>
  </div>
  <div class="container">
      <div class="box box-solid box-primary">
          <div class="box-header with-border">
            <h1 class="box-title"><b>ESPECIFICACIÓN TÉCNICA - BIENES (CONTRATACIÓN MENOR Ó DIRECTA)</b></h1>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form class="form-horizontal espeficicacion-form">
                      <h3 style="text-align: center"><span class="label label-info">CONDICIONES TÉCNICAS</span></h3><br>
                      <div class="form-group">
                          <div class="col-sm-5">
                            <label for="item">Ítem, Lote, etc. N°</label>
                            <input type="text" class="form-control" id="item" name="item"
                            placeholder="Nombre del Ítem, lote, etc. (ejemplo: computadora de escritorio).">
                            <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-sm-3">
                              <label for="cantidad">Cantidad</label><br>
                              <input type="number" class="form-control" id="cantidad" name="cantidad"
                              placeholder="Determinar en numeral.">
                              <p class="error text-center alert alert-danger hidden"></p>
                            </div>
                            <div class="col-sm-3">
                                  <label for="unidad">Unidad</label>
                                  <input type="text" class="form-control" id="unidad" name="unidad"
                                  placeholder="Determinar la unidad de medida.">
                                  <p class="error text-center alert alert-danger hidden"></p>
                            </div>
                            <div class="col-sm-1">
                                <label for="masitem">Acción</label><br>
                                <a class="btn btn-success" href="#" role="button" id="masitem"><span class="glyphicon glyphicon-plus"></span></a>
                            </div>
                       </div>
                      <!--
                       <div class="form-group">
                              <div class="col-sm-4">
                                <label for="pais">País de fabricación</label>
                                <input type="text" class="form-control" id="pais" name="pais"
                                placeholder="a ser especificado solo por el proveedor." required>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                              <div class="col-sm-4">
                                  <label for="marca">Marca</label>
                                  <input type="text" class="form-control" id="marca" name="marca"
                                  placeholder="a ser especificado solo por el proveedor." required>
                                  <p class="error text-center alert alert-danger hidden"></p>
                                </div>
                                <div class="col-sm-4">
                                      <label for="modelo">Modelo</label>
                                      <input type="text" class="form-control" id="modelo" name="modelo"
                                      placeholder="a ser especificado solo por el proveedor." required>
                                      <p class="error text-center alert alert-danger hidden"></p>
                                </div>
                          </div>-->
    
                          <div class="form-group" id="garantecnica" name="garantecnica">
                                  <div class="col-sm-7">
                                    <label for="garantia">Garantías técnicas</label>
                                    <input type="text" class="form-control" id="inp-gar1" name="inp-gar1"
                                    required>
                                    <p class="error text-center alert alert-danger hidden"></p>
                                  </div>
                                  <div class="col-sm-4">
                                          <label for="tipo">Tipo de Garantía</label>    
                                      <select class="form-control" name="sel-gar1">
                                            

                                          @foreach( $tipogarantiatecnica as $key => $value )
                                          <option value="{{ $key }}">{{ $value }}</option>
                                          @endforeach
                                         
                                          </select>
                                      </div>
                                  <div class="col-sm-1">
                                          <label for="mas">Acción</label>
                                      <button class="btn btn-success" id="masgarantecnica"  title="Añadir">
                                          <span class="glyphicon glyphicon-plus"></span>
                                      </button>
                                  </div>
                          </div>

                          
                          <div class="form-group" id="servicioconexo" name="servicioconexo">   
                                  <div class="col-sm-4">
                                          <label for="tipo">Tipo de Servicios Conexos</label>    
                                      <select class="form-control" name="sel-tsc1">
                                          @foreach( $serviciosconexos as $key => $value )
                                          <option value="{{ $key }}">{{ $value }}</option>
                                          @endforeach
                                         
                                          </select>
                                  </div>
                                  <div class="col-sm-7">
                                              <label for="servicio">Descripción del Servicio</label>
                                              <input type="text" class="form-control" id="inp-tsc1" name="inp-tsc1"
                                              required>
                                              <p class="error text-center alert alert-danger hidden"></p>
                                  </div>
                                  <div class="col-sm-1">
                                          <label for="mas">Acción</label>
                                      <button class="btn btn-success" id="masservicioconexo" title="Añadir">
                                          <span class="glyphicon glyphicon-plus"></span>
                                      </button>
                                  </div>
                      </div>
                      <h5><b>(*) Opcional</b></h5>
                      <div class="form-group">
                        <div class="col-sm-6">
                          <label for="garantiafunc">(*) Garantía de funcionamiento de maquinaria y/o equipo</label>
                          <input type="text" class="form-control" id="garantiafunc" name="garantiafunc"
                          required>
                          <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-sm-6">
                         <label for="certificacion">(*) Certificaciones</label>
                         <input type="text" class="form-control" id="certificacion" name="certificacion"
                         required>
                         <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                      </div>

                      <div class="form-group">
                              <div class="col-sm-6">
                                <label for="adicional">(*) Condiciones adicionales sujetos de evaluación</label>
                                <input type="text" class="form-control" id="adicional" name="adicional"
                                 required>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                              <div class="col-sm-6">
                                <label for="inspeccion">(*) Inspecciones y Pruebas de Funcionamiento</label>
                                <input type="text" class="form-control" id="inspeccion" name="inspeccion"
                                 required>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                      </div>
                      <br><br><h3 style="text-align: center"><span class="label label-info">CONDICIONES ADMINISTRATIVAS</span></h3><br>
                      
                      <div class="form-group">
                        @php $solicitud=App\Solicitud::with('tipo')->where('id',$idsolicitud)->first() @endphp
                        
                              <div class="col-sm-6">
                                <label for="modalidad">Modalidad de Contratación</label>
                              <input type="text" value="Contratación {{ $solicitud->tipo->nombre }}" class="form-control" id="modalidad" name="modalidad"
                                 disabled>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                              <div class="col-sm-3">
                                <label for="tipo">Forma de Adjudicación</label>    
                                      <select class="form-control" name="select-adjudicacion">
                                          @foreach( $tipoadjudicacion as $key => $value )
                                          <option value="{{ $key }}">{{ $value }}</option>
                                          @endforeach   
                                      </select>
                              </div>
                              <div class="col-sm-3">
                                  <label for="tipo">Tipo de Contrato</label>    
                                        <select class="form-control" name="select-contrato">
                                            @foreach( $tipocontrato as $key => $value )
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach     
                                        </select>
                                </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-3">
                              <label for="tipo">Lugar</label>    
                                    <select class="form-control" name="select-lugares">
                                        @foreach( $lugares as $key => $value )
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach    
                                 
                                    </select>
                            </div>
                          <div class="col-sm-7">
                            <label for="lugar">Lugar(es) de entrega</label>
                            <input type="text" class="form-control" id="lugar" name="lugar"
                             required>
                            <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-sm-2">
                            <label for="plazo">Plazo de entrega</label>
                            <div class="input-group">
                              <input type="number" class="form-control" id="plazo" name="plazo" required>
                              <span class="input-group-addon">Días</span>
                            </div>
                            <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                      </div>
                      <div class="form-group">
                              <div class="col-sm-5">
                                <label for="validez">Validez de Propuesta</label>
                                <textarea class="form-control z-depth-1" id="validez" name="validez" rows="3" placeholder="Establecer la validez de la propuesta, la misma que no deberá ser menor a treinta (30) días calendario"></textarea>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                              <div class="col-sm-5">
                                 <label for="multa">Multas</label>
                                 <textarea class="form-control z-depth-1" id="multa" name="multa" rows="3" placeholder="cuando se requiera sobre los bienes demorados, estableciendo el porcentaje y la forma de aplicación"></textarea>
                                 <p class="error text-center alert alert-danger hidden"></p>
                               </div>
                               <div class="col-sm-2">
                                  <label for="porcentaje">% multa</label>
                                  <div class="input-group">
                                    <input type="number" class="form-control" id="porcentaje" name="porcentaje" required>
                                    <span class="input-group-addon">%</span>
                                  </div>
                                  <p class="error text-center alert alert-danger hidden"></p>
                                </div>
                               <!--
                                <div class="col-sm-4">
                                  <label for="cumplimento">Garantía de cumplimiento de Contrato</label>
                                  <textarea class="form-control z-depth-1" id="cumplimiento" name="cumplimiento" rows="3" placeholder="Cuando la entrega del bien o servicio sea mayor a 15 días calendario"></textarea>
                                  <p class="error text-center alert alert-danger hidden"></p>
                              </div>-->
                      </div>
                      <div class="form-group" id="divgarantia" name="divgarantia">
                          <div class="col-sm-3  " >
                              <label for="tipo">Tipo de Gantantía</label>    
                                    <select class="form-control" name="select-gar">
                                        @foreach( $tipogarantias as $key => $value )
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach      
                                    </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="tipo">% Garantía</label>    
                                      <select class="form-control" name="selest-porc">
                                          @foreach( $porcentajes as $key => $value )
                                          <option value="{{ $key }}">{{ $value }}</option>
                                          @endforeach  
                                         
                                      </select>
                          </div>
 
                          
                                <div class="col-sm-6">
                                  <label for="garantiades">Descripción Garantía</label>
                                  <textarea class="form-control z-depth-1" id="garantiades" name="garantiades" rows="3" ></textarea>
                                  <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                          
                  </div>
                </form>
                  </div>               

                  <div class="box-footer">
                      <button type="submit"id="add" class="btn btn-primary btn-lg center-block">Registrar Especificación Técnica</button>
                  </div>          
          
          <!-- /.box-body -->
        </div>
  </div>
     

  </section>
</div>




<script type="text/javascript">
 {{-- ajax Form Add Type Bienes--}}
 $(document).ready( function () {
  var tipogarantiatecnica = @json($tipogarantiatecnica); 
  var serviciosconexos = @json($serviciosconexos);
  //var views_caracts =  new Array();
  var views_gt =  new Array();
  var views_tsc =  new Array();

  var items =  new Array();

  var indexfila=1;
  var indexcolumna=1;

  //i_caracts=1;
  i_gt=1;
  i_tsc=1;

  //views_caracts=[{id: 1,input: "inp-caracts1"}];
  views_gt=[{id: 1,input: "inp-gar1",select: "sel-gar1"}];
  views_tsc=[{id: 1,select: "sel-tsc1",input: "inp-tsc1"}];

  var idsolicitud = '<?php echo $idsolicitud;?>'

  $('#divgarantia').hide();

    $('#masitem').click(function(){
    
    var nombre=$('input[name=item]').val();
    var cantidad=$('input[name=cantidad]').val();
    var unidad=$('input[name=unidad]').val();
    
    
    if(validarItem()){
    var item=new Item(indexfila,'vista'+indexfila,nombre,cantidad,unidad);
    item.add('inp-'+indexfila+'0');
    items.push(item);

    //console.log(item);
    
  //i_caracts++;
  //'<div class="form-group">'
    var html='<div class="col-sm-6" id="vista'+indexfila+'">'
                  +'<div class="box box-primary box-solid">'
                    +'<div class="box-header with-border">'
                      +'<h3 class="box-title">'+nombre+' : '+cantidad+' ('+unidad+')</h3>' 
                      +'<div class="box-tools pull-right">'
                        +'<a class="btn btn-box-tool trash-boton" href="#" role="button"><span class="glyphicon glyphicon-trash"></span></a>'
                      +'</div>' 
                      +'<!-- /.box-tools -->'
                    +'</div>'
                    +'<!-- /.box-header -->'
                    +'<div class="box-body" id="'+indexfila+'">'
                      +'<div class="form-group">'
                          +'<div class="col-sm-10">'
                            +'<label for="caracteristicas">Características</label>'
                           +'<input type="text" class="form-control" id="inp-'+indexfila+'0" name="inp-'+indexfila+'0"'
                            +'required>'
                            +'<p class="error text-center alert alert-danger hidden"></p>'
                          +'</div>'
                          +'<div class="col-sm-1">'
                                  +'<label for="mas">Acción</label>'
                                  +'<a class="btn btn-success add_boton" href="#" role="button"><span class="glyphicon glyphicon-plus"></span></a>'
                          
                          +'</div>'
                        +'</div>'
                    +'</div>'
                    +'<!-- /.box-body -->'
                  +'</div>'
                  +'<!-- /.box -->'
                +'</div>';
                          
                          
                          
      $('#garantecnica').before(html);
      $('#item').val('');
      $('#cantidad').val('');
      $('#unidad').val('');
      console.log(items);
                          
    indexfila++;
    }
          //console.log(views_name);
  });

  $('.espeficicacion-form').on("click",".add_boton", function(e){
    e.preventDefault(); 
    var i='#'+$(this).parent().parent('div').parent('div').prop("id");
    var id=$(this).parent().parent('div').parent('div').prop("id");
    console.log("hola")
    console.log(i);


    var html="<div class='form-group' id='inp-"+id+""+indexcolumna+"' name='"+id+"'>"
                    +"<div class='col-sm-10'>"
                      +"<input type='text' class='form-control' id='inp-"+id+""+indexcolumna+"' name='inp-"+id+""+indexcolumna+"'"
                     +"required>"
                      +"<p class='error text-center alert alert-danger hidden'></p>"
                    +'</div>'
                    +"<div class='col-sm-1'>"
                      +'<a class="btn btn-danger removeItem_button" href="#" role="button"><span class="glyphicon glyphicon-minus"></span></a>'
                    +"</div>"
            +"</div>";
            $(i).append(html);
    let caracteristica={
      'id':id+""+indexcolumna,
      'input':'inp-'+id+""+indexcolumna,
    };

    for (let index = 0; index < items.length; index++) {
      if (items[index].id==id) {
        console.log("Soy Igual: "+items[index].id+"="+id);
        
        items[index].add("inp-"+id+""+indexcolumna);

      }
    }


    indexcolumna++;
    
  });

  $('.espeficicacion-form').on("click",".removeItem_button", function(e){
    e.preventDefault(); 
    //$(this).parent('div').parent('div').remove(); 
    //console.log(i);
    //console.log($(this).parent().parent('div').prop("id"));
    var id_carats=$(this).parent().parent('div').prop("id");
    var idName='#'+id_carats;
    var id=$(idName).attr('name');
    console.log(id_carats);
    console.log(id);
    for (let i = 0; i < items.length; i++) {
      if (items[i].id==id) {
        items[i].delete(id_carats);
        console.log("ok");
      }
    }
    $(this).parent('div').parent('div').remove(); 
  });
  $('.espeficicacion-form').on("click",".trash-boton", function(e){
    e.preventDefault(); 
    //
    var id=$(this).parent().parent('div').parent('div').parent('div').prop("id");
    var vista;
    for (let i = 0; i < items.length; i++) {
      
      if (items[i].vista==id) {
        var item=items[i]
        var vista=items.indexOf(item);

        if (confirm("Esta seguro de eliminar "+items[i].nombre)) {
          items.splice(vista,1);
          $(this).parent('div').parent('div').parent('div').parent('div').remove(); 
        }
        
      }
     
      
      
    }
    console.log(vista);
    //
   //
    console.log(id);
    console.log(items);
    //for (let i = 0; i < items.length; i++) {
    //  if (items[i].id==id) {
    //    items[i].delete(id_carats);
    //    console.log("ok");
    //  }
    //}
    
   
  });


  $('#masgarantecnica').click(function(){
  i_gt++;
    var html="<div class='form-group' id='"+i_gt+"'>"
                +"<div class='col-sm-7'>"
      
                  +"<input type='text' class='form-control' id='inp-gar"+i_gt+"' name='inp-gar"+i_gt+"'"
                  +"required>"
                  +"<p class='error text-center alert alert-danger hidden'></p>"
                +"</div>"
                +"<div class='col-sm-4'>"
                
                    +"<select class='form-control' name='sel-gar"+i_gt+"'>";

                      $.each(tipogarantiatecnica, function(index, value) {
                        html+="<option value='"+index+"'>"+value+"</option>";
                      });
                       
                      html+="</select>"
                    +"</div>"
                +"<div class='col-sm-1'>"
        
                    +"<button class='btn btn-danger remove_buttongarantia' title='Eliminar'>"
                        +"<span class='glyphicon glyphicon-minus'></span>"
                    +"</button>"
                +"</div>"
        +"</div>";
            $('#garantecnica').after(html);
            views_gt.push({
            id: i_gt,
            input: "inp-gar"+i_gt,
            select: "sel-gar"+i_gt
          });
          //console.log(views_name);
     });

    $('#masservicioconexo').click(function(){
    i_tsc++;
    var html="<div class='form-group' id='"+i_tsc+"'>"   
               +"<div class='col-sm-4'>"
 
                   +"<select class='form-control' name='sel-tsc"+i_tsc+"'>";

                      $.each(serviciosconexos, function(index, value) {
                        html+="<option value='"+index+"'>"+value+"</option>";
                      });
                       
                      html+="</select>"
                      +"</div>"
                      +"<div class='col-sm-7'>"
                                
                                  +"<input type='' class='form-control' id='inp-tsc"+i_tsc+"' name='inp-tsc"+i_tsc+"'"
                                  +"required>"
                                  +"<p class='error text-center alert alert-danger hidden'></p>"
                      +"</div>"
                      +"<div class='col-sm-1'>"
                           
                          +"<button class='btn btn-danger remove_buttonconexo' title='Eliminar'>"
                              +"<span class='glyphicon glyphicon-minus'></span>"
                          +"</button>"
                      +"</div>"
          +'</div>';
            $('#servicioconexo').after(html);
            views_tsc.push({
            id: i_tsc,
            select: "sel-tsc"+i_tsc,
            input: "inp-tsc"+i_tsc
          });
          //console.log(views_name);
     });


     $('.espeficicacion-form').on("click",".remove_button", function(e){
          e.preventDefault(); 
          $(this).parent('div').parent('div').remove(); 
          //console.log(i);
          //console.log($(this).parent().parent('div').prop("id"));
          var id_carats=$(this).parent().parent('div').prop("id");
          for (index=0; index < views_caracts.length; index++) {
            if(views_caracts[index].id == id_carats ){
             console.log(views_caracts[index].id+" es igual a "+id_carats);
             views_caracts.splice(index, 1);
             console.log(views_caracts);
            }
          }
        });

        $('.espeficicacion-form').on("click",".remove_buttonconexo", function(e){
          e.preventDefault(); 
          $(this).parent('div').parent('div').remove(); 
          //console.log(i);
          //console.log($(this).parent().parent('div').prop("id"));
          var id_tsc=$(this).parent().parent('div').prop("id");
          for (index=0; index < views_tsc.length; index++) {
            if(views_tsc[index].id == id_tsc ){
             console.log(views_tsc[index].id+" es igual a "+id_tsc);
             views_tsc.splice(index, 1);
             console.log(views_tsc);
            }
          }
        });

        $('.espeficicacion-form').on("click",".remove_buttongarantia", function(e){
          e.preventDefault(); 
          $(this).parent('div').parent('div').remove(); 
          //console.log(i);
          //console.log($(this).parent().parent('div').prop("id"));
          var id_gt=$(this).parent().parent('div').prop("id");
          for (index=0; index < views_gt.length; index++) {
            if(views_gt[index].id == id_gt ){
             console.log(views_gt[index].id+" es igual a "+id_gt);
             views_gt.splice(index, 1);
             console.log(views_gt);
            }
          }
        });


  $(document).on('keyup', '#plazo', function(){
    var query = $(this).val();


    console.log(query);
    if(query>=16){
      $('#divgarantia').show();
      console.log("es doce");
    }else{
      $('#divgarantia').hide();
    }
   // fetch_customer_data(query);
   });

   $("#add").click(function() {
     console.log("registrar");

    var result_caracts =  new Array();
    var result_gt =  new Array();
    var result_tsc =  new Array();
    
    for (index=0; index < views_gt.length; index++){
      result_gt.push({"id_selectgt": document.getElementsByName(views_gt[index].select)[0].value,
                                "garantia": $('input[name='+views_gt[index].input+']').val()});
    }
    
    for (index=0; index < views_tsc.length; index++){
      result_tsc.push({"id_selecttsc": document.getElementsByName(views_tsc[index].select)[0].value,
                                "servicio": $('input[name='+views_tsc[index].input+']').val()});
    }

    for (let i = 0; i < items.length; i++) {
      for (let j = 0; j < items[i].caracteristicas.length; j++) {
        datoViejo=items[i].caracteristicas[j];
        id='input[name='+datoViejo+']';
        datoNuevo=$(id).val();
        items[i].replace(datoNuevo,datoViejo);
      }
    }
    console.log(items);


    console.log(result_caracts);
    console.log(result_gt);
    console.log(result_tsc);

      var formdata=new FormData();

      formdata.append('_token',$('input[name=_token]').val());
      //formdata.append('item',$('input[name=item]').val());
      //formdata.append('cantidad',$('input[name=cantidad]').val());
      //formdata.append('unidad',$('input[name=unidad]').val());

      formdata.append('items',JSON.stringify(items));

      //formdata,append('garantia',$('input[name=garantia]').val());
      formdata.append('listagarantia',JSON.stringify(result_gt));

      formdata.append('garantiafunc',$('input[name=garantiafunc]').val());
      formdata.append('certificacion',$('input[name=certificacion]').val());

      formdata.append('listaservicioconex',JSON.stringify(result_tsc));
      //formdata,append('garantia',$('input[name=garantia]').val());

      formdata.append('adicional',$('input[name=adicional]').val());
      formdata.append('inspeccion',$('input[name=inspeccion]').val());

      formdata.append('modalidad',$('input[name=modalidad]').val());

      formdata.append('adjudicacion',document.getElementsByName("select-adjudicacion")[0].value);
      formdata.append('select-contrato',document.getElementsByName("select-contrato")[0].value);

      formdata.append('select-lugares',document.getElementsByName("select-lugares")[0].value);
      formdata.append('lugar',$('input[name=lugar]').val());
      formdata.append('plazo',$('input[name=plazo]').val());
      
      formdata.append('validez',$('textarea#validez').val());
      formdata.append('multa',$('textarea#multa').val());
      formdata.append('porcentaje',$('input[name=porcentaje]').val());

      formdata.append('select-gar',document.getElementsByName("select-gar")[0].value);
      formdata.append('select-porc',document.getElementsByName("selest-porc")[0].value);
      formdata.append('descripcion',$('textarea#garantiades').val());

      formdata.append('idsolicitud',idsolicitud);

      $.ajax({
        type: 'POST',
        url: '/solicitud/especificacion/crear',
        data:formdata,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(data){
           if ((data.errors)) {
            console.log("eroor");
            console.log(data);
            //if(data.errors.cite){$('.errorCite').removeClass('hidden');$('.errorCite').text(data.errors.cite);}
            //if(data.errors.fecha){$('.errorFecha').removeClass('hidden');$('.errorFecha').text(data.errors.fecha);}
            //if(data.errors.objeto){$('.errorObjeto').removeClass('hidden');$('.errorObjeto').text(data.errors.objeto);}
            //if(data.errors.objetivo){$('.errorObjetivo').removeClass('hidden');$('.errorObjetivo').text(data.errors.objetivo);}
            //if(data.errors.antecedentes){$('.errorAntecedentes').removeClass('hidden');$('.errorAntecedentes').text(data.errors.antecedentes);}
            //if(data.errors.justificacion){$('.errorJustificacion').removeClass('hidden');$('.errorJustificacion').text(data.errors.justificacion);}
            $('.error').removeClass('hidden');
            $('.error').text(data.errors.item);
            $('.error').text(data.errors.cantidad);
            $('.error').text(data.errors.unidad);
            $('.error').text(data.errors.garantiafunc);
            $('.error').text(data.errors.certificacion);
            $('.error').text(data.errors.adicional);
            $('.error').text(data.errors.inspeccion);
            $('.error').text(data.errors.modalidad);
            $('.error').text(data.errors.lugar);
            $('.error').text(data.errors.plazo);
            $('.error').text(data.errors.validez);
            $('.error').text(data.errors.multa);
            $('.error').text(data.errors.porcentaje);
             
             
             
            }else{
          
              console.log("exito");
              console.log(data);
              location.href ="/solicitud";

             

           // $('.error').remove();
           // $('#table').append("<tr class='post" + data.id + "'>"
           //  +"<td></td>"
           // +"<td>" + data.objeto + "<br><b>"+data.tipo_contratacion.nombre+"</b></td>"
           // +"<td>" + data.objetivo + "</td>"
           // +"<td>" + data.fecha + "</td>"
           // +"<td><span class='label bg-yellow'>Pendiente</span></td>"
           // +"<td>"
           // //+"<button href='solicitud/word' class='download-modal' data-id='" + data.id + "' data-description='" + data.objeto + "' title='NOTA INTERNA'><span class='glyphicon glyphicon-file'></span></button> "
           // +"<a href='solicitud/word/"+data.id+"' class='download-modal' title='NOTA INTERNA'><img src='images/word19-logo.png' width='40' height='40' alt='Submit'</a></td> "
           // +"<td><button href='solicitud/especificacion/"+data.id+"' class='especificacion-modal btn btn-primary btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "' title='AÑADIR ESPECIFICACIÓN TÉCNICA'><span class='glyphicon glyphicon-plus'></span></button></td>"
           // +"<td><a href='solicitud/cotizacion/"+data.id+"'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a></td>"
           // +"<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='glyphicon glyphicon-trash'></span></button></td>"
           // +"</tr>");
           //$('#objeto').val('');
           //$('#objetivo').val('');
           //$('#antecedentes').val('');
           //$('#justificacion').val('');
           //$('#buscar').val('');
          // fetch_customer_data('');
        }
       },
      });       
     // 
      });
      
 });
  //Clase Item
  function Item(id,vista,nombre, cantidad, unidad) {
    this.id=id;
    this.vista=vista;
    this.nombre = nombre;
    this.cantidad = cantidad;
    this.unidad = unidad;
    this.caracteristicas=new Array();

    this.add = function (newObject) {
        this.caracteristicas.push(newObject);
    };
    
    this.delete = function (item) {
      var i = this.caracteristicas.indexOf( item );
    i !== -1 && this.caracteristicas.splice( i, 1 );
    };

    this.replace = function (newVar,oldVar) {
      var i = this.caracteristicas.indexOf(oldVar);
      this.caracteristicas[i]=newVar;
    };
  }
  
  //Funcion validar Item
  function validarItem(){
 
  var txtItem = document.getElementById('item').value;
  var txtCantidad = document.getElementById('cantidad').value;
  var txtUnidad = document.getElementById('unidad').value;

  //Test campo Item
  if(txtItem == null || txtItem.length == 0 || /^\s+$/.test(txtItem)){
    alert('ERROR: El campo Ítem, Lote, etc. N° no debe ir vacío o lleno de solamente espacios en blanco');
    return false;
  }
  //Test Cantidad
  if(txtCantidad == null || txtCantidad.length == 0 || isNaN(txtCantidad) || txtCantidad<=0){
    alert('ERROR: Debe ingresar una cantidad válida');
    return false;
  }
  //Test campo Unidad
  if(txtUnidad == null || txtUnidad.length == 0 || /^\s+$/.test(txtUnidad)){
    alert('ERROR: El campo Unidad no debe ir vacío o lleno de solamente espacios en blanco');
    return false;
  }
  return true;
  }


 </script>
 @endsection
 