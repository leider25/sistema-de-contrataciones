<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2.3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: .5cm;
                left: 1cm;
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
               
           
                text-align: center;
                
                
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: .5cm; 
                left: 1cm; 
                right: 1cm;
                height: 2cm;

                /** Extra personal styles **/
                
                text-align: center;
  
             
            }
        </style>
    </head>
    <body style="font-size: 12px; font-family: sans-serif;">
        <!-- Define header and footer blocks before your content -->
        <header>
            <table  cellspacing="0" width="100%"> 
              <tr>
                <td align="left"><img src="images/escudoBolivia.png"  width="120" height="84"></td>
                <td align="center"><img src="images/fonabosque.png"  width="135" height="63"></td>
                <td align="right"><img src="images/logoMMAYA.png"  width="150" height="47"></td>
              </tr>
            </table>
        </header>

        <footer>
            <table border="0" cellspacing="0" width="100%"> 
                <tr>
                    <td align="left"> <h5>www.fonabosque.gob.bo</h5></td>
                    <td align="right">
                        <h6>
                         Calle Almirante Grau Nro. 557, Piso1, entre calle<br>
                         Zoilo Flores y Boquerón, Zona San pedro <br>
                         Telf:(591-2)-(2129838-2128772) <br>
                         Fax:(591-2)-2128772 <br>
                         info@fonabosque.gob.bo
                        </h6>
                    </td> 
                </tr>
  
              </table>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            <h3 style="text-align: center">ESPECIFICACIONES TÉCNICAS</h3>
            <span><strong>OBJETO DE LA CONTRATACIÓN: </strong>{{$solicitud->objeto}}</span>
            <br><br>
            @php  
            $especificacionTecnica=App\Especificaiontecnica::with('condiciontecnica','condicionadministrativa')
                                                    ->where('id', '=', $solicitud->especificaciontecnica_id)
                                                      ->first();
            @endphp
            @php 
            $items=App\Item::where('condiciontecnica_id',$especificacionTecnica->condiciontecnica->id)->get();
            $garantiastecnicas=App\Condiciontecnica::find($especificacionTecnica->condiciontecnica->id)->garantiastecnicas()->get();
            $servicios=App\Condiciontecnica::find($especificacionTecnica->condiciontecnica->id)->servicios()->get();
            @endphp
                <table border="1" cellspacing="0" width="100%">
                    <tr align="center" style="background-color: #acce75"><th colspan="2">CARACTERÍSTICAS</th></tr>
                    <tr align="center" style="background-color: #acce75"><th colspan="2">CONDICIONES TÉCNICAS</th></tr>
                    
                    @foreach ($items as $item)
                    <tr>
                        <th align="left" width="130px">Ítem, Lote, etc. N°</th>
                        <td align="left">{{$item->nombre}}</td>
                    </tr>
                    <tr>
                        <th align="left" width="130px">Cantidad</th>
                        <td align="left" >{{$item->cantidad.", ".$item->unidad}}</td>
                    </tr>
                    @php $caracteristicas=App\Caracteristica::where('item_id',$item->id)->get(); @endphp
                    <tr>
                        <th align="left" width="130px">Características</th>
                        <td align="left" >
                        <ul type="circle">
                        @foreach ($caracteristicas as $caracteristica)
                         <li>{{$caracteristica->descripcion}} 
                        @endforeach
                        </ul>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <th width="130px">Garantías Técnicas</th>
                        <td>
                            @foreach ($garantiastecnicas as $gtecnica)
                            @php $tipogarantiatecnica=App\Tipogarantiatecnica::where('id',$gtecnica->tipogarantiatecnica_id)->first() @endphp
                        <strong>{{$tipogarantiatecnica->descripcion.": "}}</strong>{{$gtecnica->descripcion }}<br>
                                
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th width="130px">Garantía de funcionamiento de maquinaria y/o equipo</th>
                        <td>{{$especificacionTecnica->condiciontecnica->garantia_funcionamiento}}</td>
                    </tr>

                    <tr>
                        <th width="130px">Certificaciones</th>
                        <td>{{$especificacionTecnica->condiciontecnica->certificacion}}</td>
                    </tr>

                    <tr>
                        <th width="130px">Servicios conexos</th>
                        <td>
                            @foreach ($servicios as $servicio)
                            @php
                            $servicioconexo=App\Servicioconexo::where('id',$servicio->servicioconexo_id)->first();    
                            @endphp
                            <strong>{{$servicioconexo->descripcion.": "}}</strong>{{$servicio->descripcion}} <br>
                            @endforeach
                        </td>
                    </tr>

                    <tr>
                        <th width="130px">Condiciones adicionales sujetos de evaluación</th>
                        <td>{{$especificacionTecnica->condiciontecnica->condicion_adicional}}</td>
                    </tr>

                    <tr>
                        <th width="130px">Inspecciones y Pruebas de Funcionamiento</th>
                        <td>{{$especificacionTecnica->condiciontecnica->pruebas_funcionamiento}}</td>
                    </tr>
                    <tr align="center" style="background-color: #acce75"><th colspan="2">CONDICIONES ADMINISTRATIVAS</th></tr>
                    <tr>
                        <th width="130px">Modalidad de Contratación</th>
                        <td>{{$solicitud->tipo->nombre}}</td>
                    </tr>
                    @php $adjudicacion=App\Tipoadjudicacion::find($especificacionTecnica->condicionadministrativa->tipoadjudicacion_id); @endphp
                    <tr>
                        <th width="130px">Forma de Adjudicación</th>
                        <td>{{$adjudicacion->descripcion}}</td>
                    </tr>
                    @php $entrega=App\Entrega::with('lugar')->where('id',$especificacionTecnica->condicionadministrativa->entrega_id)->first(); @endphp
                    <tr>
                        <th width="130px">Lugar de entrega</th>
                        <td>{{$entrega->lugar->descripcion}}</td>
                    </tr>
                    <tr>
                        <th width="130px">Plazo de entrega</th>
                        <td>{{$entrega->plazo." (dias)"}}</td>
                    </tr>
                    <tr>
                        <th width="130px">Validez de la Propuesta</th>
                        <td>{{$especificacionTecnica->condicionadministrativa->validez_propuesta}}</td>
                    </tr>
                    @php $multa=App\Multa::find($especificacionTecnica->condicionadministrativa->multa_id)->first(); @endphp
                    <tr>
                        <th width="130px">Multas</th>
                        <td>{{$multa->descripcion." ".$multa->porcentaje_multa."%"}}</td>
                    </tr>
                    @php $garantia=App\Garantia::with('tipogarantia','porcentaje')->where('id',$especificacionTecnica->condicionadministrativa->garantia_id)->first(); @endphp
                    @if(isset($garantia))
                    <tr>
                        <th width="130px">Garantía de cumplimiento de Contrato</th>
                        <td>{{$garantia->descripcion." (".$garantia->porcentaje->descripcion.'), ('.$garantia->tipogarantia->descripcion.")"}}</td>
                    </tr>
                    @endif
                </table>
        </main>
    </body>
</html>