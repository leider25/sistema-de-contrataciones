@extends('layouts.partials.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bienvenido
      <small>Especificación Técnica</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="#">Especificación Técnica</li>
      <li class="active">Editar</li>
    </ol>
  </section>

  <section class="content">

    <div class="box-header">
      <div class="col-lg-12 margin-tb">
          <div class="pull-right">
              <a class="btn btn-primary" href="{{ route('solicitud') }}"> volver</a>
          </div>
      </div>
  </div>
  @php $id_ctec=$especificacionbienes->condiciontecnica->id;@endphp
  <div class="container">
      <div class="box box-solid box-primary">
          <div class="box-header with-border">
            <h1 class="box-title"><b>ESPECIFICACIÓN TÉCNICA - BIENES (CONTRATACIÓN MENOR Ó DIRECTA)</b></h1>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form class="form-horizontal espeficicacion-form">
              <h3 style="text-align: center"><span class="label label-info">CONDICIONES TÉCNICAS</span></h3><br>
              <div class="form-group">
                  <div class="col-sm-5">
                    <label for="item">Ítem, Lote, etc. N°</label>
                    <input type="text" class="form-control" id="item" name="item"
                    placeholder="Nombre del Ítem, lote, etc. (ejemplo: computadora de escritorio).">
                    <p class="error text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-3">
                      <label for="cantidad">Cantidad</label><br>
                      <input type="number" class="form-control" id="cantidad" name="cantidad"
                      placeholder="Determinar en numeral." >
                      <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                    <div class="col-sm-3">
                          <label for="unidad">Unidad</label>
                          <input type="text" class="form-control" id="unidad" name="unidad"
                          placeholder="Determinar la unidad de medida.">
                          <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                    <div class="col-sm-1">
                        <label for="masitem">Acción</label><br>
                        <a class="btn btn-success" href="#" role="button" id="masitem"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>
               </div>
               @php $tipogarantiatecnica= App\Tipogarantiatecnica::pluck('descripcion','id') @endphp
               @php
                $garantiatecnica=App\Garantiatecnica::with('tipogarantiatecnica')->where('condiciontecnica_id',$id_ctec)->get()
               @endphp

              <div class="form-group" id="garantecnica" name="garantecnica">
                      <div class="col-sm-7">
                        <label for="garantia">Garantías técnicas</label>
                      <input type="text" value="{{$garantiatecnica[0]->descripcion}}" class="form-control" id="inp-gar1" name="inp-gar1"
                        required>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                      <div class="col-sm-4">
                          <label for="tipo">Tipo de Garantía</label>    
                          <select class="form-control" name="sel-gar1">
                              @foreach( $tipogarantiatecnica as $key => $value )
                                @if ($garantiatecnica[0]->tipogarantiatecnica->id==$key)
                                  <option value="{{ $key }}" selected>{{ $value }}</option>
                                @else
                                  <option value="{{ $key }}">{{ $value }}</option>
                                @endif                                 
                              @endforeach  
                          </select>
                      </div>
                      <div class="col-sm-1">
                              <label for="mas">Acción</label>
                              <a class="btn btn-success" id="masgarantecnica" href="#" role="button" title="Añadir"><span class="glyphicon glyphicon-plus"></span></a>
                      </div>
              </div>
              <div class="form-group">
                      <div class="col-sm-6">
                        <label for="garantiafunc">Garantía de funcionamiento de maquinaria y/o equipo</label>
                      <input type="text" value="{{$especificacionbienes->condiciontecnica->garantia_funcionamiento}}" class="form-control" id="garantiafunc" name="garantiafunc"
                        required>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                      <div class="col-sm-6">
                       <label for="certificacion">Certificaciones</label>
                       <input type="text" value="{{$especificacionbienes->condiciontecnica->certificacion}}" class="form-control" id="certificacion" name="certificacion"
                       required>
                       <p class="error text-center alert alert-danger hidden"></p>
                      </div>
              </div>
              @php
                $servicios=App\Servicio::with('servicioconexo')->where('condiciontecnica_id',$id_ctec)->get();
              @endphp

              @php $serviciosconexos= App\Servicioconexo::pluck('descripcion','id') @endphp
              <div class="form-group" id="servicioconexo" name="servicioconexo">   
                      <div class="col-sm-4">
                          <label for="tipo">Tipo de Servicios Conexos</label>    
                          <select class="form-control" name="sel-tsc1">
                              @foreach( $serviciosconexos as $key => $value )  
                              @if ($servicios[0]->servicioconexo->id==$key)
                                <option value="{{ $key }}" selected>{{ $value }}</option>
                              @else
                                <option value="{{ $key }}">{{ $value }}</option>
                              @endif                               
                              @endforeach 
                          </select>
                      </div>
                      <div class="col-sm-7">
                                  <label for="servicio">Descripción del Servicio</label>
                      <input type="text" value="{{$servicios[0]->descripcion}}" class="form-control" id="inp-tsc1" name="inp-tsc1"
                                  required>
                                  <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                      <div class="col-sm-1">
                              <label for="mas">Acción</label>
                          <button class="btn btn-success" id="masservicioconexo" title="Añadir">
                              <span class="glyphicon glyphicon-plus"></span>
                          </button>
                      </div>
              </div>
              <div class="form-group">
                      <div class="col-sm-6">
                        <label for="adicional">Condiciones adicionales sujetos de evaluación</label>
                        <input type="text" value="{{$especificacionbienes->condiciontecnica->condicion_adicional}}" class="form-control" id="adicional" name="adicional"
                         required>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                      <div class="col-sm-6">
                        <label for="inspeccion">Inspecciones y Pruebas de Funcionamiento</label>
                        <input type="text" value="{{$especificacionbienes->condiciontecnica->pruebas_funcionamiento}}" class="form-control" id="inspeccion" name="inspeccion"
                         required>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
              </div>
              <br><br><h3 style="text-align: center"><span class="label label-info">CONDICIONES ADMINISTRATIVAS</span></h3><br>

              @php $solicitud=App\Solicitud::with('tipo')->where('especificaciontecnica_id',$id)->first() @endphp

              
              @php $tipoadjudicacion=App\Tipoadjudicacion::pluck('descripcion','id') @endphp
              @php $tipocontrato=App\Tipocontrato::pluck('descripcion','id') @endphp
              @php $lugares= App\Lugar::pluck('descripcion','id') @endphp

              <div class="form-group">
                  <div class="col-sm-6">
                    <label for="modalidad">Modalidad de Contratación</label>      
                    <input type="text" value="Contratación {{$solicitud->tipo->nombre}}" class="form-control" id="modalidad" name="modalidad"
                     disabled>
                    <p class="error text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-3">
                    <label for="tipo">Forma de Adjudicación</label>    
                          <select class="form-control" name="select-adjudicacion">
                                  
                              @foreach( $tipoadjudicacion as $key => $value )
                                  @if($key==$especificacionbienes->condicionadministrativa->tipoadjudicacion_id)
                                  <option value="{{ $key }}" selected>{{ $value }}</option>
                                  @else
                                  <option value="{{ $key }}">{{ $value }}</option>
                                  @endif
                              
                              @endforeach  
                          </select>
                  </div>
                  <div class="col-sm-3">
                      <label for="tipo">Tipo de Contrato</label>    
                      <select class="form-control" name="select-contrato">
                          @foreach( $tipocontrato as $key => $value )
                              @if($key==$especificacionbienes->condicionadministrativa->tipocontrato_id)
                              <option value="{{ $key }}" selected>{{ $value }}</option>
                              @else
                              <option value="{{ $key }}">{{ $value }}</option>
                              @endif
                          @endforeach    
                      </select>
                  </div>
              </div>

              @php $entrega=App\Entrega::with('lugar')
                          ->where('id',$especificacionbienes->condicionadministrativa->entrega_id)
                              ->first() @endphp
              <div class="form-group">
                  <div class="col-sm-3">
                      <label for="tipo">Lugar</label>    
                            <select class="form-control" name="select-lugares">
                              @foreach( $lugares as $key => $value )
                                  @if($key==$entrega->lugar_id)
                                  <option value="{{ $key }}" selected>{{ $value }}</option>
                                  @else
                                  <option value="{{ $key }}">{{ $value }}</option>
                                  @endif
                              
                              @endforeach
                          
                            </select>
                    </div>
                  <div class="col-sm-7">
                    <label for="lugar">Lugar(es) de entrega</label>
                    <input type="text" value="{{$entrega->descripcion}}" class="form-control" id="lugar" name="lugar"
                     required>
                    <p class="error text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-2">
                    <label for="plazo">Plazo de entrega</label>
                    <div class="input-group">
                      <input type="number" value="{{$entrega->plazo}}" class="form-control" id="plazo" name="plazo" required>
                      <span class="input-group-addon">Días</span>
                    </div>
                    <p class="error text-center alert alert-danger hidden"></p>
                  </div>
              </div>
              @php $plazo=$entrega->plazo @endphp
              @php $multa= App\Multa::where('id',$especificacionbienes->condicionadministrativa->multa_id)->first() @endphp
              <div class="form-group">
                  <div class="col-sm-5">
                    <label for="validez">Validez de Propuesta</label>
                  <textarea class="form-control z-depth-1" id="validez" name="validez" rows="3" placeholder="Establecer la validez de la propuesta, la misma que no deberá ser menor a treinta (30) días calendario">{{$especificacionbienes->condicionadministrativa->validez_propuesta}}</textarea>
                    <p class="error text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-5">
                     <label for="multa">Multas</label>
                  <textarea class="form-control z-depth-1" id="multa" name="multa" rows="3" placeholder="cuando se requiera sobre los bienes demorados, estableciendo el porcentaje y la forma de aplicación">{{$multa->descripcion}}</textarea>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-2">
                      <label for="porcentaje">% multa</label>
                      <div class="input-group">
                      <input type="number" value="{{$multa->porcentaje_multa}}" class="form-control" id="porcentaje" name="porcentaje" required>
                        <span class="input-group-addon">%</span>
                      </div>
                      <p class="error text-center alert alert-danger hidden"></p>
                  </div>
              </div>
              @php $garan=App\Garantia::with('tipogarantia','porcentaje')->where('id',$especificacionbienes->condicionadministrativa->garantia_id)->first()@endphp

              
              @php $tipo_gar=App\Tipogarantia::pluck('descripcion','id')  @endphp
              @php $porcentajes=App\Porcentaje::pluck('descripcion','id')  @endphp
              @if($plazo>15)
              <div class="form-group" id="divgarantia" name="divgarantia">
                  <div class="col-sm-3  " >
                      <label for="tipo">Tipo de Gantantía</label>    
                      <select class="form-control" name="select-gar">
                      @foreach ($tipo_gar as $key => $value)
                        @if($key==$garan->tipogarantia_id)
                        <option value="{{ $key }}" selected>{{ $value }}</option>
                        @else
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endif
                      @endforeach          
                      </select>
                  </div>
                  <div class="col-sm-3">
                      <label for="tipo">% Garantía</label>    
                      <select class="form-control" name="selest-porc"> 
                        @foreach ($porcentajes as $key => $value)
                        @if($key==$garan->porcentaje_id)
                        <option value="{{ $key }}" selected>{{ $value }}</option>
                        @else
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endif
                        @endforeach 
                      </select>
                  </div>
                   <div class="col-sm-6">
                       <label for="garantiades">Descripción Garantía</label>
                   <textarea class="form-control z-depth-1" id="garantiades" name="garantiades" rows="3" >{{$garan->descripcion}}</textarea>
                       <p class="error text-center alert alert-danger hidden"></p>
                   </div>
              </div>
              @else
              <div class="form-group" id="divgarantia" name="divgarantia" style="display: none">
                  <div class="col-sm-3  " >
                      <label for="tipo">Tipo de Gantantía</label>    
                      <select class="form-control" name="select-gar">
                      @foreach ($tipo_gar as $key => $value)               
                        <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach          
                      </select>
                  </div>
                  <div class="col-sm-3">
                      <label for="tipo">% Garantía</label>    
                      <select class="form-control" name="selest-porc"> 
                      @foreach ($porcentajes as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach 
                      </select>
                  </div>
                   <div class="col-sm-6">
                       <label for="garantiades">Descripción Garantía</label>
                   <textarea class="form-control z-depth-1" id="garantiades" name="garantiades" rows="3" ></textarea>
                       <p class="error text-center alert alert-danger hidden"></p>
                   </div>
              </div>
              @endif
                  
              
              
          </form>
      </div>               

      <div class="box-footer">
          <button type="submit"id="add" class="btn btn-success btn-lg center-block">Actualizar Especificación Técnica</button>
      </div>          
          
          <!-- /.box-body -->
  </div>
</div>

</section>
</div>



@php
    $items=App\Item::with('caracteristicas')->where('condiciontecnica_id',$id_ctec)->get();
@endphp

<script type="text/javascript">
    {{-- ajax Form Add Type Bienes--}}
    $(document).ready( function () {

     var tipogarantiatecnica = @json($tipogarantiatecnica); 
     var serviciosconexos = @json($serviciosconexos);
     var garantiastecnicas = @json($garantiatecnica);
     var servicios = @json($servicios);

     var itemsDB=@json($items);
   
     var items =  llenarListaItems(itemsDB);

     var views_gt=llenarvistasGt(garantiastecnicas,tipogarantiatecnica);
     var vistasItem=maquetarItems(itemsDB);
     var views_tsc = maquetarServicios(servicios,serviciosconexos);

     console.log(views_gt);
     console.log(items);
     console.log(views_tsc);

     //console.log(views_gt);
     var indexfila=itemsDB.length +1;
     var indexcolumna=columnasLenght(itemsDB);
   
    
     i_gt=garantiastecnicas.length;
     i_tsc=servicios.length;

     for (let index = 0; index < vistasItem.length; index++) {
      $('#garantecnica').before(vistasItem[index]);
     }

   
     var idespecificaciontecnica = '<?php echo $id;?>'
     var plazo = '<?php echo $plazo;?>'

     if (plazo<16) {
      $('#divgarantia').hide();
     }
     
   
       $('#masitem').click(function(){
       
       var nombre=$('input[name=item]').val();
       var cantidad=$('input[name=cantidad]').val();
       var unidad=$('input[name=unidad]').val();
       if(validarItem()){
       var item=new Item(indexfila,'vista'+indexfila,nombre,cantidad,unidad,0);
       //item.add('inp-'+indexfila+'0');
       
       var caract= new Caracteristica(0,"inp-"+indexfila+"0");
        item.add(caract);
       
       items.push(item);
       var html='<div class="col-sm-6" id="vista'+indexfila+'">'
                     +'<div class="box box-primary box-solid">'
                       +'<div class="box-header with-border">'
                         +'<h3 class="box-title">'+nombre+' : '+cantidad+' ('+unidad+')</h3>' 
                         +'<div class="box-tools pull-right">'
                           +'<a class="btn btn-box-tool trash-boton" href="#" role="button"><span class="glyphicon glyphicon-trash"></span></a>'
                         +'</div>' 
                         +'<!-- /.box-tools -->'
                       +'</div>'
                       +'<!-- /.box-header -->'
                       +'<div class="box-body" id="'+indexfila+'">'
                         +'<div class="form-group">'
                             +'<div class="col-sm-10">'
                               +'<label for="caracteristicas">Características</label>'
                              +'<input type="text" class="form-control" id="inp-'+indexfila+'0" name="inp-'+indexfila+'0"'
                               +'>'
                               +'<p class="error text-center alert alert-danger hidden"></p>'
                             +'</div>'
                             +'<div class="col-sm-1">'
                                     +'<label for="mas">Acción</label>'
                                     +'<a class="btn btn-success add_boton" href="#" role="button"><span class="glyphicon glyphicon-plus"></span></a>'
                             +'</div>'
                           +'</div>'
                       +'</div>'
                       +'<!-- /.box-body -->'
                     +'</div>'
                     +'<!-- /.box -->'
                   +'</div>';                                                               
         $('#garantecnica').before(html);
         $('#item').val('');
         $('#cantidad').val('');
         $('#unidad').val('');
         console.log(items);              
       indexfila++;
       }
     });
     $('.espeficicacion-form').on("click",".add_boton", function(e){
       e.preventDefault(); 
       var i='#'+$(this).parent().parent('div').parent('div').prop("id");
       var id=$(this).parent().parent('div').parent('div').prop("id");
       console.log("hola")
       console.log(i);
       var html="<div class='form-group' id='inp-"+id+""+indexcolumna+"' name='"+id+"'>"
                       +"<div class='col-sm-10'>"
                         +"<input type='text' class='form-control' id='inp-"+id+""+indexcolumna+"' name='inp-"+id+""+indexcolumna+"'"
                        +">"
                         +"<p class='error text-center alert alert-danger hidden'></p>"
                       +'</div>'
                       +"<div class='col-sm-1'>"
                         +'<a class="btn btn-danger removeItem_button" href="#" role="button"><span class="glyphicon glyphicon-minus"></span></a>'
                       +"</div>"
               +"</div>";
               $(i).append(html);
       let caracteristica={
         'id':id+""+indexcolumna,
         'input':'inp-'+id+""+indexcolumna,
       };
       for (let index = 0; index < items.length; index++) {
         if (items[index].id==id) {
           console.log("Soy Igual: "+items[index].id+"="+id);
           var caract= new Caracteristica(0,"inp-"+id+""+indexcolumna);
           items[index].add(caract);
           //items[index].add("inp-"+id+""+indexcolumna);
         }
       }
       indexcolumna++;
       console.log(items);
     });
     
     //Este procedimiento elimina una caracteristica del Item.
     $('.espeficicacion-form').on("click",".removeItem_button", function(e){
       e.preventDefault(); 
       var id_carats=$(this).parent().parent('div').prop("id");
       var idName='#'+id_carats;
       var id=$(idName).attr('name');
       console.log(id_carats);
       console.log(id);
       for (let i = 0; i < items.length; i++) {
         if (items[i].id==id) {
           items[i].delete(id_carats);
           console.log("ok");
         }
       }
       $(this).parent('div').parent('div').remove(); 
     });

     //Este procedimiento elimina un Item y sus caracteristicas.
     $('.espeficicacion-form').on("click",".trash-boton", function(e){
       e.preventDefault(); 
       var id=$(this).parent().parent('div').parent('div').parent('div').prop("id");
       var vista;
       for (let i = 0; i < items.length; i++) {
         if (items[i].vista==id) {
           var item=items[i]
           var vista=items.indexOf(item);
           if (confirm("Esta seguro de eliminar "+items[i].nombre)) {
             items.splice(vista,1);
             $(this).parent('div').parent('div').parent('div').parent('div').remove(); 
           } 
         }
       }
       console.log(vista);
       console.log('id'+id);
       console.log(items);
     });
   
   
     $('#masgarantecnica').click(function(){
     i_gt++;
       var html="<div class='form-group' id='"+i_gt+"'>"
                   +"<div class='col-sm-7'>"
         
                     +"<input type='text' class='form-control' id='inp-gar"+i_gt+"' name='inp-gar"+i_gt+"'"
                     +"required>"
                     +"<p class='error text-center alert alert-danger hidden'></p>"
                   +"</div>"
                   +"<div class='col-sm-4'>"
                   
                       +"<select class='form-control' name='sel-gar"+i_gt+"'>";
   
                         $.each(tipogarantiatecnica, function(index, value) {
                           html+="<option value='"+index+"'>"+value+"</option>";
                         });
                          
                         html+="</select>"
                       +"</div>"
                   +"<div class='col-sm-1'>"
           
                       +"<button class='btn btn-danger remove_buttongarantia' title='Eliminar'>"
                           +"<span class='glyphicon glyphicon-minus'></span>"
                       +"</button>"
                   +"</div>"
           +"</div>";
               $('#garantecnica').after(html);
               views_gt.push({
               id: i_gt,
               input: "inp-gar"+i_gt,
               select: "sel-gar"+i_gt,
               clave: 0
             });
             console.log(views_gt);
        });
   
       $('#masservicioconexo').click(function(){
       i_tsc++;
       var html="<div class='form-group' id='"+i_tsc+"'>"   
                  +"<div class='col-sm-4'>"
    
                      +"<select class='form-control' name='sel-tsc"+i_tsc+"'>";
   
                         $.each(serviciosconexos, function(index, value) {
                           html+="<option value='"+index+"'>"+value+"</option>";
                         });
                          
                         html+="</select>"
                         +"</div>"
                         +"<div class='col-sm-7'>"
                                   
                                     +"<input type='' class='form-control' id='inp-tsc"+i_tsc+"' name='inp-tsc"+i_tsc+"'"
                                     +"required>"
                                     +"<p class='error text-center alert alert-danger hidden'></p>"
                         +"</div>"
                         +"<div class='col-sm-1'>"
                              
                             +"<button class='btn btn-danger remove_buttonconexo' title='Eliminar'>"
                                 +"<span class='glyphicon glyphicon-minus'></span>"
                             +"</button>"
                         +"</div>"
             +'</div>';
               $('#servicioconexo').after(html);
               views_tsc.push({
               id: i_tsc,
               select: "sel-tsc"+i_tsc,
               input: "inp-tsc"+i_tsc,
               clave: 0
             });
             console.log(views_tsc);
        });
   
   
        //$('.espeficicacion-form').on("click",".remove_button", function(e){
        //     e.preventDefault(); 
        //     $(this).parent('div').parent('div').remove(); 
        //     //console.log(i);
        //     //console.log($(this).parent().parent('div').prop("id"));
        //     var id_carats=$(this).parent().parent('div').prop("id");
        //     for (index=0; index < views_caracts.length; index++) {
        //       if(views_caracts[index].id == id_carats ){
        //        console.log(views_caracts[index].id+" es igual a "+id_carats);
        //        views_caracts.splice(index, 1);
        //        console.log(views_caracts);
        //       }
        //     }
        //   });

          //Este procedimiento elimina garantias tecnicas. 
          $('.espeficicacion-form').on("click",".remove_buttongarantia", function(e){
             e.preventDefault(); 
             $(this).parent('div').parent('div').remove(); 
             //console.log(i);
             //console.log($(this).parent().parent('div').prop("id"));
             var id_gt=$(this).parent().parent('div').prop("id");
             for (index=0; index < views_gt.length; index++) {
               if(views_gt[index].id == id_gt ){
                console.log(views_gt[index].id+" es igual a "+id_gt);
                views_gt.splice(index, 1);
                console.log(views_gt);
               }
             }
           });
           
           //Este procedimiento elimina tipos de servicios conexos. 
           $('.espeficicacion-form').on("click",".remove_buttonconexo", function(e){
             e.preventDefault(); 
             $(this).parent('div').parent('div').remove(); 
             //console.log(i);
             //console.log($(this).parent().parent('div').prop("id"));
             var id_tsc=$(this).parent().parent('div').prop("id");
             for (index=0; index < views_tsc.length; index++) {
               if(views_tsc[index].id == id_tsc ){
                console.log(views_tsc[index].id+" es igual a "+id_tsc);
                views_tsc.splice(index, 1);
                console.log(views_tsc);
               }
             }
           });
   
   
     $(document).on('keyup', '#plazo', function(){
       var query = $(this).val();
   
   
       console.log(query);
       if(query>=16){
         $('#divgarantia').show();
         console.log("es doce");
       }else{
         $('#divgarantia').hide();
       }
      // fetch_customer_data(query);
      });
   
      $("#add").click(function() {
        console.log("registrar");
   
      
       var result_gt =  new Array();
       var result_tsc =  new Array();
       
       for (index=0; index < views_gt.length; index++){
         result_gt.push({"id_selectgt": document.getElementsByName(views_gt[index].select)[0].value,
                          "garantia": $('input[name='+views_gt[index].input+']').val(),
                          "clave": views_gt[index].clave});
       }
       
       for (index=0; index < views_tsc.length; index++){
         result_tsc.push({"id_selecttsc": document.getElementsByName(views_tsc[index].select)[0].value,
                          "servicio": $('input[name='+views_tsc[index].input+']').val(),
                          "clave": views_tsc[index].clave});
       }
   
       for (let i = 0; i < items.length; i++) {
         for (let j = 0; j < items[i].caracteristicas.length; j++) {

           datoViejo=items[i].caracteristicas[j].descripcion;
           
           id='input[name='+datoViejo+']';
           datoNuevo=$(id).val();
           items[i].replace(datoNuevo,datoViejo);
         }
       }
       console.log(items);
   
   

       console.log(result_gt);
       console.log(result_tsc);

       console.log(items);
   
       //console.log(result_gt);
       
       console.log('garantiafunc',$('input[name=garantiafunc]').val());
       console.log('certificacion',$('input[name=certificacion]').val());
       
       //console.log(result_tsc);
       
       console.log('adicional',$('input[name=adicional]').val());
       console.log('inspeccion',$('input[name=inspeccion]').val());
       
       console.log('modalidad',$('input[name=modalidad]').val());
       
       console.log('adjudicacion',document.getElementsByName("select-adjudicacion")[0].value);
       console.log('select-contrato',document.getElementsByName("select-contrato")[0].value);
       
       console.log('select-lugares',document.getElementsByName("select-lugares")[0].value);
       console.log('lugar',$('input[name=lugar]').val());
       console.log('plazo',$('input[name=plazo]').val());
       
       console.log('validez',$('textarea#validez').val());
       console.log('multa',$('textarea#multa').val());
       console.log('porcentaje',$('input[name=porcentaje]').val());
       
       if(plazo>15){
        console.log('select-gar',document.getElementsByName("select-gar")[0].value);
        console.log('select-porc',document.getElementsByName("selest-porc")[0].value);
        console.log('descripcion',$('textarea#garantiades').val());
       }
       
       
       
       console.log('idespecificaciontecnica',idespecificaciontecnica);
   
         var formdata=new FormData();
   
        formdata.append('_token',$('input[name=_token]').val());
   
         formdata.append('items',JSON.stringify(items));
   
         formdata.append('listagarantia',JSON.stringify(result_gt));
   
         formdata.append('garantiafunc',$('input[name=garantiafunc]').val());
         formdata.append('certificacion',$('input[name=certificacion]').val());
   
         formdata.append('listaservicioconex',JSON.stringify(result_tsc));
   
         formdata.append('adicional',$('input[name=adicional]').val());
         formdata.append('inspeccion',$('input[name=inspeccion]').val());
   
         formdata.append('modalidad',$('input[name=modalidad]').val());
   
         formdata.append('adjudicacion',document.getElementsByName("select-adjudicacion")[0].value);
         formdata.append('select-contrato',document.getElementsByName("select-contrato")[0].value);
   
         formdata.append('select-lugares',document.getElementsByName("select-lugares")[0].value);
         formdata.append('lugar',$('input[name=lugar]').val());
         formdata.append('plazo',$('input[name=plazo]').val());
         
         formdata.append('validez',$('textarea#validez').val());
         formdata.append('multa',$('textarea#multa').val());
         formdata.append('porcentaje',$('input[name=porcentaje]').val());
   
         formdata.append('select-gar',document.getElementsByName("select-gar")[0].value);
         formdata.append('select-porc',document.getElementsByName("selest-porc")[0].value);
         formdata.append('descripcion',$('textarea#garantiades').val());
   
         formdata.append('idespecificaciontecnica',idespecificaciontecnica);
   
         $.ajax({
           type: 'POST',
           url: '/solicitud/especificacion/actualizar',
           data:formdata,
           contentType: false,
           processData: false,
           dataType: "json",
           success: function(data){
              if ((data.errors)) {
               console.log("eroor");
               console.log(data);
               //if(data.errors.cite){$('.errorCite').removeClass('hidden');$('.errorCite').text(data.errors.cite);}
               //if(data.errors.fecha){$('.errorFecha').removeClass('hidden');$('.errorFecha').text(data.errors.fecha);}
               //if(data.errors.objeto){$('.errorObjeto').removeClass('hidden');$('.errorObjeto').text(data.errors.objeto);}
               //if(data.errors.objetivo){$('.errorObjetivo').removeClass('hidden');$('.errorObjetivo').text(data.errors.objetivo);}
               //if(data.errors.antecedentes){$('.errorAntecedentes').removeClass('hidden');$('.errorAntecedentes').text(data.errors.antecedentes);}
               //if(data.errors.justificacion){$('.errorJustificacion').removeClass('hidden');$('.errorJustificacion').text(data.errors.justificacion);}
               $('.error').removeClass('hidden');
               $('.error').text(data.errors.item);
               $('.error').text(data.errors.cantidad);
               $('.error').text(data.errors.unidad);
               $('.error').text(data.errors.garantiafunc);
               $('.error').text(data.errors.certificacion);
               $('.error').text(data.errors.adicional);
               $('.error').text(data.errors.inspeccion);
               $('.error').text(data.errors.modalidad);
               $('.error').text(data.errors.lugar);
               $('.error').text(data.errors.plazo);
               $('.error').text(data.errors.validez);
               $('.error').text(data.errors.multa);
               $('.error').text(data.errors.porcentaje);
                
                
                
               }else{
                 console.log("itemdb");
                 console.log(data);
                 //location.href ="/solicitud";
           }
          },
         });       
         
         });
         
    });
     //Clase Item
     function Item(id,vista,nombre, cantidad, unidad, clave){
       this.id=id;
       this.vista=vista;
       this.nombre = nombre;
       this.cantidad = cantidad;
       this.unidad = unidad;
       this.clave = clave;
       this.caracteristicas=new Array();
   
       this.add = function (newObject) {
           this.caracteristicas.push(newObject);
       };

       this.addClave = function (newClave) {
           this.clave = newClave;
       };
       
       this.delete = function (item) {
        // var i = this.caracteristicas.indexOf( item );
        //i !== -1 && this.caracteristicas.splice( i, 1 );
        for (let index = 0; index < this.caracteristicas.length; index++) {
          console.log(this.caracteristicas[index].descripcion);
          if(this.caracteristicas[index].descripcion==item){
            console.log("son iguales");
            this.caracteristicas.splice( index, 1 );
          }
        }
       };
   
       this.replace = function (newVar,oldVar) {
        for (let index = 0; index < this.caracteristicas.length; index++) {
          //console.log(this.caracteristicas[index].descripcion);
          if(this.caracteristicas[index].descripcion==oldVar){
            //console.log("Caratacteristica actualizado");
            this.caracteristicas[index].descripcion=newVar;
          }
        }



         //var i = this.caracteristicas.indexOf(oldVar);
         //this.caracteristicas[i]=newVar;
       };

       this.addArray = function (array,i,x) {
        var caract = new Caracteristica(array[0].id,"inp-"+i+0); 
        this.caracteristicas.push(caract);
        //  clave:array[0].id,
        //  desc:"inp-"+i+0}); 
        let size=array.length+x -1;
        let p=1;
        let t=x+1;
         for (let j = x; j < size; j++) {
        var caracteristica=new Caracteristica(array[p].id,"inp-"+i+t);
          this.caracteristicas.push(caracteristica);
           // clave:array[p].id,
            //desc:"inp-"+i+j
          //});
            p++;  
            t++;
         }
       };
     }
     //Clase caracateristicas
     function Caracteristica(clave, descripcion){
       
       this.clave = clave;
       this.descripcion = descripcion;
   
       this.getClave = function (clave) {
           return this.clave;
       };

       this.getDescripcion = function (descripcion) {
           return this.descripcion;
       };

       this.setDescripcion = function (descripcion) {
           this.descripcion = descripcion;
       };

       this.setClave = function (clave) {
           this.clave = clave;
       };
     }
     
     //Funcion validar Item
     function validarItem(){
    
     var txtItem = document.getElementById('item').value;
     var txtCantidad = document.getElementById('cantidad').value;
     var txtUnidad = document.getElementById('unidad').value;
   
     //Test campo Item
     if(txtItem == null || txtItem.length == 0 || /^\s+$/.test(txtItem)){
       alert('ERROR: El campo Ítem, Lote, etc. N° no debe ir vacío o lleno de solamente espacios en blanco');
       return false;
     }
     //Test Cantidad
     if(txtCantidad == null || txtCantidad.length == 0 || isNaN(txtCantidad) || txtCantidad<=0){
       alert('ERROR: Debe ingresar una cantidad válida');
       return false;
     }
     //Test campo Unidad
     if(txtUnidad == null || txtUnidad.length == 0 || /^\s+$/.test(txtUnidad)){
       alert('ERROR: El campo Unidad no debe ir vacío o lleno de solamente espacios en blanco');
       return false;
     }
     return true;
     }

     function maquetarItems(items){
       var result=new Array();
       let contador=1;
       for (let index = 0; index < items.length; index++) {
         let indexAux=index+1;
         var caracteristicas=items[index].caracteristicas;
         console.log(caracteristicas);
         
          var html='<div class="col-sm-6" id="vista'+indexAux+'">'
                      +'<div class="box box-primary box-solid">'
                        +'<div class="box-header with-border">'
                          +'<h3 class="box-title">'+items[index].nombre+' : '+items[index].cantidad+' ('+items[index].unidad+')</h3>' 
                          +'<div class="box-tools pull-right">'
                            +'<a class="btn btn-box-tool trash-boton" href="#" role="button"><span class="glyphicon glyphicon-trash"></span></a>'
                          +'</div>' 
                          +'<!-- /.box-tools -->'
                        +'</div>'
                        +'<!-- /.box-header -->'
                        +'<div class="box-body" id="'+indexAux+'">';
                          for (let j = 0; j < caracteristicas.length; j++) {
                          let index
                            if (j==0) {
                              html+='<div class="form-group">'
                              +'<div class="col-sm-10">'
                                +'<label for="caracteristicas">Características</label>'
                               +'<input type="text" value="'+caracteristicas[j].descripcion+'" class="form-control" id="inp-'+indexAux+'0" name="inp-'+indexAux+'0"'
                                +'>'
                                +'<p class="error text-center alert alert-danger hidden"></p>'
                              +'</div>'
                              +'<div class="col-sm-1">'
                                      +'<label for="mas">Acción</label>'
                                      +'<a class="btn btn-success add_boton" href="#" role="button"><span class="glyphicon glyphicon-plus"></span></a>'
                              +'</div>'
                            +'</div>'; 
                              
                            }else{
                              html+="<div class='form-group' id='inp-"+indexAux+""+contador+"' name='"+indexAux+"'>"
                                        +"<div class='col-sm-10'>"
                                          +'<input type="text" value="'+caracteristicas[j].descripcion+'" class="form-control" id="inp-'+indexAux+contador+'" name="inp-'+indexAux+contador+'"'
                                         +'>'
                                          +"<p class='error text-center alert alert-danger hidden'></p>"
                                        +'</div>'
                                        +"<div class='col-sm-1'>"
                                          +'<a class="btn btn-danger removeItem_button" href="#" role="button"><span class="glyphicon glyphicon-minus"></span></a>'
                                        +"</div>"
                                +"</div>";
                                contador++;

                            }
                                                         
                          }
                        html+='</div>'
                        +'<!-- /.box-body -->'
                      +'</div>'
                      +'<!-- /.box -->'
                    +'</div>';
          result.push(html);                                                             
      }
      return result;
    }

    function columnasLenght(items){
      var result = 0;

      for (let index = 0; index < items.length; index++) {
        result+=items[index].caracteristicas.length - 1;
      }
      return result + 1;
    }

    function llenarListaItems(items){
      var result=new Array();
      let x=0;
      
      for (let index = 0; index < items.length; index++) {
        let i=index+1;
        var item=new Item(i,'vista'+i,items[index].nombre,items[index].cantidad,items[index].unidad,items[index].id);
        if (index==0) {
          item.addArray(items[index].caracteristicas,i,0);
        }else{
          let j=index-1;
          //x es la cantidad de catacteristicas del anterior Item.
          x+=items[j].caracteristicas.length-1;
          item.addArray(items[index].caracteristicas,i,x);
        }
        
        result.push(item);
      }
      return result;
    }

    function llenarvistasGt(garantiastecnicas,tipogarantiatecnica){
      var result=[{id: 1,input: "inp-gar1",select: "sel-gar1", clave:garantiastecnicas[0].id}];
      let i_gt=2;
      for (let i = 1; i < garantiastecnicas.length; i++) {
        var html="<div class='form-group' id='"+i_gt+"'>"
                +"<div class='col-sm-7'>"
         
                  +"<input type='text' value='"+garantiastecnicas[i].descripcion+"' class='form-control' id='inp-gar"+i_gt+"' name='inp-gar"+i_gt+"'"
                  +"required>"
                  +"<p class='error text-center alert alert-danger hidden'></p>"
                +"</div>"
                +"<div class='col-sm-4'>"
                
                    +"<select class='form-control' name='sel-gar"+i_gt+"'>";
   
                      $.each(tipogarantiatecnica, function(index, value) {
                       
                        if (garantiastecnicas[i].tipogarantiatecnica.id==index) {
                          html+="<option value='"+index+"' selected>"+value+"</option>";
                        } else {
                          html+="<option value='"+index+"'>"+value+"</option>"; 
                        }
                      });
                       
                      html+="</select>"
                    +"</div>"
                +"<div class='col-sm-1'>"
        
                    +"<button class='btn btn-danger remove_buttongarantia' title='Eliminar'>"
                        +"<span class='glyphicon glyphicon-minus'></span>"
                    +"</button>"
                +"</div>"
        +"</div>";
            $('#garantecnica').after(html);
            result.push({
            id: i_gt,
            input: "inp-gar"+i_gt,
            select: "sel-gar"+i_gt,
            clave: garantiastecnicas[i].id
          });
          i_gt++;
      }
      return result;
    }

    function maquetarServicios(servicios,serviciosconexos){
      var result=[{id: 1,select: "sel-tsc1",input: "inp-tsc1",clave:servicios[0].id}];
      i_tsc=2;
      for (let i = 1; i < servicios.length; i++) {
       var html="<div class='form-group' id='"+i_tsc+"'>"   
                  +"<div class='col-sm-4'>"
                      +"<select class='form-control' name='sel-tsc"+i_tsc+"'>";
                         $.each(serviciosconexos, function(index, value) {
                           if (servicios[i].servicioconexo.id == index) {
                            html+="<option value='"+index+"' selected>"+value+"</option>";
                           } else {
                            html+="<option value='"+index+"'>"+value+"</option>";
                           }
                         });
                         html+="</select>"
                         +"</div>"
                         +"<div class='col-sm-7'>"
                                   
                                     +"<input type='text' value='"+servicios[i].descripcion+"' class='form-control' id='inp-tsc"+i_tsc+"' name='inp-tsc"+i_tsc+"'"
                                     +"required>"
                                     +"<p class='error text-center alert alert-danger hidden'></p>"
                         +"</div>"
                         +"<div class='col-sm-1'>"  
                             +"<button class='btn btn-danger remove_buttonconexo' title='Eliminar'>"
                                 +"<span class='glyphicon glyphicon-minus'></span>"
                             +"</button>"
                         +"</div>"
             +'</div>';
               $('#servicioconexo').after(html);
               result.push({
               id: i_tsc,
               select: "sel-tsc"+i_tsc,
               input: "inp-tsc"+i_tsc,
               clave: servicios[i].id
             });
             i_tsc++;
      }
      return result;
    }
   
   
    </script>
    @endsection
    
 