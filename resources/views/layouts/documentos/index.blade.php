@extends('layouts.partials.layout')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
        Documentos
        <small>Notas Internas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Documentos</li>
      </ol>
    </section>

    <section class="content">
    <!--Comienzo -->
    <div class="col-xs-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">Solicitud de Contrataciones</h3>
            </div>
            @php $solicitudes=App\Solicitud::with('tipo','estados')->get() @endphp
                 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="table">
                    <thead>
                      <tr class="text-center success">
                        <th>No</th>
                        <th>Estado</th>
                        <th>Objeto de Contratación</th>
                        <th>Objetivo</th>
                        <th>Fecha</th>
                        <th class="info text-center">Motivo</th>
                        <th>Docs</th>
                        <th class="text-center" width="150px">
                          <a href="#" class="create-modal btn btn-success btn-sm"> <b> Generar Solicitud </b></a>
                        </th>
                      </tr>
                    </thead>
                  
                  {{ csrf_field() }}
                  @php  $no=1;@endphp
                  <tbody id="tablacontenido">
                  @foreach ($solicitud as $value)
                    <tr>
                      <td>{{ $no++ }}</td>
                     <!--ESTADO -->
                     @php $coleccion=$value->estados @endphp

                     @foreach ($coleccion as $item)
                       @if ($item->pivot->activo==1)
                         @if ( in_array($item->id,[1,2] ))
                           <td><span class="label label-warning">{{$item->descripcion}}</span></td>
                         @elseif ( in_array($item->id,[4] ) )
                           <td><span class="label label-info">{{$item->descripcion}}</span></td>
                         @elseif ( in_array($item->id,[3] ) )
                         <td><span class="label bg-maroon">{{$item->descripcion}}</span></td>
                         @elseif ( in_array($item->id,[5,10] ) )
                         <td><span class="label label-danger">{{$item->descripcion}}</span></td>
                         @elseif ( in_array($item->id,[6,8] ) )
                         <td><span class="label bg-maroon">{{$item->descripcion}}</span></td>
                         @endif
                         
                       @endif
                         
                     @endforeach
                    <!--OBJETO DE LA CONTRATACION -->
                      <td>{{ $value->objeto}} <br> <b>{{ $value->tipo->nombre }} </b></td>
                    <!--OBJETIVO DE LA CONTRATACION -->
                      <td>{{ $value->objetivo }}</td>
                    <!--FECHA -->
                      <td>{{ $value->fecha }}</td>
                    <!-- MOTIVO -->
                      @foreach ($coleccion as $item)
                      @if ($item->pivot->activo==1)
                        
                      <td class="info text-center">{{$item->pivot->motivo}}</td>
                      @endif
                          
                      @endforeach
                      <!-- BOTON DOCUMENTOS -->
                      <td>
                        @if(!isset($value->especificaciontecnica_id))
                          <a href="#" class="documentos-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                            data-objeto="{{$value->objeto}}"> 
                            <i class="fa fa-eye"></i>
                          </a>
                        @else
                          <a href="#" class="documentos-dos-modal btn btn-warning btn-sm" data-id="{{$value->id}}" 
                            data-objeto="{{$value->objeto}}"
                            data-esp="{{$value->especificaciontecnica_id}}"
                            
                            @foreach ($coleccion as $item)
                            @if ($item->pivot->activo==1)

                            data-estado="{{$item->id}}"> 
                            @endif
                            @endforeach
                            <i class="fa fa-eye"></i>
                          </a>
                        @endif

                        <!-- ESPECIFICACION TECNICA A REGISTRAR-->
                        @if(!isset($value->especificaciontecnica_id))
                          @switch($value->subtipo->id)
                          
                          @case(1)
                          <a href="{{ url('solicitud/especificacionbienes/'.$value->id) }}" class="especificacion-modal btn btn-primary btn-sm" title="AÑADIR ESPECIFICACIÓN TÉCNICA"><i class="glyphicon glyphicon-plus"></i></a>
                              @break
                          @case(2)
                          <a href="{{ url('solicitud/especificacionservicios/'.$value->id) }}" class="especificacion-modal btn btn-primary btn-sm" title="AÑADIR ESPECIFICACIÓN TÉCNICA"><i class="glyphicon glyphicon-plus"></i></a>
                              @break
                          @case(3)
                          <a href="{{ url('solicitud/especificacionconsultoria/'.$value->id) }}" class="especificacion-modal btn btn-primary btn-sm" title="AÑADIR ESPECIFICACIÓN TÉCNICA"><i class="glyphicon glyphicon-plus"></i></a>
                          @break
                          @default

                          @endswitch
                        @endif
                      </td>

                      <!-- DEMAS BOTONES -->
                      <td>
                        @foreach ($coleccion as $item)
                        @if ($item->pivot->activo==1)
                        @if (in_array($item->id,[1]))
                          @if(isset($value->especificaciontecnica_id))
                            <a href="#" class="send-modal btn btn-primary btn-sm" data-id="{{$value->id}}"
                            data-id="{{$value->id}}"
                            data-objeto="{{$value->objeto}}"
                            data-objetivo="{{$value->objetivo}}"
                            data-estado="{{$item->id}}"
                            data-tipo="{{$value->tipo->nombre}}">
                            <i class="fa fa-send"></i>
                            </a>
                          @endif
                            <a href="#" class="edit-modal btn btn-info btn-sm">
                              <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                            <a href="#" class="delete-modal btn btn-danger btn-sm">
                              <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        @elseif (in_array($item->id,[3]))
                        <a href="#" class="send-modal btn btn-primary btn-sm" data-id="{{$value->id}}"
                          data-id="{{$value->id}}"
                          data-objeto="{{$value->objeto}}"
                          data-objetivo="{{$value->objetivo}}"
                          data-estado="{{$item->id}}"
                          data-tipo="{{$value->tipo->nombre}}">
                          <i class="fa fa-send"></i>
                          </a>
                          <a href="#" class="edit-modal btn btn-info btn-sm">
                            <i class="glyphicon glyphicon-pencil"></i>
                          </a>
                          @elseif (in_array($item->id,[4]))
                          <a href="#" class="send-modal btn btn-success btn-sm" data-id="{{$value->id}}"
                            data-id="{{$value->id}}"
                            data-objeto="{{$value->objeto}}"
                            data-objetivo="{{$value->objetivo}}"
                            data-estado="{{$item->id}}"
                            data-tipo="{{$value->tipo->nombre}}">
                            <i class="fa fa-send"></i>
                            </a>


                          @endif
                        @endif
                        @endforeach
                        
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                    
                </table>
              </div>
            
            </div>
        </div>  
      </div>
      {{-- Modelo Formulario: Crear Solicitud --}}
      <div id="create" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal request-form" role="form" enctype="multipart/form-data" id="formSol">
      
                <div class="form-group" id="{{$no}}">  
                  <div class="col-sm-6">
                      <label>Actividades</label>
                      <select name="actividades" id="actividades" class="form-control">
                        <option disabled selected>Seleccione</option>
                  
                        @foreach( $actividades as $key => $value )
                        <option value="{{ $key }}">{{ $key." - ".$value }}</option>
                        @endforeach
                    
                      </select>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Partidas</label><br>
                        <select class="form-control" multiple="multiple" style="width: 95%;" name="codigoPartidas" id="codigoPartidas" >
                          
                      
                        </select>
                    </div>
                    
                </div>
                </div>
      
                <div class="form-group">
                      <div class="col-sm-6">
                        <label for="cite">Cite</label>
                        <input type="text" class="form-control" id="cite" name="cite"
                        placeholder="Ej: DGE-JNC-O247-NOT/19" required>
                        <p class="errorCite text-center alert alert-danger hidden"></p>
                      </div>
                      <div class="col-sm-6">
                          <label for="objetivo">Fecha</label>
                          <input type="date" class="form-control" id="fechanota" name="fechanota" required>
                          <p class="errorFecha text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
      
                <div class="form-group">  
                  <div class="col-sm-6">
                      <label>Tipo de Contratacion</label>
                      <select name="type1" id="type1" class="form-control">
                        <option disabled selected>Seleccione</option>
                  
                        @foreach( $tipocontratacion as $key => $value )
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    
                      </select>
                  </div>

                  <div class="col-sm-6">
                      <label>Subtipo</label>
                      <select name="type2" id="type2" class="form-control">
                        <option disabled selected>Seleccione</option> 
                    
                      </select>
                  </div>
                </div>
      
                <div class="form-group">
                  <div class="col-sm-12">
                    <label for="objeto">Objeto de la Contratación</label>
                    <input type="text" class="form-control" id="objeto" name="objeto"
                    placeholder="Ingrese el objeto" required>
                    <p class="errorObjeto text-center alert alert-danger hidden"></p>
                  </div>
                </div>
      
                <div class="form-group">
                    <div class="col-sm-12">
                      <label for="objetivo">Objetivo de la Contratación</label>
                      <input type="text" class="form-control" id="objetivo" name="objetivo"
                      placeholder="Ingrese el objetivo" required>
                      <p class="errorObjetivo text-center alert alert-danger hidden"></p>
                    </div>
                  </div>
      
                <div class="form-group shadow-textarea">
                    <div class="col-sm-12">
                      <label for="antecedentes">Antecedentes</label>
                      <textarea class="form-control z-depth-1" id="antecedentes" name="antecedentes" rows="3" placeholder="Escribe algo aquí..."></textarea>
                      <p class="errorAntecedentes text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                
                <div class="form-group shadow-textarea">
                  <div class="col-sm-12">
                    <label for="justificacion">Justificación Técnica</label>
                    <textarea class="form-control z-depth-1" id="justificacion" name="justificacion" rows="3" placeholder="(Incluir los resultados o productos que se pretende alcanzar con la adquisición o contratación. Para bienes, incluir la justificación del destino y uso de los bienes a ser adquiridos)"></textarea>
                    <p class="errorJustificacion text-center alert alert-danger hidden"></p>
                  </div>
              </div>
      
              <div class="form-group purple-border" id="divdirecta" name="divdirecta">
                <div class="col-sm-12">
                  <label for="justificacion">Justificación de Contratación Directa</label>
                  <textarea class="form-control z-depth-1" id="directa" name="directa" rows="3" placeholder="(Incluir la justificación de acuerdo a las causales establecidas en el Artículo 72 de las NB-SABS D.S. 0181, cuando corresponda)"></textarea>
                  <p class="errorDirecta text-center alert alert-danger hidden"></p>
                </div>
              </div>
      
              <div class="form-group">  
                  <div class="col-sm-12">
                    
                      <label for="cotizacion">Subir Cotización</label>
                      <input type="file" class="form-control-file" id="cotizacion" name="cotizacion">
                      <p class="errorCotizacion text-center alert alert-danger hidden"></p>
                  </div>
              </div>
      
                <div class="form-group">  
                    <div class="col-sm-6">
                          <label for="title">Partidas</label>
                          <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Busque por codigo ó descripción" />
                    </div>
                </div>
      
                <div class="form-group">  
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                       <thead>
                        <tr>
                         <th>Codigo</th>
                         <th>Descripción</th>
                         <th>Seleccionados</th>
                        </tr>
                       </thead>
                       <tbody name="partidas" id="partidas">
                
                       </tbody>
                      </table>
                     </div>
                  </div>
              </div>
              </form>
            </div>
                <div class="modal-footer">
                  <button class="btn btn-primary" type="submit" id="add">
                    <span class="glyphicon glyphicon-plus"></span>Generar Solicitud
                  </button>
                  <button class="btn btn-danger" type="button" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remobe"></span>Cerrar
                  </button>
                </div>
          </div>
        </div>
      </div>
      {{-- Modal Form Editar y Eliminar Solicitud --}}
      <div id="myModal"class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" role="modal">
                <!--<div class="form-group">
                  <label class="control-label col-sm-2"for="id">ID</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="fid" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2"for="title">Descripción</label>
                  <div class="col-sm-10">
                  <input type="name" class="form-control" id="t">
                  </div>
                </div>-->
                {{-- Modal Form Editar --}}
                <div class="form-group">
                  <div class="col-sm-6">
                    <label for="cite">Cite</label>
                    <input type="text" class="form-control" id="editcite"
                    placeholder="Ej: DGE-JNC-O247-NOT/19" required>
                    <p class="errorCite text-center alert alert-danger hidden"></p>
                  </div>
                  <div class="col-sm-6">
                      <label for="objetivo">Fecha</label>
                      <input type="date" class="form-control" id="editfechanota" required>
                      <p class="errorFecha text-center alert alert-danger hidden"></p>
                    </div>
                </div>
      
            <div class="form-group">  
              <div class="col-sm-6">
                  <label>Tipo de Contratacion</label>
                  <select name="type1" id="type1" class="form-control">
                    <option disabled selected>Seleccione</option>
              
                    @foreach( $tipocontratacion as $key => $value )
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                
                  </select>
              </div>
              <div class="col-sm-6">
                  <label>Subtipo</label>
                  <select name="type2" id="type2" class="form-control">
                    <option disabled selected>Seleccione</option> 
                
                  </select>
              </div>
            </div>
      
            <div class="form-group">
              <div class="col-sm-12">
                <label for="objeto">Objeto de la Contratación</label>
                <input type="text" class="form-control" id="editobjeto"
                placeholder="Ingrese el objeto" required>
                <p class="errorObjeto text-center alert alert-danger hidden"></p>
              </div>
            </div>
      
            <div class="form-group">
                <div class="col-sm-12">
                  <label for="objetivo">Objetivo de la Contratación</label>
                  <input type="text" class="form-control" id="editobjetivo" 
                  placeholder="Ingrese el objetivo" required>
                  <p class="errorObjetivo text-center alert alert-danger hidden"></p>
                </div>
              </div>
      
            <div class="form-group shadow-textarea">
                <div class="col-sm-12">
                  <label for="antecedentes">Antecedentes</label>
                  <textarea class="form-control z-depth-1" id="editantecedentes" rows="3" placeholder="Escribe algo aquí..."></textarea>
                  <p class="errorAntecedentes text-center alert alert-danger hidden"></p>
                </div>
            </div>
            
            <div class="form-group shadow-textarea">
              <div class="col-sm-12">
                <label for="justificacion">Justificación Técnica</label>
                <textarea class="form-control z-depth-1" id="editjustificacion" rows="3" placeholder="Escribe algo aquí..."></textarea>
                <p class="errorJustificacion text-center alert alert-danger hidden"></p>
              </div>
          </div>
      
          <div class="form-group">  
              <div class="col-sm-12">
                
                  <label for="cotizacion">Subir Cotización</label>
                  <input type="file" class="form-control-file" id="editcotizacion">
                  <p class="errorCotizacion text-center alert alert-danger hidden"></p>
              </div>
          </div>
      
              </form>
                      {{-- Formulario: Eliminar Solicitud--}}
              <div class="deleteContent">
                Are You sure want to delete <span class="title"></span>?
                <span class="hidden id"></span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn actionBtn" data-dismiss="modal">
                <span id="footer_action_button" class="glyphicon"></span>
              </button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                <span class="glyphicon glyphicon"></span>close
              </button>
            </div>
          </div>
        </div>
      </div>
      
      {{-- Modelo Formulario: Crear Especificacines Tecnicas --}}
      <div id="create-especificacion" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal request-form" role="form" >
                <div class="form-group">  
                  <div class="col-sm-6">
                      <label>Tipo de Contratacion</label>
                      <select name="type1" id="type1" class="form-control">
                        <option disabled selected>Seleccione</option>
                  
                        @foreach( $tipocontratacion as $key => $value )
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    
                      </select>
                  </div>
                  <div class="col-sm-6">
                      <label>Subtipo</label>
                      <select name="type2" id="type2" class="form-control">
                        <option disabled selected>Seleccione</option> 
                    
                      </select>
                  </div>
                </div>
      
                <div class="form-group">
                  <div class="col-sm-12">
                    <label for="objeto">Objeto de la Contratación</label>
                    <input type="text" class="form-control" id="objeto" name="objeto"
                    placeholder="Ingrese el objeto" required>
                    <p class="errorObjeto text-center alert alert-danger hidden"></p>
                  </div>
                </div>
      
                <div class="form-group">
                    <div class="col-sm-12">
                      <label for="objetivo">Objetivo de la Contratación</label>
                      <input type="text" class="form-control" id="objetivo" name="objetivo"
                      placeholder="Ingrese el objetivo" required>
                      <p class="errorObjetivo text-center alert alert-danger hidden"></p>
                    </div>
                  </div>
      
                <div class="form-group shadow-textarea">
                    <div class="col-sm-12">
                      <label for="antecedentes">Antecedentes</label>
                      <textarea class="form-control z-depth-1" id="antecedentes" name="antecedentes" rows="3" placeholder="Escribe algo aquí..."></textarea>
                      <p class="errorAntecedentes text-center alert alert-danger hidden"></p>
                    </div>
                </div>
      
                <div class="form-group shadow-textarea">
                    <div class="col-sm-12">
                      <label for="justificacion">Justificación Técnica</label>
                      <textarea class="form-control z-depth-1" id="justificacion" name="justificacion" rows="3" placeholder="Escribe algo aquí..."></textarea>
                      <p class="errorJustificacion text-center alert alert-danger hidden"></p>
                    </div>
                </div>
      
                <div class="form-group">  
                    <div class="col-sm-6">
                          <label for="title">Partidas</label>
                          <input type="text" name="buscarr" id="buscarr" class="form-control" placeholder="Busque por codigo ó descripción" />
                    </div>
                </div>
      
                <div class="form-group">  
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                       <thead>
                        <tr>
                         <th>Codigo</th>
                         <th>Descripción</th>
                         <th>Seleccionados</th>
                        </tr>
                       </thead>
                       <tbody name="partidass" id="partidass">
                
                       </tbody>
                      </table>
                     </div>
                  </div>
              </div>
              </form>
            </div>
                <div class="modal-footer">
                  <button class="btn btn-warning" type="submit" id="add">
                    <span class="glyphicon glyphicon-plus"></span>Generar Solicitud
                  </button>
                  <button class="btn btn-warning" type="button" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remobe"></span>Cancelar
                  </button>
                </div>
          </div>
        </div>corregido
      </div>
      
      
      {{-- Modal Form Show Solicitud --}}
<div id="send" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
            <div class="modal-body">


                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row"><label for="">ID :</label></th>
                        <td id="tid"></td>
                      </tr>
                      
                      <tr>
                        <th scope="trow"><label for="">OBJETO :</label></th>
                        <td id="tobjeto"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">OBJETIVO :</label></th>
                        <td id="tobjetivo"></td>
                      </tr>

                      <tr>
                        <th scope="row"><label for="">TIPO :</label></th>
                        <td id="ttipo"></td>
                      </tr>

                    </tbody>
                  </table>
                  <form class="form-horizontal">
                  <div class="form-group" id="div-motivo">  
                    <div class="col-sm-12">
                          <label for="title">Motivo</label>
                          <input type="text" name="motivo" id="motivo" class="form-control" placeholder="Ingrese el motivo" />
                    </div>
                  </div>
                  </form>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" id="btnCorreccion">
                  <span class="glyphicon glyphicon-send"> </span> Enviar Correccion
                </button>
                <button class="btn btn-success" type="submit" id="btnSol">
                  <span class="glyphicon glyphicon-send"> </span> Enviar Solicitud
                </button>
                <button class="btn btn-warning" type="submit" id="btnSolicitud">
                  <span class="glyphicon glyphicon-send"> </span> Enviar Solicitud
                </button>
                <button class="btn btn-default" type="button" data-dismiss="modal">
                  Cancelar
                </button>
            </div>
      </div>
      

    </div>
  </div>
      {{-- Modal Form Editar y Eliminar Solicitud --}}
      <div id="myModal"class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" role="modal">
                <div class="form-group">
                  <label class="control-label col-sm-2"for="id">ID</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="fid" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2"for="title">Descripción</label>
                  <div class="col-sm-10">
                  <input type="name" class="form-control" id="t">
                  </div>
                </div>
              </form>
                      {{-- Formulario: Eliminar Solicitud--}}
              <div class="deleteContent">
                Are You sure want to delete <span class="title"></span>?
                <span class="hidden id"></span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn actionBtn" data-dismiss="modal">
                <span id="footer_action_button" class="glyphicon"></span>
              </button>
              <button type="button" class="btn btn-warning" data-dismiss="modal">
                <span class="glyphicon glyphicon"></span>close
              </button>
            </div>
          </div>
        </div>
      </div>
    <!--Fin-->
    </section>
</div>

{{-- Modal Form Show dcumentos --}}
<div id="show-documentos" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="sol-id"></td>
                  <td ><b id="sol-objeto"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="coti-id"></td>
                  <td><b id="coti-detalle"></b></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>
{{-- Modal Form Show dcumentos dos --}}
<div id="show-documentos-dos" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="dos-modal-title"></h4>
      </div>
          <div class="modal-body">
            <h4>OBJETO: <b id="dos-titulodocs"></b> </h4>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" WIDTH="150">DOCUMENTO</th>
                  <th scope="col">TIPO</th>
                </tr>
              </thead>
              <tbody>
                <!-- SOLICITUD DE CONTRATACION -->
                <tr>
                  <th scope="row"><b>1</b></th>
                  <td id="dos-sol-id"></td>
                  <td ><b id="dos-sol-detalle"></b></td>
                </tr>
                <!-- COTIZACION -->
                <tr>
                  <th scope="row"><b>2</b></th>
                  <td id="dos-coti-id"></td>
                  <td><b id="dos-coti-detalle"></b></td>
                </tr>
                <!-- ESPECIFICACION TECNICA -->
                <tr>
                  <th scope="row"><b>3</b></th>
                  <td id="dos-esp-id"></td>
                  <td><b id="dos-esp-detalle"></b></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>Cerrar
            </button>
          </div>
    </div>
  </div>
</div>



<script type="text/javascript" src="{{asset("js/notaInterna.js")}}"></script>

<script type="text/javascript">
  {{-- ajax Form Add Type Supplier--}}
$(document).ready( function () {  
  $('#codigoPartidas').select2();
  var no = "<?php echo $no;?>";
  var idsol;
  var idesp;
  console.log(no);
  
// var checks =  new Array();
var array_check=[];
//var i=1; 
//views_name=[{id: 1,select: "sel-1",input: "inp-1"}];
//Flat red color scheme for iCheck  
$('#table').DataTable({
  'paging'      : true,
  'lengthChange': true,
  'searching'   : true,
  'info'        : true,
  'autoWidth'   : false,
  "pageLength": 20,
  "language": {
  "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
}
});   
  
});

$(document).on('click', '.documentos-modal', function() {
  idsol=$(this).data('id');
  console.log(idsol);
  $('#show-documentos').modal('show');
  $('#titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  $('#sol-id').html("<a href='solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>");
  $('#sol-objeto').text("Solicitud");
  /* COTIZACION */
  $('#coti-id').html("<a href='solicitud/cotizacion/"+idsol+"'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>");
  $('#coti-detalle').text("Cotizacion");

  $('.modal-title').text('Documentos');
});

$(document).on('click', '.documentos-dos-modal', function() {
  idsol=$(this).data('id');
  idesp=$(this).data('esp');
  var estado=$(this).data('estado');
  console.log(idsol);
  $('#show-documentos-dos').modal('show');
  $('#dos-titulodocs').text($(this).data('objeto'));
  /* SOLICITUD DE CONTRATACION */
  if (estado==1||estado==6) {
    $('#dos-sol-id').html("<a href='solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>"+
    "&nbsp <a href='#' class='btn btn-info btn-sm'><i class='glyphicon glyphicon-pencil'></i></a>");
  }else{
    $('#dos-sol-id').html("<a href='solicitud/pdf/"+idsol+"' class='especificacion-modal' title='NOTA INTERNA'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>");
  }
  $('#dos-sol-detalle').text("Solicitud");
  
  /* COTIZACION */
  if (estado==1||estado==6) {
    $('#dos-coti-id').html("<a href='solicitud/cotizacion/"+idsol+"'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>"+
    "&nbsp <a href='#' class='btn btn-info btn-sm'><i class='glyphicon glyphicon-pencil'></i></a>");
  } else {
    $('#dos-coti-id').html("<a href='solicitud/cotizacion/"+idsol+"'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>");
  }
  $('#dos-coti-detalle').text("Cotización");
  
  /* espeficicacion  */
  if (estado==1||estado==6) {
    $('#dos-esp-id').html("<a href='solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>"+
   "&nbsp <a href='solicitud/especificacionbienes/edit/"+idesp+"' class='btn btn-info btn-sm'><i class='glyphicon glyphicon-pencil'></i></a>");
  }else{
   $('#dos-esp-id').html("<a href='solicitud/especificacion/pdf/"+idesp+"' class='especificacion-modal' title='ESPECIFICACIÓN TÉCNICA'><img src='images/pdf-logo.png' width='40' height='40' alt='Submit'></a>");
  }
  $('#dos-esp-detalle').text("Especificación Técnica");


  
  $('.dos-modal-title').text('Documentos');
});
</script>






@endsection





