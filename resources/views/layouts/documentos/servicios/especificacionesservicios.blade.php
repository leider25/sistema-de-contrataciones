@extends('layouts.partials.layout')
@section('content')
<div class="box-header">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('solicitud') }}"> volver</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="box box-solid box-warning">
        <div class="box-header with-border">
          <h1 class="box-title"><b>ESPECIFICACIÓN TÉCNICA - SERVICIOS (CONTRATACIÓN MENOR Ó DIRECTA)</b></h1>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal espeficicacion-form">
                    <h3 style="text-align: center"><span class="label label-info">CONDICIONES TÉCNICAS</span></h3><br>

                        <div class="form-group" id="espservicio" name="espservicio">
                                <div class="col-sm-11">
                                  <label for="espservicio">Especificaciones Tecnicas del Servicio</label>
                                  <input type="text" class="form-control" id="inp-serv1" name="inp-serv1"
                                  required>
                                  <p class="error text-center alert alert-danger hidden"></p>
                                </div>
                                <div class="col-sm-1">
                                        <label for="mas">Acción</label>
                                    <button class="btn btn-success" id="mas-esp-serv" title="mas-esp-serv">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                        </div>
                        <div class="form-group" id="garantecnica" name="garantecnica">
                                <div class="col-sm-7">
                                  <label for="garantia">Garantías técnicas</label>
                                  <input type="text" class="form-control" id="inp-gar1" name="inp-gar1"
                                  required>
                                  <p class="error text-center alert alert-danger hidden"></p>
                                </div>
                                <div class="col-sm-4">
                                        <label for="tipo">Tipo de Garantía</label>    
                                    <select class="form-control" name="sel-gar1">
                                          
                                        <option value="1">Bolivia</option>
                                  
                                        </select>
                                    </div>
                                <div class="col-sm-1">
                                        <label for="mas">Acción</label>
                                    <button class="btn btn-success" id="masgarantecnica"  title="Añadir">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                        </div>
                    <br><h3 style="text-align: center"><span class="label label-info">CONDICIONES ADMINISTRATIVAS</span></h3><br>
                    
                    <div class="form-group">
                            <div class="col-sm-6">
                              <label for="modalidad">Modalidad de Contratación</label>
                              <input type="text" class="form-control" id="modalidad" name="modalidad"
                               required>
                              <p class="error text-center alert alert-danger hidden"></p>
                            </div>
                            <div class="col-sm-3">
                              <label for="tipo">Forma de Adjudicación</label>    
                                    <select class="form-control" name="select-adjudicacion">
                                        <option value="1">Bolivia</option>
                                    </select>
                            </div>

                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="tipo">Lugar</label>    
                                  <select class="form-control" name="select-lugares">
                                    <option value="1">Bolivia</option>
                            
                               
                                  </select>
                          </div>
                        <div class="col-sm-7">
                          <label for="lugar">Lugar de prestacion del servicio</label>
                          <input type="text" class="form-control" id="lugar" name="lugar"
                           required>
                          <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-sm-3">
                          <label for="plazo">Plazo de prestación del servicio</label>
                          <div class="input-group">
                            <input type="number" class="form-control" id="plazo" name="plazo" required>
                            <span class="input-group-addon">Días</span>
                          </div>
                          <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="col-sm-5">
                              <label for="validez">Validez de Propuesta</label>
                              <textarea class="form-control z-depth-1" id="validez" name="validez" rows="3" placeholder="Establecer la validez de la propuesta, la misma que no deberá ser menor a treinta (30) días calendario"></textarea>
                              <p class="error text-center alert alert-danger hidden"></p>
                            </div>
                            <div class="col-sm-5">
                               <label for="multa">Multas</label>
                               <textarea class="form-control z-depth-1" id="multa" name="multa" rows="3" placeholder="cuando se requiera sobre los bienes demorados, estableciendo el porcentaje y la forma de aplicación"></textarea>
                               <p class="error text-center alert alert-danger hidden"></p>
                             </div>
                             <div class="col-sm-2">
                                <label for="porcentaje">% multa</label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="porcentaje" name="porcentaje" required>
                                  <span class="input-group-addon">%</span>
                                </div>
                                <p class="error text-center alert alert-danger hidden"></p>
                              </div>
                             <!--
                              <div class="col-sm-4">
                                <label for="cumplimento">Garantía de cumplimiento de Contrato</label>
                                <textarea class="form-control z-depth-1" id="cumplimiento" name="cumplimiento" rows="3" placeholder="Cuando la entrega del bien o servicio sea mayor a 15 días calendario"></textarea>
                                <p class="error text-center alert alert-danger hidden"></p>
                            </div>-->
                    </div>
                    <div class="form-group" id="divgarantia" name="divgarantia">
                        <div class="col-sm-3  " >
                            <label for="tipo">Tipo de Gantantía</label>    
                                  <select class="form-control" name="select-gar">
                                        <option value="1">Bolivia</option>
                                 
                                  </select>
                          </div>
                          <div class="col-sm-3">
                              <label for="tipo">% Garantía</label>    
                                    <select class="form-control" name="selest-porc">
                              
                                    </select>
                        </div>

                        
                              <div class="col-sm-6">
                                <label for="garantiades">Descripción Garantía</label>
                                <textarea class="form-control z-depth-1" id="garantiades" name="garantiades" rows="3" ></textarea>
                                <p class="error text-center alert alert-danger hidden"></p>
                            </div>
                        
                </div>
              </form>
                </div>               

                <div class="box-footer">
                    <button type="submit"id="add" class="btn btn-primary btn-lg center-block">Registrar Especificación Técnica</button>
                </div>          
        
        <!-- /.box-body -->
      </div>
</div>

@endsection