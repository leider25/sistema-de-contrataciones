@extends("layouts/partials.layout")
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bienvenido
        <small>FONABOSQUE</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Bienvenida</li>
      </ol>
    </section>



    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-success">
            <div class="box-header">
              <h3 class="box-title">Reporte de Procesos</h3>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table id="boletas" class="table table-bordered table-hover table-striped table-condenced">
                  <thead>
                  <tr class="text-center success">
                    <th width="1%">#</th>
                    <th width="3%">Estado</th>
                    <th width="12%" class="success text-center">Objeto</th>
                    <th width="12%">Tipo</th>
                    <th width="6%" class="info text-center">Solicitud</th>
                    <th width="6%" class="danger text-center">Cotización</th>
                    <th width="6%" class="warning text-center">Especificacion Técnica</th>
         
                  </tr>
                  </thead>
                  <tbody>
       
                    <tr>
                      <td colspan="9" align="center"> SIN REGISTROS</td>
                    </tr>
                 
                  </tbody>
                </table>
              </div>
            </div>
            <div
                class="pagination-wrapper"> </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.main (body) -->
      </div>

    </section>
  </div>
    
@endsection