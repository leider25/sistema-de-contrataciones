@extends('layouts.app')

@section('contento')
<div class="container">
        @if (session()->has('flash'))
        <div class="alert alert-success">{{session('flash')}}</div>
    @endif
    <div class="row justify-content-center">
        
        <div class="col-md-4" style="margin: 7% auto;">
            
                    <img class="card-img-top" src="{{asset("dist/img/fonabosque-logo.png")}}" alt="Card image cap">
            <br><br>
            <div class="card">
                    
                <div class="card-header" style="text-align: center"><a href="#"><b>SISTEMA DE CONTRATACIONES</b></a></div> 

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        
                        <div class="form-group row">
                            

                            <div class="col-md-12">
                                <input id="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Nombre de Usuario" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Clace de acceso" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                   

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Ingresar') }}
                                </button>

                           
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
