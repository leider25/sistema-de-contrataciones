<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{
    protected $connection = "contrataciones";
    protected $table = "caracteristicas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion',
        'item_id'
    ];

    public function item()
	{
		return $this->belongsTo(Item::class);
    }
}
