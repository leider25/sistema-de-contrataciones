<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $connection = "sispoa";
    protected $table = "actividades";

		protected $fillable = [
			'cod_actividad',
            'categoria'
        ];
}
