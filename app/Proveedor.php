<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
	protected $connection = "contrataciones";
    protected $table = "proveedores";

	protected $primaryKey = 'id';

	protected $fillable = [
		'descripcion',
		'direccion',
		'encargado_id'
    ];
    
    public function referencia()
	{
		return $this->belongsToMany(Referencia::class)->withTimestamps();
	}
	public function encargado()
	{
		return $this->belongsTo(Encargado::class);
	}
	public function adjudicaciones()
	{
		return $this->hasMany(Adjudicacion::class);
	}
}
