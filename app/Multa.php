<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multa extends Model
{
    protected $connection = "contrataciones";
    protected $table = "multas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion',
        'porcentaje_multa'
    ];
    public function condiciones_administrativas()
    {
        return $this->hasMany(Condicionadministrativa::class);
    }
}
