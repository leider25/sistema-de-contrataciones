<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = "contrataciones";
    protected $table = "estados";

	protected $primaryKey = 'id';

	protected $fillable = [
		'id',
		'descripcion'
    ];
    
    public function proveedores()
	{
		return $this->hasMany(Proveedor::class);
    }
    public function solicitudes()
	{
		return $this->belongsToMany(Solicitud::class)->withPivot('usuario_id', 'motivo', 'activo')->withTimestamps();
    }
}
