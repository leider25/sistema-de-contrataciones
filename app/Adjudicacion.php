<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjudicacion extends Model
{
    protected $connection = "contrataciones";
    protected $table = "adjudicaciones";

	protected $primaryKey = 'id';

	protected $fillable = [
		'fecha',
		'monto',
		'proveedor_id',
		'solicitud_id'
    ];
    
	public function proveedor()
	{
		return $this->belongsTo(Proveedor::class);
    }
    public function solicitud()
	{
		return $this->belongsTo(Solicitud::class);
	}
}
