<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicioconexo extends Model
{
    protected $connection = "contrataciones";
    protected $table = "servicios_conexos";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];
    
    public function servicios()
	{
		return $this->hasMany(Servicio::class);
	}
}
