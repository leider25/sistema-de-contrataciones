<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = "rrhh";
    protected $fillable = [
        'username',
        'description',
        'name',
        'password',
        'department_id',
        'depend',
        'userstypes_id',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //public function area()
	//{
	//	return $this->belongsTo(Area::class);
    //}
    public function solicitud()
	{
		return $this->hasMany('App\Solicitud');
    }
    public function department()
	{
		return $this->belongsTo(Despartamento::class);
	}
}
