<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condicionadministrativa extends Model
{
    protected $connection = "contrataciones";
    protected $table = "condiciones_administrativas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'validez_propuesta',
        'tipoadjudicacion_id',
        'tipocontrato_id',
        'entrega_id',
        'garantia_id',
        'multa_id'
    ];

    public function garantias()
	{
		return $this->belongsTo(Garantia::class);
    }

    public function entregas()
	{
		return $this->belongsTo(Entrega::class);
    }

    public function multas()
	{
		return $this->belongsTo(Multa::class);
    }

    public function tipo_adjudicacion()
	{
		return $this->belongsTo(Tipoadjudicacion::class);
    }

    public function tipo_contrato()
	{
		return $this->belongsTo(Tipocontrato::class);
    }
    
    public function especificaciones_tecnicas()
		{
			return $this->hasMany(Especificaiontecnica::class);
		}
}
