<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $connection = "contrataciones";
    protected $table = "servicios";

	protected $primaryKey = 'id';

	protected $fillable = [
        'descripcion',
        'servicioconexo_id',
        'condiciontecnica_id'
    ];

    public function servicioconexo()
	{
		return $this->belongsTo(Servicioconexo::class);
    }

    public function condiciontecnica()
	{
		return $this->belongsTo(Condiciontecnica::class);
    }


}
