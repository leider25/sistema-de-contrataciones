<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encargado extends Model
{
    protected $connection = "contrataciones";
    protected $table = "encargados";

		protected $primaryKey = 'id';

		protected $fillable = [
			'nombre',
			'cargo'
        ];
        
        public function proveedores()
		{
			return $this->hasMany(Proveedor::class);
		}
}
