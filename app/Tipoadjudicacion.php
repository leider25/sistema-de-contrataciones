<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipoadjudicacion extends Model
{
    protected $connection = "contrataciones";
    protected $table = "tipo_adjudicacion";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];
    public function condiciones_administrativas()
    {
        return $this->hasMany(Condicionadministrativa::class);
    }
}
