<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especificaiontecnica extends Model
{
    protected $connection = "contrataciones";
    protected $table = "especificaciones_tecnicas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'condiciontecnica_id',
        'condicionadministrativa_id'
    ];
    public function condicionadministrativa()
	{
		return $this->belongsTo(Condicionadministrativa::class);
    }
    public function condiciontecnica()
	{
		return $this->belongsTo(Condiciontecnica::class);
    }
    public function solicitud()
    {
        return $this->hasMany(Solicitud::class);
    }
    
}
