<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    protected $connection = "contrataciones";
    protected $table = "referencias";

		protected $primaryKey = 'id';

		protected $fillable = [
            'descripcion',
            'tiporeferencia_id'
        ];
        
        public function proveedores()
		{
			return $this->belongsToMany('App\Proveedor')->withTimestamps();
        }
        public function typereference()
		{
			return $this->belongsTo('App\Tiporeferencia');
		}
}
