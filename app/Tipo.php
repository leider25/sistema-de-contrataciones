<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $connection = "contrataciones";
    protected $table = "tipo";

	protected $primaryKey = 'id';
	protected $fillable = [
        'nombre'
    ];
    public function subtipos()
	{
		return $this->belongsToMany(Subtipo::class)->withTimestamps();
    }

    public function solicitud()
	{
		return $this->hasMany(Solicitud::class);
    }

}
