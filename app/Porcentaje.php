<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Porcentaje extends Model
{
    protected $connection = "contrataciones";
    protected $table = "porcentajes";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];
    public function garantias()
    {
    	return $this->hasMany(Garantia::class);
    }
}
