<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $connection = "rrhh";
    protected $table = "departments";
	protected $primaryKey = 'id';

	protected $fillable = [
		'name',
		'user_id'
	];

	public function user()
	{
		return $this->hasMany(User::class);
	}
}
