<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sispoacontrataciones extends Model
{
    protected $connection = "sispoa";
    protected $table = "sispoa_contrataciones";

		protected $fillable = [
			'usuario',
            'cod_actividad',
            'partida_2',
            'glosa_3',
            'costo_unitario',
            'cantidad',
            'subtotal_1a'
        ];
}
