<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
    protected $connection = "contrataciones";
    protected $table = "entregas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion',
        'plazo',
        'lugar_id'
    ];

    public function lugar()
	{
		return $this->belongsTo(Lugar::class);
    }
    
    public function condiciones_administrativas()
    {
        return $this->hasMany(Condicionadministrativa::class);
    }
}
