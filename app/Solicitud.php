<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $connection = "contrataciones";
    protected $table = "solicitud";

	protected $primaryKey = 'id';
	protected $fillable = [
        'objeto',
        'objetivo',
        'cite',
        'fecha',
        'antecedente',
        'justificacion_tecnica',
        'justificacion_directa',
        'nombreArchivo',
        'usuario_id',
        'tipo_id',
        'subtipo_id',
        'especificaciontecnica_id'
    ];
    public function usuario()
	{
		return $this->belongsTo('App\users')->withTimestamps();
    }
    public function partidas()
	{
		return $this->belongsToMany('App\Partida')->withTimestamps();
    }
    public function tipo()
	{
		return $this->belongsTo(Tipo::class);
    }
    public function subtipo()
  {
    return $this->belongsTo(Subtipo::class);
    }
    public function especificaciones_tecnicas()
	{
		return $this->belongsTo(Especificaiontecnica::class);
    }
    public function Adjudicaciones()
		{
			return $this->hasMany(Adjudicacion::class);
    }
    public function estados()
	{
		return $this->belongsToMany(Estado::class)->withPivot('usuario_id', 'motivo', 'activo')->withTimestamps();
    }
    

}
