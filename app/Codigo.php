<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigo extends Model
{
    protected $connection = "sispoa";
    protected $table = "codigos";

    protected $primaryKey = 'codigo';

		protected $fillable = [
			'padre',
            'nivel',
            'ramas',
            'nombre',
            'segundonombre',
        ];
}
