<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Garantia extends Model
{
    protected $connection = "contrataciones";
    protected $table = "garantias";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion',
        'tipogarantia_id',
        'porcentaje_id'
    ];
    public function tipogarantia()
	{
		return $this->belongsTo(Tipogarantia::class);
    }

    public function porcentaje()
	{
		return $this->belongsTo(Porcentaje::class);
    }

    public function condiciones_administrativas()
    {
    	return $this->hasMany(Condicionadministrativa::class);
    }
}
