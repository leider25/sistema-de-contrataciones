<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtipo extends Model
{
	protected $connection = "contrataciones";
    protected $table = "subtipo";

	protected $primaryKey = 'id';
	protected $fillable = [
		'nombre',
		'tipo_id'
    ];

    public function tipos()
	{
		return $this->belongsToMany(Tipo::class)->withTimestamps();
    }
	public function solicitud()
	{
		return $this->hasMany(Solicitud::class);
    }
}
