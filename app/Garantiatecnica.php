<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Garantiatecnica extends Model
{
    protected $connection = "contrataciones";
    protected $table = "garantiastecnicas";

	protected $primaryKey = 'id';

	protected $fillable = [
        'descripcion',
        'tipogarantiatecnica_id',
        'condiciontecnica_id'
    ];

    public function tipogarantiatecnica()
	{
		return $this->belongsTo(Tipogarantiatecnica::class);
    }

    public function condicionecnica()
	{
		return $this->belongsTo(Condiciontecnica::class);
    }
    
}
