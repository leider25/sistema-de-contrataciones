<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condiciontecnica extends Model
{
    protected $connection = "contrataciones";
    protected $table = "condiciones_tecnicas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'garantia_funcionamiento',
        'certificacion',
        'condicion_adicional',
        'pruebas_funcionamiento'

    ];

    public function items()
	{
		return $this->hasMany(Item::class);
    }

    public function garantiastecnicas()
	{
		return $this->hasMany(Garantiatecnica::class);
    }
    public function servicios()
	{
		return $this->hasMany(Servicio::class);
    }
    
    public function especificaciones_tecnicas()
	{
		return $this->hasMany(Especificaiontecnica::class);
	}
}
