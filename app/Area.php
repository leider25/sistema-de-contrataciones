<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $connection = "contrataciones";
    protected $table = "areas";

		protected $primaryKey = 'id';

		protected $fillable = [
			'alias',
			'nombre'
        ];
        public function users()
		{
			return $this->hasMany(User::class);
		}

}
