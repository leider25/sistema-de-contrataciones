<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = "contrataciones";
    protected $table = "items";

	protected $primaryKey = 'id';
	protected $fillable = [
        'nombre',
        'cantidad',
        'unidad',
        'condiciontecnica_id'       
    ];

    public function condiciontecnica()
	{
		return $this->belongsTo(Condiciontecnica::class);
    }

    public function caracteristicas()
	{
		return $this->hasMany(Caracteristica::class);
	}
}
