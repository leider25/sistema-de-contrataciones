<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiporeferencia extends Model
{
	protected $connection = "contrataciones";
    protected $table = "tipo_referencias";

		protected $primaryKey = 'id';

		protected $fillable = [
			'descripcion'
        ];
        
        public function referencias()
		{
			return $this->hasMany('App\Referencia');
		}
}
