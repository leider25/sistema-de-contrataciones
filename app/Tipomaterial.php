<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipomaterial extends Model
{
	protected $connection = "contrataciones";
    protected $table = "tipo_materiales";

		protected $primaryKey = 'id';

		protected $fillable = [
			'descripcion'
        ];
}
