<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipogarantia extends Model
{
    protected $connection = "contrataciones";
    protected $table = "tipo_garantia";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];
    public function garantias()
    {
    	return $this->hasMany(Garantia::class);
    }
}
