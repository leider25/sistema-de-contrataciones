<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $connection = "contrataciones";
    protected $table = "lugares";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];
    public function entregas()
    {
        return $this->hasMany(Entrega::class);
    }
}
