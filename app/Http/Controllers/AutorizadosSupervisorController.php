<?php

namespace App\Http\Controllers;

use App\Adjudicacion;
use App\Proveedor;
use App\Solicitud;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Estado;
use Auth;


class AutorizadosSupervisorController extends Controller
{
    public function index()
    {
        return view('layouts.supervisor.autorizado.index');
    }

    public function autorizacion($id){
      $solicitud=Solicitud::with('tipo')->where('id',$id)->first();
      $view = \View::make('layouts.supervisor.autorizado.print',compact('solicitud',$solicitud))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('autorizacion');
    }

    public function enviarLegal(Request $request){
        $solicitud=Solicitud::with('estados')->where('id',$request->get('id'))->first();
        $coleccion=$solicitud->estados;
        $user = Auth::user();
        foreach ($coleccion as $value) {
          if ($value->pivot->activo==1) {
            $value->pivot->activo=0;
            $value->pivot->save();
          }
        }
        $estado=Estado::find(10);

        $solicitud->estados()->attach($estado,[
          'usuario_id' => $user->id,
          'motivo'=> $request->get('motivo')
        ]);
        $solicitud->save();
        return response()->json($solicitud);
    }

    public function empresa($id){
        $proveedor=Proveedor::with('encargado','referencia')->where('id',$id)->first();
        return response()->json($proveedor);
    }
    public function generarAdjudicacion($id,Request $request){
        $adjudicacion=new Adjudicacion;
        $adjudicacion->fecha=$request->get('fecha');
        $adjudicacion->monto=$request->get('importe');
        $adjudicacion->proveedor_id=$request->get('select');
        $adjudicacion->solicitud_id=$id;
        $adjudicacion->save();
        return response()->json($adjudicacion);
    }

    public function adjudicacion($id){
        $adjudicacion=Adjudicacion::with('solicitud','proveedor')->where('solicitud_id',$id)->first();
        $fecha=fechaLiteral($adjudicacion->fecha);
        $importe=convertir($adjudicacion->monto);
        $view = \View::make('layouts.supervisor.autorizado.printAdjudicacion',compact('adjudicacion','fecha','importe'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('adjudicacion');
      }
    
    public function documentosProveedor(Request $request){
        $rules = array(
            'archivo' => 'required'
          );
            $validator = Validator::make ( Input::all(), $rules);
    
            if ($validator->fails())
            return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
            else {

            $adjudicacion=Adjudicacion::with('proveedor')->where('solicitud_id',$request->get('idsol'))->first();

              $archivo=$request->file('archivo');
              //MODIFICAR EL NOMBRE DE LA COTIZACION.
              //$nombreArchivos = 'cotizacion-'.time().'.'.$archivo->getClientOriginalExtension();
              $nombreArchivos = 'docs-'.$adjudicacion->proveedor->descripcion.'.'.$archivo->getClientOriginalExtension();
              //$archivo->storeAs($archivo->getClientOriginalName());
              //Storage::disk('local')->put('docs', $nombreArchivos);
              $path = $archivo->storeAs('documentos/docs',$nombreArchivos); 
              $adjudicacion->documento=$nombreArchivos;
              $adjudicacion->save();
    
              return response()->json($adjudicacion);
            }
           
    }


    public function documentos($id){
      $adjudicacion=Adjudicacion::findOrFail($id);
      return Storage::download('documentos/docs/'.$adjudicacion->documento);
    } 
}
