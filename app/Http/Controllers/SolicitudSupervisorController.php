<?php

namespace App\Http\Controllers;

use App\Estado;
use App\Solicitud;
use Illuminate\Http\Request;
use App\Tipo;
use Auth;

class SolicitudSupervisorController extends Controller
{
    public function index()
    {
        return view('layouts.supervisor.solicitud.index',compact('solicitud','tipocontratacion'));
    }

    public function validar(Request $request){
        $solicitud=Solicitud::with('estados')->where('id',$request->get('id'))->first();
        $coleccion=$solicitud->estados;
        $user = Auth::user();
        foreach ($coleccion as $value) {
          if ($value->pivot->activo==1) {
            $value->pivot->activo=0;
            $value->pivot->save();
          }
        }
        $estado=Estado::find(4);

        $solicitud->estados()->attach($estado,[
          'usuario_id' => $user->id,
          'motivo'=> $request->get('motivo')

        ]);

        $solicitud->save();

        return response()->json($solicitud);
    }

    public function corregir(Request $request){
        $solicitud=Solicitud::with('estados')->where('id',$request->get('id'))->first();
        $coleccion=$solicitud->estados;
        $user = Auth::user();
        foreach ($coleccion as $value) {
          if ($value->pivot->activo==1) {
            $value->pivot->activo=0;
            $value->pivot->save();
          }
        }
        $estado=Estado::find(3);

        $solicitud->estados()->attach($estado,[
          'usuario_id' => $user->id,
          'motivo'=> $request->get('motivo')

        ]);

        $solicitud->save();
        return response()->json($solicitud);
    }
}
