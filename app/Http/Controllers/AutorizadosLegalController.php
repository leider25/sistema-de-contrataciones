<?php

namespace App\Http\Controllers;

use App\Solicitud;
use Illuminate\Http\Request;
use App\Estado;
use Auth;

class AutorizadosLegalController extends Controller
{
    public function index()
    {
        return view('layouts.legal.index');
    }
    public function aprobar(Request $request){
        $solicitud=Solicitud::with('estados')->where('id',$request->get('id'))->first();
        $coleccion=$solicitud->estados;
        $user = Auth::user();
        foreach ($coleccion as $value) {
          if ($value->pivot->activo==1) {
            $value->pivot->activo=0;
            $value->pivot->save();
          }
        }
        $estado=Estado::find(7);
        $solicitud->estados()->attach($estado,[
          'usuario_id' => $user->id,
          'motivo'=> $request->get('motivo')
        ]);
        $solicitud->save();
        return response()->json($solicitud);
    }

    public function rechazar(Request $request){
        $solicitud=Solicitud::with('estados')->where('id',$request->get('id'))->first();
        $coleccion=$solicitud->estados;
        $user = Auth::user();
        foreach ($coleccion as $value) {
          if ($value->pivot->activo==1) {
            $value->pivot->activo=0;
            $value->pivot->save();
          }
        }
        $estado=Estado::find(11);

        $solicitud->estados()->attach($estado,[
          'usuario_id' => $user->id,
          'motivo'=> $request->get('motivo')
        ]);
        $solicitud->save();
        return response()->json($solicitud);
    }
}
