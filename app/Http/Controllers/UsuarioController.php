<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UsuarioController extends Controller
{
    public function index(Request $request)
    {
        $data = User::where('status',1)->whereIn('userstypes_id',[0,1,2])->orderBy('username','ASC')->paginate(15);
        $roles = Role::all(); 
        return view('layouts.administrator.usuarios.index',compact('roles','data'))
            ->with('i', ($request->input('page', 1) - 1) * 15);
    }

    public function roles($id){
        $user = User::find($id);
        $userRole = $user->roles->pluck('id')->all();
        return response()->json($userRole);

    }
}
