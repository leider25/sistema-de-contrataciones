<?php

namespace App\Http\Controllers\AdministratorController;

use App\Encargado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;   
use App\Proveedor;
use App\Tiporeferencia;
use App\Referencia;
use Illuminate\Support\Facades\Input;
class SupplierController extends Controller
{
    public function index(){
        $proveedor = Proveedor::paginate(20);
        $tipofererencia = Tiporeferencia::pluck('descripcion','id');
        return view('layouts.administrator.suppliers.index',compact('proveedor','tipofererencia'));
      }
    public function addSupplier(Request $request){
      $rules = array(
        'decripcion' => 'required',
        'direccion' => 'required',
        'encargado' => 'required',
        'cargo' => 'required'
      );
        $validator = Validator::make ( Input::all(), $rules);

        if ($validator->fails())
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
        else {
          $encargado=new Encargado;
          $encargado->nombre=$request->get('encargado');
          $encargado->cargo=$request->get('cargo');
          $encargado->save();

          $proveedor=new Proveedor;
          $proveedor->descripcion=$request->get('decripcion');
          $proveedor->direccion=$request->get('direccion');
          $proveedor->encargado_id=$encargado->id;
          $proveedor->save();

          $referencesArray = json_decode($request->get('array'));
          foreach ($referencesArray as $value) {
           
            $reference = new Referencia;
            $reference->descripcion=$value->reference;
            $reference->tiporeferencia_id=$value->id_type;
            $reference->save();
            $proveedor->referencia()->attach($reference);
          }

          return response()->json($proveedor);
        }
    }

      public function editSupplier(request $request){
        $proveedor = Proveedor::find ($request->id);
        $proveedor->descripcion = $request->title;
        $proveedor->save();
        return response()->json($proveedor);
      }

      public function deleteSupplier(request $request){
        $proveedor = Proveedor::find ($request->id)->delete();
        return response()->json();
      }
}
