<?php

namespace App\Http\Controllers;

use App\Area;
use App\Caracteristica;
use App\Condicionadministrativa;
use App\Condiciontecnica;
use App\Entrega;
use App\Especificaiontecnica;
use App\Garantia;
use App\Garantiatecnica;
use App\Lugar;
use App\Item;
use App\Multa;
use App\Porcentaje;
use App\Servicio;
use App\Servicioconexo;
use App\Sispoacontrataciones;
use App\Solicitud;
use App\Subtipocontratacion;
use App\Tipoadjudicacion;
use App\Tipocontratacion;
use App\Tipocontrato;
use App\Tipogarantia;
use App\Tipogarantiatecnica;
use Auth;
use Validator;
use Response;   
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\Style\Language;
use PhpOffice\PhpWord\Settings;

class EspecificacionTecnicaBienesController extends Controller
{
    public function index($id)
    {
        //$request = Hiringtype::paginate(20);
        //$solicitud=Tipocontratacion::Join('solicitud','tipocontratacion_id','=','tipo_contratacion.id')->get();
        $tipogarantiatecnica= Tipogarantiatecnica::pluck('descripcion','id');
        $serviciosconexos= Servicioconexo::pluck('descripcion','id');
        $tipoadjudicacion= Tipoadjudicacion::pluck('descripcion','id');
        $tipocontrato= Tipocontrato::pluck('descripcion','id');
        $lugares= Lugar::pluck('descripcion','id');
        $porcentajes= Porcentaje::pluck('descripcion','id');
        $tipogarantias= Tipogarantia::pluck('descripcion','id');
        $idsolicitud=$id;
        
        //$solicitud=Solicitud::with('tipocontratacion')->where('id',$id)->first();
        //$subTipoContratacion=Subtipocontratacion::with('tipocontratacion')->where('id',$solicitud->subtipocontratacion_id)->first();

 
        return view('layouts.documentos.bienes.especificacionesbienes',compact('tipogarantiatecnica',
                                                                  'serviciosconexos',
                                                                  'tipoadjudicacion',
                                                                  'tipocontrato',
                                                                  'lugares',
                                                                  'tipogarantias',
                                                                  'porcentajes',
                                                                  'idsolicitud'));
            

        
    }

    public function crear(Request $request){
        $rules = array(
            //'item' => 'required',
            //'cantidad' => 'required',
            //'unidad' => 'required',
            'garantiafunc' => 'required',
            'certificacion' => 'required',
            'adicional' => 'required',
            'inspeccion' => 'required',
            'modalidad' => 'required',
            'adjudicacion' => 'required',
            'select-contrato' => 'required',
            'select-lugares' => 'required',
            'lugar' => 'required',
            'plazo' => 'required',
            'validez' => 'required',
            'multa' => 'required',
            'porcentaje' => 'required'
          );
        $validator = Validator::make ( Input::all(), $rules);
        if ($validator->fails())
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
        else {
        
        $items=json_decode($request->get('items'));
        $lista_garantia=json_decode($request->get('listagarantia'));
        $lista_servicioconex=json_decode($request->get('listaservicioconex'));
        
        $items_lenght=count($items);
        $listgarant_lenght=count($lista_garantia);
        $listservicio_lenght=count($lista_servicioconex);


        //$user = Auth::user();

        //for ($i=0; $i < $items_lenght ; $i++) {   
        //    $datoCaracteristica["descripcion"]=$lista_caracts[$i];
        //    $datoCaracteristica["item_id"]=$item->id;
        //    Caracteristica::create($datoCaracteristica);
        //}
        $datoCondicionTectica["garantia_funcionamiento"]=$request->get('garantiafunc');
        $datoCondicionTectica["certificacion"]=$request->get('certificacion');
        $datoCondicionTectica["condicion_adicional"]=$request->get('adicional');
        $datoCondicionTectica["pruebas_funcionamiento"]=$request->get('inspeccion');
        $condicionTecnica=Condiciontecnica::create($datoCondicionTectica);

        for ($index=0; $index < $items_lenght; $index++) { 
          
          $datoItem["nombre"]=$items[$index]->nombre;
          $datoItem["cantidad"]=$items[$index]->cantidad;
          $datoItem["unidad"]=$items[$index]->unidad;
          $datoItem["condiciontecnica_id"]=$condicionTecnica->id;

          $item1=Item::create($datoItem);

          $caracteristicas=$items[$index]->caracteristicas;
          $caracteristicas_lenght=count($caracteristicas);

          for ($i=0; $i < $caracteristicas_lenght; $i++) { 
            $caracteristica=new Caracteristica;
            $caracteristica->descripcion=$caracteristicas[$i];
            //$datoCaracteristica["descripcion"]=$caracteristicas[$i];
            $item1->caracteristicas()->save($caracteristica);
          }
          //$condicionTecnica->item()->attach($item1);
        }


        //foreach($lista_garantia as $garantia){
        //    $datoGaratinaTecnica["descripcion"]=$garantia->garantia;
        //    $datoGaratinaTecnica["tipogarantiatecnica_id"]=$garantia->id_selectgt;
        //    $garantiaTecnica=Garantiatecnica::create($datoGaratinaTecnica);
        //    $condicionTecnica->garantiastecnicas()->attach($garantiaTecnica);
//
        //    //echo "El " . $posicion . " es " . $jugador;
        //}

        for ($i=0; $i < $listgarant_lenght ; $i++) {   
            $datoGaratinaTecnica["descripcion"]=$lista_garantia[$i]->garantia;
            $datoGaratinaTecnica["tipogarantiatecnica_id"]=$lista_garantia[$i]->id_selectgt;
            $datoGaratinaTecnica["condiciontecnica_id"]=$condicionTecnica->id;
            $garantiaTecnica=Garantiatecnica::create($datoGaratinaTecnica);
            //$condicionTecnica->garantiastecnicas()->attach($garantiaTecnica);
        }

        for ($i=0; $i < $listservicio_lenght ; $i++) {   
            $datoServicioConexo["descripcion"]=$lista_servicioconex[$i]->servicio;
            $datoServicioConexo["servicioconexo_id"]=$lista_servicioconex[$i]->id_selecttsc;
            $datoServicioConexo["condiciontecnica_id"]=$condicionTecnica->id;
            $servicio=Servicio::create($datoServicioConexo);
            //$condicionTecnica->servicios()->attach($servicio);
        }

        $datoEntrega["descripcion"]=$request->get('lugar');
        $datoEntrega["plazo"]=$request->get('plazo');
        $datoEntrega["lugar_id"]=$request->get('select-lugares');
        $entrega=Entrega::create($datoEntrega);

        $datoEntrega["descripcion"]=$request->get('multa');
        $datoEntrega["porcentaje_multa"]=$request->get('porcentaje');
        $multa=Multa::create($datoEntrega);

        $datoCondicioAdministrativa["validez_propuesta"]=$request->get('validez');
        $datoCondicioAdministrativa["tipoadjudicacion_id"]=$request->get('adjudicacion');
        $datoCondicioAdministrativa["tipocontrato_id"]=$request->get('select-contrato');
        $datoCondicioAdministrativa["entrega_id"]=$entrega->id;
        
        $datoCondicioAdministrativa["multa_id"]=$multa->id;

        if($entrega->plazo < 16){
            $datoCondicioAdministrativa["garantia_id"]=null;
        }else{
            $datoGarantia["descripcion"]=$request->get('descripcion');
            $datoGarantia["tipogarantia_id"]=$request->get('select-gar');
            $datoGarantia["porcentaje_id"]=$request->get('select-porc');
            $garantia=Garantia::create($datoGarantia);
            $datoCondicioAdministrativa["garantia_id"]=$garantia->id;
        }

        $condicionAdministrativa=Condicionadministrativa::create($datoCondicioAdministrativa);

        $datoEspecificacionTecnica["condiciontecnica_id"]=$condicionTecnica->id;
        $datoEspecificacionTecnica["condicionadministrativa_id"]=$condicionAdministrativa->id;
        $especificacionTecnica=Especificaiontecnica::create($datoEspecificacionTecnica);

        $idSolicitud=$request->get('idsolicitud');
        $solicitud=Solicitud::findOrFail($idSolicitud);
        $solicitud->especificaciontecnica_id=$especificacionTecnica->id;
        $solicitud->save();

        //$tipocontratacion = Tipocontratacion::pluck('nombre','id');
        //$solicitud=Solicitud::with('tipoContratacion')->get();
//
        //$user = Auth::user();
        //$area=Area::where('id',$user->area_id)->first();
        //$sispoa=Sispoacontrataciones::where('usuario',$area->alias)->get();

   
        //$datoCondicionTectica["fecha"]=$request->get('fecha');          
        //$datoCondicionTectica["antecedente"]=$request->get('antecedentes');
        //$datoCondicionTectica["justificacion_tecnica"]=$request->get('justificacion');
        //$datoCondicionTectica["usuario_id"]=$user->id;
        //$datoCondicionTectica["tipocontratacion_id"]=$request->get('tipo1');
        //return view('layouts.documentos.index',compact('solicitud','tipocontratacion','sispoa'));

        }
        
        return response()->json($solicitud);
    }

    public function actualizar(Request $request){
      $rules = array(
          'garantiafunc' => 'required',
          'certificacion' => 'required',
          'adicional' => 'required',
          'inspeccion' => 'required',
          'modalidad' => 'required',
          'adjudicacion' => 'required',
          'select-contrato' => 'required',
          'select-lugares' => 'required',
          'lugar' => 'required',
          'plazo' => 'required',
          'validez' => 'required',
          'multa' => 'required',
          'porcentaje' => 'required'
        );
      $validator = Validator::make ( Input::all(), $rules);
      if ($validator->fails())
      return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
      else {


      
      
      $items=json_decode($request->get('items'));
      $lista_garantia=json_decode($request->get('listagarantia'));
      $lista_servicioconex=json_decode($request->get('listaservicioconex'));
    
      
      $items_lenght=count($items);
      $listgarant_lenght=count($lista_garantia);
      $listservicio_lenght=count($lista_servicioconex);
      
      $idEspecificacionTecnica=$request->get('idespecificaciontecnica');
      
      $especificacionTecnica=Especificaiontecnica::with('condiciontecnica','condicionadministrativa')
                              ->where('id',$idEspecificacionTecnica)->first();
      
      $itemsDB=Item::with('caracteristicas')
              ->where('condiciontecnica_id',$especificacionTecnica->condiciontecnica->id)
              ->get();
      $itemsDB_lenght=count($itemsDB);

      /**INICIO ACTUALIZACION ITEMS */
      $itemsValidos=array();
      $itemsEliminados=array();
      for ($i=0; $i < $itemsDB_lenght; $i++) { 
        for ($j=0; $j < $items_lenght; $j++) { 
          if ($itemsDB[$i]->id == $items[$j]->clave) {
            array_push($itemsValidos,$itemsDB[$i]->id);
     
          }
        }
      }
      $itemsValidos_lenght=count($itemsValidos);

      for ($i=0; $i <$itemsDB_lenght ; $i++) { 
         if (!in_array($itemsDB[$i]->id,$itemsValidos,TRUE) && !in_array($itemsDB[$i]->id,$itemsEliminados,TRUE)) {
           array_push($itemsEliminados,$itemsDB[$i]->id);
         }
      }

      $itemsEliminados_lenght=count($itemsEliminados);

      /*Actualizando datos de los items nuevos*/
      for ($i=0; $i <$itemsValidos_lenght ; $i++) {
        /* Llamando a uno de los items validos */ 
        $item=Item::with('caracteristicas')->where('id',$itemsValidos[$i])->first();
        $itemNuevo=null;
        for ($j=0; $j <$items_lenght ; $j++) { 
          if ($item->id==$items[$j]->clave) {
            /* Mismo item anterior pero con datos Actualizados */
            $itemNuevo=$items[$j];

            /*Actualizando Datos de item*/
            $item->nombre   = $itemNuevo->nombre;
            $item->unidad   = $itemNuevo->unidad;
            $item->cantidad = $itemNuevo->cantidad;

            /**Actualizando Características*/
            $caracteristicasAntiguas=$item->caracteristicas;
            $caracteristicasActualizadas=$itemNuevo->caracteristicas;

            
            

            $caracteristicasAntiguas_lenght=count($caracteristicasAntiguas);
            $caracteristicasActualizadas_lenght=count($caracteristicasActualizadas);

            $caracteristicasValidas=array();
            $caracteristicasEliminados=array();
            /**Obtenemos el Id de las caracteristicas que no se han eliminados, estas son alamacenadas en 
             * el array $caracteristicasValidas
             */
            for ($x=0; $x <$caracteristicasAntiguas_lenght; $x++) { 
              for ($y=0; $y <$caracteristicasActualizadas_lenght ; $y++) { 
                if ($caracteristicasAntiguas[$x]->id==$caracteristicasActualizadas[$y]->clave) {
                  array_push($caracteristicasValidas,$caracteristicasAntiguas[$x]->id);
                }
              }
            }
            
            /**Obtenemos el Id de las caracteristicas que se han eliminados, estas son alamacenadas en 
             * el array $caracteristicasEliminados
             */
            for ($z=0; $z <$caracteristicasAntiguas_lenght ; $z++) { 
              if (!in_array($caracteristicasAntiguas[$z]->id,$caracteristicasValidas,TRUE) && !in_array($caracteristicasAntiguas[$z]->id,$caracteristicasEliminados,TRUE)) {
                array_push($caracteristicasEliminados,$caracteristicasAntiguas[$z]->id);
              }
            }
         
            $caracteristicasValidas_lenght=count($caracteristicasValidas);
            $caracteristicasEliminados_lenght=count($caracteristicasEliminados);
            
            


            /**Actualizando Caracteristicas */
            for ($x=0; $x <$caracteristicasValidas_lenght ; $x++) {
              /**Caracteristica antigua no eliminada */ 
              $caracteristica=Caracteristica::where('id',$caracteristicasValidas[$x])->first();
              $caracteristicaActualizada=null;
              for ($y=0; $y <$caracteristicasActualizadas_lenght ; $y++) { 
                if ($caracteristica->id==$caracteristicasActualizadas[$y]->clave) {
                  /**Misma caracteristica con posibles nuevos datos a hacer actualizada*/
                  $caracteristicaActualizada=$caracteristicasActualizadas[$y];

                  $caracteristica->descripcion = $caracteristicaActualizada->descripcion;
                  $caracteristica->save();
                }
              }
            }
           

            /**Eliminando Caracteristicas*/
            for ($x=0; $x <$caracteristicasEliminados_lenght ; $x++) { 
              $caracteristica=Caracteristica::find($caracteristicasEliminados[$x]);
              $caracteristica->delete();
            }
         

            /**Añadiendo nuevas Caracteristicas*/
            for ($x=0; $x <$caracteristicasActualizadas_lenght ; $x++) { 
              if ($caracteristicasActualizadas[$x]->clave==0) {
                $caracteristica=new Caracteristica;
                $caracteristica->descripcion=$caracteristicasActualizadas[$x]->descripcion;
                $item->caracteristicas()->save($caracteristica);
              }
            }/**Fin de Actualizacion de Características */

          }
        }
        
      }
      

        /**Añadiendo Items Nuevos */
        for ($x=0; $x < $items_lenght; $x++) { 
          if ($items[$x]->clave==0) {
            $datoItem["nombre"]=$items[$x]->nombre;
            $datoItem["cantidad"]=$items[$x]->cantidad;
            $datoItem["unidad"]=$items[$x]->unidad;
            $datoItem["condiciontecnica_id"]=$especificacionTecnica->condiciontecnica->id;
            $item1=Item::create($datoItem);
            /**Caracteristicas del Nuevo Item*/
            $caracteristicasNuevo=$items[$x]->caracteristicas;
            $caracteristicasNuevo_lenght=count($caracteristicasNuevo);
                
            for ($y=0; $y < $caracteristicasNuevo_lenght; $y++) { 
              $caracteristica=new Caracteristica;
              $caracteristica->descripcion=$caracteristicasNuevo[$y]->descripcion;
              $item1->caracteristicas()->save($caracteristica);
            }
            
            
          }
        }
        

        /**Eliminando Items*/
        for ($x=0; $x <$itemsEliminados_lenght ; $x++) { 
          $itemElim=Item::with('caracteristicas')->where('id',$itemsEliminados[$x])->first();
          

          $caractsElim=$itemElim->caracteristicas;
          $caractsElim_lenght=count($caractsElim);

          for ($y=0; $y <$caractsElim_lenght ; $y++) { 
            $caracteristicaElim=Caracteristica::where('item_id',$itemElim->id)->first();
            $caracteristicaElim->delete();
          }
          $itemElim->delete();
        }
        /**FIN ACTUALIZACION ITEMS */

        /**INICIO ACTUALIZACION GARANTIAS TÉCNICAS*/
        $garantiastecnicasDB=Garantiatecnica::with('tipogarantiatecnica')
                              ->where('condiciontecnica_id',$especificacionTecnica->condiciontecnica->id)
                              ->get();
        $garantiastecnicasDB_lenght=count($garantiastecnicasDB);
        

        $gtValidos=array();
        $gtEliminados=array();

        for ($i=0; $i <$garantiastecnicasDB_lenght; $i++) { 
          for ($j=0; $j <$listgarant_lenght; $j++) { 
            if ($garantiastecnicasDB[$i]->id==$lista_garantia[$j]->clave) {
              array_push($gtValidos,$garantiastecnicasDB[$i]->id);
            }
          }
        }
        $gtValidos_lenght=count($gtValidos);

        for ($x=0; $x <$garantiastecnicasDB_lenght ; $x++) { 
          if (!in_array($garantiastecnicasDB[$x]->id,$gtValidos,TRUE) && !in_array($garantiastecnicasDB[$x]->id,$gtEliminados,TRUE)) {
            array_push($gtEliminados,$garantiastecnicasDB[$x]->id);
          }
        }

        $gtEliminados_lenght=count($gtEliminados);

        for ($i=0; $i <$gtValidos_lenght; $i++) { 
          $garantiaTecnica=Garantiatecnica::find($gtValidos[$i]);
          for ($j=0; $j < $listgarant_lenght; $j++) { 
            if ($garantiaTecnica->id==$lista_garantia[$j]->clave) {
              $garantiaTecnicaActualizado=$lista_garantia[$j];
              $garantiaTecnica->descripcion             = $garantiaTecnicaActualizado->garantia;
              $garantiaTecnica->tipogarantiatecnica_id  = $garantiaTecnicaActualizado->id_selectgt;
              $garantiaTecnica->save();
            }
          }
        }

        for ($i=0; $i <$gtEliminados_lenght ; $i++) { 
          $garantiaTecnica=Garantiatecnica::find($gtEliminados[$i]);
          $garantiaTecnica->delete();
        }

        for ($i=0; $i <$listgarant_lenght ; $i++) {
          if ($lista_garantia[$i]->clave==0) {
            $garantiaTec=new Garantiatecnica();
            $garantiaTec->descripcion=$lista_garantia[$i]->garantia;
            $garantiaTec->tipogarantiatecnica_id=$lista_garantia[$i]->id_selectgt;
            $garantiaTec->condiciontecnica_id=$especificacionTecnica->condiciontecnica->id;
            $garantiaTec->save();
          } 
          
        }
        /**FIN ACTUALIZACION GARANTIAS TÉCNICAS*/

        /**INICIO ACTUALIZACION Tipo de Servicios Conexos*/
        /**FIN ACTUALIZACION Tipo de Servicios Conexos*/


      }
      return response()->json($lista_garantia);
  }


    public function edit($id){
      $especificacionbienes=Especificaiontecnica::with('condicionadministrativa','condiciontecnica')->where('id',$id)->first();
      return view('layouts.documentos.bienes.edit',compact('id','especificacionbienes'));
    }

    public function generadorWord($id){
      
       // $solicitud=Solicitud::with('tipoContratacion')->where('id', '=', $id)->get();
        //dd($solicitud);
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
          // New portrait section
         //$section = $phpWord->addSection(array("paperSize" => "Letter"));
        $PidPageSettings = array(
         "paperSize" => "Letter",
         'headerHeight'=> \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.2),
         'footerHeight'=> \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0),
         //'marginLeft'  => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5),
         //'marginRight' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5),
         //'marginTop'   => 1,
         //'marginBottom'=> 1
        );
        $section = $phpWord->createSection($PidPageSettings);
        
        $header = $section->addHeader();
  
          $header->addWatermark('./images/escudoBolivia.png',array('width' => 105, 'height' => 73,
          'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0.2)),
          'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1)),
          'posHorizontal' => 'absolute','posVertical' => 'absolute'));
  
          $header->addWatermark('./images/fonabosque.png',array('width' => 120, 'height' => 60,
          'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-0.25)),
          'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(4.80)),
          'posHorizontal' => 'absolute','posVertical' => 'absolute'));
  
  
          $header->addWatermark('./images/logoMMAYA.png', 
          array('width' => 130, 'height' => 41,
          'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-0.4)),
          'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(10.15)),
          'posHorizontal' => 'absolute','posVertical' => 'absolute'));
  
  
  
  
          
         
          $header->addWatermark('./images/footer-info.png',
          array('width' => 612, 'height' => 55,
          'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(17)),
          'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1.75)),
          'posHorizontal' => 'absolute','posVertical' => 'absolute'));
  
  
  
          //$estiloTitulo=array('size' => 12, 'Arial' => true,'bold' => true,'bgColor' => 'fbbb10');
          //$section->addText('NOTA INTERNA',$estiloTitulo,array('align'=>'center'));
  
          $phpWord->addTitleStyle(1, array('size' => 12, 'Arial' => true,'bold' => true), array('align' => 'center' ));
          $section->addTitle('ESPECIFICACIONES TÉCNICAS', 1);
          
  
  
  
          
          $esttiloFuente = array("name" => "Arial Narrow",
                          "size" => 11,
                        //  "color" => "8bc34a",
                        //  "italic" => true,
                        //  "bold" => true
                        );
          $esttiloFuente2 = array("name" => "Arial Narrow",
            "size" => 11,
          //  "color" => "8bc34a",
          //  "italic" => true,
            "bold" => true
          );

          $phpWord->addFontStyle(
            'estiloDescripcion',
            array(
              'name' => 'Arial',
                'size'    => '11',
                //'bold'    => true,
                //'allCaps' => true,
                //'scale'   => 200,
                //'spacing' => 240,
                //'kerning' => 10,
            ));

        //Estilos para los párrafos
        $solicitud=Solicitud::where('especificaciontecnica_id',$id)->first();;

        $fontStyle = array("name" => "Calibri","size" => 10, "bold"=>true);
        $phpWord->addFontStyle('arial11bold', $fontStyle);

        $fontStyle2 = array("name" => "Calibri","size" => 10, );
        $phpWord->addFontStyle('arial11', $fontStyle2);
        
        $estiloParrafo = array('align'=>'both');
        $phpWord->addParagraphStyle('parrafo',$estiloParrafo);
  
  
        $estiloTablaEspecificacion = array('borderColor' => '006699','borderSize' => 6,'align' => 'center');
        $estiloCabecera = array('bgColor' => 'C7D69E');
        $phpWord->addTableStyle('miTablaEspecificacion', $estiloTablaEspecificacion, $estiloCabecera);

        $quitarEspaciosEntreParrafos=array("spaceAfter"=>0,"lineHeight"=>1.0);
        $sangria=array("spaceAfter"=>0,"lineHeight"=>1.0,"indentation" => array("left" => 100));
        $parrafoTitulo=array('align' => 'center',"spaceAfter"=>0,"lineHeight"=>1.0);
  
        
        $textoCorrido=$section->addTextRun('parrafo');
        $textoCorrido->addText("OBJETO DE LA CONTRATACIÓN: ", 'arial11bold');
        $textoCorrido->addText($solicitud->objeto, 'arial11');
        
        
        
        $especificacionTecnica=Especificaiontecnica::with('condiciontecnica','condicionadministrativa')
                                                    ->where('id', '=', $id)
                                                      ->first();


       // $items=Item::whereHas('condiciontecnica', function($q) use ($id) {
       //   $q->where('id', $id);
       //   })->get();

        

        

        $items=Item::where('condiciontecnica_id',$especificacionTecnica->condiciontecnica->id)->get();


        $garantiastecnicas=Condiciontecnica::find($especificacionTecnica->condiciontecnica->id)->garantiastecnicas()->get();
        $servicios=Condiciontecnica::find($especificacionTecnica->condiciontecnica->id)->servicios()->get();

        
        $tablaEspecificacion = $section->addTable('miTablaEspecificacion');
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(12000, array('valign'=>'center','gridSpan' => 2,'hMerge' => 'restart'))->addText("CARACTERÍSTICAS",'arial11bold', $parrafoTitulo);
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(12000, array('valign'=>'center','gridSpan' => 2,'hMerge' => 'restart','bgColor' => 'C7D69E'))->addText("CONDICIONES TÉCNICAS",'arial11bold',$parrafoTitulo);

        foreach ($items as $key => $item) {
        
          $tablaEspecificacion->addRow();
          $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Ítem, Lote, etc. N°",'arial11bold', $sangria);
          $tablaEspecificacion->addCell(9000)->addText($items[$key]->nombre,'arial11',$sangria);

          $tablaEspecificacion->addRow();
          $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Cantidad",'arial11bold', $sangria);
          $tablaEspecificacion->addCell(9000)->addText($items[$key]->cantidad.", ".$items[$key]->unidad,'arial11',$sangria);

          //$tablaEspecificacion->addRow();
          //$tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Marca",'arial11bold', $sangria);
          //$tablaEspecificacion->addCell(9000)->addText("",'arial11',$sangria);

          //$tablaEspecificacion->addRow();
          //$tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("País de fabricación",'arial11bold', $sangria);
          //$tablaEspecificacion->addCell(9000)->addText("",'arial11',$sangria);

          //$tablaEspecificacion->addRow();
          //$tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Modelo",'arial11bold', $sangria);
          //$tablaEspecificacion->addCell(9000)->addText("",'arial11',$sangria);

          $tablaEspecificacion->addRow();
          $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Características",'arial11bold', $sangria);
          $celda=$tablaEspecificacion->addCell(9000);

          $caracteristicas=Caracteristica::where('item_id',$items[$key]->id)->get();

          foreach ($caracteristicas as $clave => $value) {
            $celda->addListItem($caracteristicas[$clave]->descripcion,0,'arial11',ENT_COMPAT,array("spaceAfter"=>0,"lineHeight"=>1.0));
          }
        
        }

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Garantías técnicas",'arial11bold', $sangria);
        $celda=$tablaEspecificacion->addCell(9000);
        foreach ($garantiastecnicas as $clave => $value) {
          $tipogarantiatecnica=Tipogarantiatecnica::where('id',$garantiastecnicas[$clave]->tipogarantiatecnica_id)->first();
          $textoCorridoGarantia=$celda->addTextRun($sangria);
          $textoCorridoGarantia->addText($tipogarantiatecnica->descripcion.": ",'arial11bold');
          $textoCorridoGarantia->addText($garantiastecnicas[$clave]->descripcion,'arial11');
        }
        
        

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Garantía de funcionamiento de maquinaria y/o equipo",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($especificacionTecnica->condiciontecnica->garantia_funcionamiento,'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Certificaciones",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($especificacionTecnica->condiciontecnica->certificacion,'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Servicios conexos",'arial11bold',$sangria);
        $celda=$tablaEspecificacion->addCell(9000);
        foreach ($servicios as $clave => $value) {
          $servicioconexo=Servicioconexo::where('id',$servicios[$clave]->servicioconexo_id)->first();
          $textoCorridoSolicitud=$celda->addTextRun($sangria);
          $textoCorridoSolicitud->addText($servicioconexo->descripcion.": ",'arial11bold');
          $textoCorridoSolicitud->addText($servicios[$clave]->descripcion,'arial11');
        }


        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Condiciones adicionales sujetos de evaluación",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($especificacionTecnica->condiciontecnica->condicion_adicional,'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Inspecciones y Pruebas de Funcionamiento",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($especificacionTecnica->condiciontecnica->pruebas_funcionamiento,'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(12000, array('valign'=>'center','gridSpan' => 2,'hMerge' => 'restart','bgColor' => 'C7D69E'),$estiloCabecera)->addText("CONDICIONES ADMINISTRATIVAS",'arial11bold', $parrafoTitulo);

        $solicitud=Solicitud::with('tipo')->where('especificaciontecnica_id',$id)->first();
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Modalidad de Contratación",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($solicitud->tipo->nombre,'arial11',$sangria);

        $adjudicacion=Tipoadjudicacion::find($especificacionTecnica->condicionadministrativa->tipoadjudicacion_id);
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Forma de Adjudicación",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($adjudicacion->descripcion,'arial11',$sangria);

        $entrega=Entrega::with('lugar')->where('id',$especificacionTecnica->condicionadministrativa->entrega_id)->first();
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Lugar de entrega",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($entrega->lugar->descripcion,'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Plazo de entrega",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($entrega->plazo." (dias)",'arial11',$sangria);

        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Validez de la Propuesta",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($especificacionTecnica->condicionadministrativa->validez_propuesta,'arial11',$sangria);

        $multa=Multa::find($especificacionTecnica->condicionadministrativa->multa_id)->first();
        $tablaEspecificacion->addRow();
        $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Multas",'arial11bold', $sangria);
        $tablaEspecificacion->addCell(9000)->addText($multa->descripcion." ".$multa->porcentaje_multa."%",'arial11',$sangria);

        $garantia=Garantia::with('tipogarantia','porcentaje')->where('id',$especificacionTecnica->condicionadministrativa->garantia_id)->first();
        if (isset($garantia)) {
          $tablaEspecificacion->addRow();
          $tablaEspecificacion->addCell(3000, array('valign'=>'center'))->addText("Garantía de cumplimiento de Contrato",'arial11bold', $sangria);
          $tablaEspecificacion->addCell(9000)->addText($garantia->descripcion." (".$garantia->porcentaje->descripcion.'), ('.$garantia->tipogarantia->descripcion.")",'arial11',$sangria);
        }
        


        $phpWord->getSettings()->setThemeFontLang(new Language(Language::ES_ES));
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Appdividend.docx');

        
        return response()->download(public_path('Appdividend.docx'));
    }
    public function generadorPdf($id){
      //$id = Hashids::decode($id)[0];
      //$ticket = Ticket::find($id);
      $solicitud=Solicitud::with('tipo')->where('especificaciontecnica_id',$id)->first();
      $view = \View::make('layouts.documentos.bienes.especificacionPdf',compact('solicitud',$solicitud))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('report');
   }
}
