<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipogarantiatecnica extends Model
{
    protected $connection = "contrataciones";
    protected $table = "tipo_garantias_tecnicas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'descripcion'
    ];

    public function garantiastecnicas()
	{
		return $this->hasMany(Garantiatecnica::class);
	}

}
